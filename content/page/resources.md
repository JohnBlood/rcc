---
title: "Resources"
date: "2012-07-27"
---

**Fr. John Emerson FSSP's Interview with the Wanderer in the 1990s concerning the SSPX and the Latin Mass Movement**

[PDF scan of original copy](https://www.dropbox.com/s/szbttld5if1thgw/WandererArticle_merged.pdf?dl=0) and [cleaned up copy](https://www.dropbox.com/s/42nbmsvjhavafnr/The%20Wanderer%20Interviews%20Society%20of%20St%20Peter%20Priest.pdf?dl=0)

**Michael Davies' Medjugorje after Twenty-One Years**

[PDF](https://www.dropbox.com/s/eyyive1j7tu89e9/MichaelDaviesMedjugorje.pdf?dl=0)

**How Christ Said the First Mass**

[PDF](https://www.dropbox.com/s/0eywv5mc6fwxx1f/How%20Christ%20Said%20the%20First%20Mass.pdf?dl=0)
