---
title: "eBooks"
date: "2015-03-29"
---

## Church Triumphant: 25 Men and Women who Gave Their Lives to Christ

[![Church Triumphant](images/church-triumphant-cover-small.jpg)](https://books2read.com/b/mdp7Rm)

[![buy-now](images/buy-now.png)](https://books2read.com/b/mdp7Rm)

## Letters Home

[![letters home-small](images/letters-home-small.jpg)](https://books2read.com/b/4NR509)

[![buy-now](images/buy-now.png)](https://books2read.com/b/4NR509)
