---
title: "About"
date: "2009-03-18"
---

This blog is written by a Roman Catholic who is so fed up with what is going on in the world and in his church that he will no longer be silent.  The goal of this blog is to bring answers and enlightenment to some and shame to others.  With Jesus and Mary guiding his words, the writer hopes to help combat the move towards liberalism (the mental disorder) and help bring about a return to traditions.
