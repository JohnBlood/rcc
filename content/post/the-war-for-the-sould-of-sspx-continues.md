---
title: "The War for the Soul of SSPX Continues"
date: "2013-04-15"
categories: 
  - "bishop-williamson"
  - "judas"
  - "latin-mass"
  - "protestants"
  - "sspx"
  - "tradition"
tags: 
  - "catholic"
  - "latin-mass"
  - "religion"
---

Last October, I wrote an [article](http://realromancatholic.com/2012/10/20/sspx-splinters-further-like-all-protestant-organizations/) stating that there were three distinctive splinter groups within SSPX. It appears that I was wrong about that. Instead, there appears to be one main, umbrella group working to create an alternative, ultra-right wing version of SSPX. I'm basing this on a couple of articles that I came across recently.

\[caption id="attachment\_634" align="alignright" width="345"\][![Fr Joseph Pfeiffer](images/frjoepfeiffer.png)](http://realromancatholic.files.wordpress.com/2013/04/frjoepfeiffer.png) Fr Joseph Pfeiffer, superior general of the Society of St. Pius X of Strict Observance and possible future bishop of a new SSPX/traditionalist splinter group\[/caption\]

First, some background. As most people know (or should know), in the last couple of years the SSPX (Society of St. Pius X) has undergone quite a bit of internal conflict. The leaders of the SSPX (especially Bishop Fellay, the superior general) have become more friendly and willing to at least talk with the Vatican as time goes on. However, other elements within the organization are dead set against even talking with Rome. This latter group is the one I will be discussing.

I recently came across a short blog post dated January 2013. This post stated that there was a rumor that secret meeting was going to take place in Virginia consisting of Bishop Williamson (who was kicked out of SSPX in October 2012) and a number of priests who were kicked out of the Society because their opposition to the SSPX-Vatican talks. According to the rumor, the purpose of this meeting was to create an organization that conformed to their (narrow-minded) interpretation of Archbishop Lefebvre's vision for the society. Further, the rumor stated that they may pick a new bishop (who would probably be consecrated by Williamson) and that the candidate may be Fr. Pfeiffer, the head of the Society of St. Pius X of Strict Observance (which was created in August of 2012).

\[caption id="attachment\_643" align="alignright" width="350"\][![Not quite what most seminarians expect.](images/sspxsoseminary.png)](http://realromancatholic.files.wordpress.com/2013/04/sspxsoseminary.png) Not quite what most seminarians expect.\[/caption\]

The second document I found was a letter send out by a group calling itself the "resistance". This letter was dated December 22, 2012 and tells of a five-day retreat preached by Bishop Williamson. It appears that there were only 10 priests in attendance (including Williamson) and the focus was on the problems in the "conciliar church" and "neo-SSPX". These two phrase show just how far out these people are and I will cover these terms later. The end result was the creation of a "federation" of several "Traditional" catholic groups who are not connected with Rome under the spiritual and moral authority of Williamson. (Nothing like having a Holocaust denier on your side to make you look legit.) The letter concludes by telling of plans to create their own seminary in a small farms house (since SSPX seminaries are now corrupted) and with a list of upcoming events to help the "resistance" grow around the world.

\[caption id="attachment\_638" align="alignright" width="299"\][![Bishop Williamson](images/bpwilliamson.png)](http://realromancatholic.files.wordpress.com/2013/04/bpwilliamson.png) Bishop Williamson\[/caption\]

Finally, Bishop Williamson published an open letter to all SSPX priests on Holy Thursday of this year. Now, Williamson publishes a newsletter every week, but this was a special edition directed at SSPX priests. This letter was nothing but a call to arms against both the Catholic Church and the SSPX. He accuses the leadership of SSPX of going with the flow and forgetting Lefebvre's vision by even considering the Doctrinal Declaration the Vatican sent to SSPX last April. One of the points that irritates me the most is that he says:

> Fidelity promised to the “Catholic Church” and to the “Roman Pontiff” can easily be misdirected today towards the Conciliar Church as such, and to the Conciliar Pontiffs. Distinctions are needed to avoid confusion.

What is he talking about? The Catholic Church is in Rome and the Roman Pontiff is its head. Further, he goes on to call the Vatican "the apostates of Rome".

**Conclusion**

This "resistance" within SSPX has been coming for a long time. Early last year and even earlier, there were reports that Williamson and his cronies were working to get control of SSPX property if Fellay did sign an agreement to return to Rome.

All of these efforts find their root in a hatred from Rome (and by extension the Roman Catholic Church) and a love of power. These people are seeking to rebel against their superiors and the Church's spiritual authority. They pretend that they are safeguarding the Faith, but the truth of the matter is that they are embracing the Protestant mentality. They are embracing the Protestant mentality that if you do not agree with the current Church structure or Church leaders, you can create a new organization that you like and agree with and, more importantly, one that you control. Many of these men have no one order them and operate at their own discretion and they like it. See, they are ignoring the Pope's right to rule the Church because they do not want any authority but their own. In fact, Williamson does not want to give fidelity to the "Roman Pontiff" and he calls Pope Benedict XVI’s “Hermeneutic of continuity" madness. When a man who claims to be a prince of the Church attacks the ideas of the infallible ruler of that Church, how can he be taken seriously? Especially, when he has already set himself up as the head of a religious organization with the sole power to ordain and confirm. It is obvious that he, like others of his ilk, want to be pope in all but name.

The fact that both the letter from the "resistance" and Williamson use the phrase"conciliar church" to describe the Roman Catholic Church is further evidence that they intend to stay separated from Rome. By using this phrase they make it sound like the Catholic Church is no longer the one true Church founded by Christ to save souls. But it still is. The Church at this time may be very flawed and broken, but that is because it is made up of men who are sinners in a world tainted with sin. The sole purpose of this phrase is create a wedge between well intentioned people and the Church by replacing Catholic or universal with a word that loathesome to them.

In the end, all we can do is pray. We must pray that these resistance and rebel groups within SSPX will not lead good and well-intentioned people away from the Roman Catholic Church. We must also pray that those involved in this rebellion will see God's light and return to the Catholic Church to help us in our fight to guard tradition.
