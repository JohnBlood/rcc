---
title: "SSPX Widens the Gap Between Themselves and Rome Yet Again (Will They Ever Stop)"
date: "2013-11-24"
categories: 
  - "sspx"
---

Once again the SSPX has been in the news. And once again, I have a bone to pick with them. (Actually, several bones.)

To begin with, Fellay made a couple very stupid statement recently at the Angelus Press Conference in Kansas City.

I'm going to start with the worst:

> “The situation of the Church is a real disaster, and the present Pope is making it 10,000 times worse.”

Fellay, by making this one statement, has done more to harm SSPX relations with the Vatican than anything since Lefebvre's original schismatic act. By attacking the Pope, the Vicar of Christ, Fellay is attacking Christ and His Church. What self-respecting "bishop" would dare to attack the Pope, especially one that claims to uphold the traditions of the Church? The Church has had her share of liberal Popes in the past, but that have never changed the validity of the office of the Pope.

Fellay's second statement concerned the recent Vatican-SSPX negotiations:

> They want us to recognize not only that the \[New\] Mass is valid provided it is celebrated correctly, etc., but that it is licit. I told them: we don’t use that word. It’s a bit messy, our faithful have enough \[confusion\] regarding the validity, so we tell them, ‘The New Mass is bad, it is evil’ and they understand that. Period!’” Of course the Roman authorities “were not very happy with that.

Regardless of what you believe about English (or Novus Ordo) Mass, as long as the words of Consecration are still present then the Mass is valid. The whole purpose of the Holy Sacrifice of the Mass is to give the Children of God spiritual food. As long as the required words exist, the Mass is valid. That is not complicated as Fellay maked or wants it to be.

The second recent event really ticked me off was the funeral of Erich Priebke. The former SS captain was found guilty of taking part in the massacre of 335 Italians by an Italian court in 1998. Since then he has lived under house arrest. After he died, Diocese of Rome said that his funeral should take place in private. This did not satisfy the Priebke family lawyer, who went to SSPX who in turn agreed to hold a funeral for Priebke. The result was over 500 people protesting outside of the SSPX chapel.

This funeral does nothing help the image of the SSPX, especially among World War II survivors and Jews. They already look anti-semetic, Holocaust deniers because of Williamson.

Finally, a week or so ago members of the SSPX disrupted a commemoration of Kristallnacht, when the Nazi burned synagogues and wrecked Jewish homes and businesses. The SSPX member shouted that they did not want people who followed false gods in a Catholic cathedral. What group are they talking about? Jews. Remember those people? They share the same faith that Jesus and His family practised.

For those who think that the Catholic Church is supposed to be anti-semetic, here is Pope Gregory X's thoughts on the subject.

> Pope Gregory X's Papal Bull "Papal Protection of the Jews" issued October 7, 1272:
> 
> Gregory, bishop, servant of the servants of God, extends greetings and the apostolic benediction to the beloved sons in Christ, the faithful Christians, to those here now and to those in the future.
> 
> Even as it is not allowed to the Jews in their assemblies presumptuously to undertake for themselves more than that which is permitted them by law, even so they ought not to suffer any disadvantage in those \[privileges\] which have been granted them. \[This sentence, first written by Gregory I in 598, embodies the attitude of the Church to the Jew.\] Although they prefer to persist in their stubbornness rather than to recognize the words of their prophets and the mysteries of the Scriptures \[which, according to the Church, foretold the coming of Jesus\], and thus to arrive at a knowledge of Christian faith and salvation; nevertheless, inasmuch as they have made an appeal for our protection and help, we therefore admit their petition and offer them the shield of our protection through the clemency of Christian piety. In so doing we follow in the footsteps of our predecessors of blessed memory, the popes of Rome -- Calixtus, Eugene, Alexander, Clement, Innocent, and Honorius.
> 
> We decree moreover that no Christian shall compel them or any one of their group to come to baptism unwillingly. But if any one of them shall take refuge of his own accord with Christians, because of conviction, then, after his intention will have been manifest, he shall be made a Christian without any intrigue. For, indeed, that person who is known to have come to Christian baptism not freely, but unwillingly, is not believed to posses the Christian faith.
> 
> \[The Church, in principle, never approved of compulsory baptism of Jews.\]
> 
> Moreover no Christian shall presume to seize, imprison, wound, torture, mutilate, kill or inflict violence on them; furthermore no one shall presume, except by judicial action of the authorities of the country, to change the good customs in the land where they live for the purpose of taking their money or goods from them or from others.
> 
> In addition, no one shall disturb them in any way during the celebration of their festivals, whether by day or by night, with clubs or stones or anything else. Also no one shall exact any compulsory service of them unless it be that which they have been accustomed to render in previous times.
> 
> \[Up to this point Gregory X has merely repeated the bulls of his predecessors.\]
> 
> Inasmuch as the Jews are not able to bear witness against the Christians, we decree furthermore that the testimony of Christians against Jews shall not be valid unless there is among these Christians some Jew who is there for the purpose of offering testimony.
> 
> \[the Church council at Carthage, as early as 419, had forbidden Jews to bear witness against Christians; Justinian's law of 531 repeats this prohibition. Gregory X here -- in accordance with the medieval legal principle that every man has the right to be judged by his peers -- insists that Jews can only be condemned if there are Jewish as well as Christian witnesses against them. A similar law to protect Jews was issued before 825 by Louis the Pious (814 - 840) of the Frankish Empire.\]
> 
> Since it happens occasionally that some Christians lose their children, the Jews are accused by their enemies of secretly carrying off and killing these same Christian children and of making sacrifices of the heart and blood of these very children. It happens, too, that the parents of these very children, or some other Christian enemies of these Jews, secretly hide these very children in order that they may be able to injure these Jews, and in order that they may be able to extort from them a certain amount of money by redeeming them from their straits. \[Following the lead of Innocent IV, 1247, Gregory attacks the ritual murder charge at length.\]
> 
> And most falsely do these Christians claim that the Jews have secretly and furtively carried away these children and killed them, and that the Jews offer sacrifices from the heart and the blood of these children, since their law in this matter precisely and expressly forbids Jews to sacrifice, eat, or drink the blood, or to eat the flesh of animals having claws. This has been demonstrated many times at our court by Jews converted to the Christian faith: nevertheless very many Jews are often seized and detained unjustly because of this.
> 
> We decree, therefore, that Christians need not be obeyed against Jews in a case or situation of this type, and we order that Jews seized under such a silly pretext be freed from imprisonment, and that they shall not be arrested henceforth on such a miserable pretext, unless -- which we do not believe -- they be caught in the commission of the crime. We decree that no Christian shall stir up anything new against them, but that they should be maintained in that status and position in which they were in the time of our predecessors, from antiquity till now.
> 
> We decree in order to stop the wickedness and avarice of bad men, that no one shall dare to devastate or to destroy a cemetery of the Jews or to dig up human bodies for the sake of getting money. \[The Jews had to pay a ransom before the bodies of their dead were restored to them.\] Moreover, if any one, after having known the content of this decree, should -- which we hope will not happen -- attempt audaciously to act contrary to it, then let him suffer punishment in his rank and position, or let him be punished by the penalty of excommunication, unless he makes amends for his boldness by proper recompense. Moreover, we wish that only those Jews who have not attempted to contrive anything toward the destruction of the Christian faith be fortified by support of such protection ...
> 
> Given at Orvieto by the hand of the Magister John Lectator, vice-chancellor of the Holy Roman Church, on the 7th of October, n the first indiction \[cycle of fifteen years\], in the year 1272 of the divine incarnation, in the first year of the pontificate of our master, the Pope Gregory X.

Now most of you are saying "So what, SSPX often does things to embarrass or anger the Holy See." Well, I think these are definite example of the SSPX setting themselves up as an alternative to the Catholic Church. First, Fellay has give up all pretence of discussing reunion and openly attacks the Pope. Second, they gave an unrepentant Nazi (before his death, Priebke denied the Holocaust in a TV interview) a funeral, even though the Catholic Church offered a private service. Finally, their members are so filled with hatred for Jews that they disrupt a service commemorating one of the most horrific events in Jewish history and calling them followers of a false god. It should be obvious to most pople by now that SSPX is not in union with Rome, but has form and organization in opposition to it. As Father Emerson FSSP said:

>     The consecration of bishops, on the other hand, is a clear act of schism, of breaking with the head of the Church, of setting up a parallel hierarchy, setting up an independent church. Of course that is exactly what schism is. It is not to be a heretic, which is to say, no, the Pope is, not head of the Church. It is to say, yes, he is, but we are simply going to ignore that and go ahead on our own. Another part of schism, interestingly, is this: Even if one continues to say, yes, we are under the Pope, we obey the Pope, but it is then to break entirely with those who also are under the Pope, and that the Society of St. Pius X also has done very clearly since the break by considering utterly traditional groups like us (Priestly Fraternity of St. Peter), like the Abbey at La Barrou in France , to be now part of the Modernist conspiracy, if you like. In truth, we are in every way traditional, but we accept the Pope and we obey him. Therefore we are outside the pale. That, too, is classically the schismatic mentality. It’s clear the Society \[of St. Pius X\] has accepted the dangers of a real schism and is just plowing ahead.

I pray fervently that the leaders of the SSPX will see the error of their ways and return ot Rome, but I'm not going to hold my breath. If one of these days, SSPX is pulled aboard the barque of Peter kicking and screaming (a miracle as great or greater than the parting of the Red Sea), I will delete and renounce every comment I have ever made about the SSPX. However I doubt that day will ever come.
