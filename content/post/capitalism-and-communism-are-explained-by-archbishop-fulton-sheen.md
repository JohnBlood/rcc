---
title: "Capitalism and Communism are Explained by Archbishop Fulton Sheen"
date: "2012-10-21"
categories: 
  - "archbishop-fulton-j-sheen"
  - "capitalism"
  - "communism"
  - "tv"
  - "video"
tags: 
  - "archbishop-sheen"
---

In this video, Archbishop Fulton Sheen expounds on the importance of Capitalism and private property and the evil of Communism

\[youtube=http://www.youtube.com/watch?v=7ra0d\_1OVfQ\]
