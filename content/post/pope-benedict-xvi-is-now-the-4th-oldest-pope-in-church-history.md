---
title: "Pope Benedict XVI is Now the 4th Oldest Pope in Church History"
date: "2013-01-18"
categories: 
  - "pope-benedict-xvi"
  - "roman-catholic"
tags: 
  - "pope"
  - "pope-benedict-xvi"
  - "pope-clement-x"
  - "pope-leo-xiii"
  - "pope-pius-ix"
---

[![Six Oldest Popes 2013](images/sixoldestpopes2013-finished.png)](http://realromancatholic.files.wordpress.com/2013/01/sixoldestpopes2013-finished.png)Pope Benedict XVI continues to amaze me. I read the other day that he is now the fourth oldest pope in the history of the Roman Catholic Church. He just recently passed Blessed Pope Pius IX. Next year he will overtake Pope Clement X. However, it will be awhile before he reaches Pope Leo XIII's number one slot. (Pope Leo XIII died at 93.)

This is amazing because the then Cardinal Ratzinger was planning to retire in the early 2000s, but Pope John Paul II refused to accept his resignation several times. God obviously has other plans. Keep Pope Benedict XVI in your prayers as he continues to lead God's people.
