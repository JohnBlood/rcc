---
title: "Cardinal Dolan, the Man Who Must Never be Pope"
date: "2013-04-20"
categories: 
  - "cardinal-timothy-dolan"
  - "liberalism"
  - "modernism"
  - "video"
tags: 
  - "dolan"
---

Before the conclave took place, there was quite a bit of speculation that an American would be chosen to lead the Church. One name in particular led the list: Cardinal Timothy Dolan. On the surface, Dolan looks good. He's happy and jolly, seems to be relatively conservative, but once you scratch below the surface you find something different. Watch below and be thankful that Dolan was not elected.

\[youtube=http://www.youtube.com/watch?v=O9U-esned-c\]

\[youtube=http://www.youtube.com/watch?v=LRZQ399Ynj4\]
