---
title: "What the Hell are the Bishops at the Synod Thinking?"
date: "2015-10-24"
categories: 
  - "2015-synod-on-the-family"
---

\[caption id="attachment\_1068" align="alignright" width="300"\][![Archbishop-Paul-Andre-Durocher](https://realromancatholic.files.wordpress.com/2015/10/archbishop-paul-andre-durocher.png?w=300)](https://realromancatholic.files.wordpress.com/2015/10/archbishop-paul-andre-durocher.png) Archbishop Paul-Andre Durocher "Isn't this whole thing just too funny for words?"\[/caption\]

_This is **part two** of a series of three articles this week where I comment on the 2015 Synod on the Family in Rome. Hold onto your seats._

During the course of this Synod on the Family, a number of bishops and cardinals proposed ideas that either had nothing to do with the family or were heretical in nature. In this post, I'll list a number of problematic clerics to help you understand the problems at the Synod.

To start us off, **Archbishop Paul-Andre Durocher of Quebec** called for women deacons. What exactly does that have to do with the family? Absolutely nothing. The Archbishop claims that this will give women more opportunities in the Church. What he really means is that it would bring women priests one step closer to reality. I talked to a priest one time about the idea of women deacons and he said there was no problem with it because it wasn't really an ordained position in the Church. Besides they did it in the Early Church. My answer would be "Jesus picked 12 men to be His Apostles. Because of that the Catholic Church has always had a male clergy. Even St. John Paul II said that man could not introduce women clergy because it was a law created by God and therefore, unchangeable by man."

We can't forget **Cardinal Walter Kasper**, who wants desperately to give Holy Communion to those in sin. Kasper has been campaigning for years to allow Holy Communion for the civilly divorced. Never mind the fact that Jesus said in the Gospel "What therefore God hath joined together, let no man put asunder." (St. Matthew 19:6, DRB) This may prohibit divorce, but to it seems that the Cardinal is more interested in allowing those in sin to approach the Altar of God than to follow Christ's commands.

 **Cardinal Oswald Gracias of Mumbai, India,** caused quite a few people to raise their eyebrows when he granted an interview to New Ways Ministries. New Ways Ministries  was condemned by Cardinal Ratzinger as head of the Congregation for the Doctrine of the Fain in 1999 for opposing the Catholic teaching on homosexuality. Cardinal Gracias told the gay-friendly group that the synod should have listened to same-sex couples because they so much to offer. This cleric seems to forget that when God created the original family, He created Adam and Even, not Adam and Larry. Same-sex couples are two people who have decided to give into temptation and live in sin. It almost seems like, in order to get ahead in the Church, you must oppose its doctrine.

**Bishop Peter Doyle of Northampton, England,** has complained that the Synod has not addressed the plight of homosexuals. Um, Your Excellency, it's called the Synod on the Family for a reason: you're supposed to be discussing the plight of _**FAMILIES**_ in the modern world. The Church counsels those with same-sex attractions to live chaste lives, so they can grow in holiness. There are a number of programs in the Church designed to help this with such attractions. What more do you want? The Church cannot approve that which God has declared sinful.

From our own country, we have **Archbishop Blase Cupich of Chicago** who pledged to give Holy Communion to gay couples. Again, these clerics seem to ignore the fact that gay couples are living in sin and therefore have no right to receive Holy Communion. To do so would be a grave sacrilege on both the part of the person going to Communion and the priest knowingly giving it to them.

Finally, I would like to mention **Cardinal Deneels of Belgium**. As far as I know, he has not proposed any goofy ideas, but this former primate of Belgium has no right to be at the Synod. He knew that a friend and fellow bishop was a pedophile and covered up for him. In 1990, he counseled the King of Belgium to approve abortion, even though the monarch had misgivings. He recently congratulated the government on approving same-sex "marriages". During his reign, Sunday Mass attendance dropped to 11% and has not recovered. (In the US, it is about 25%.) Any one of these errors should have kept him from being invited to the Synod, but Pope Francis picked him anyway.

If you look at the prelates I listed here, you might think that the conservative bishops outnumber the far-out ones. That may not be true. The German bishops have pledged to support Kasper's Communion for the divorced plan. There are other groups of prelates lining up behind different outrageous proposals. We must pray and pray hard for strength for those who have pledged to uphold tradition (such as the Latvian and Polish bishops) against those who would try to change doctrine. In the end, the latter will not succeed because God promised that Hell would not prevail. For now, we must do what we can to prevent souls from being lost to the Devil.

In my final post, I will take a look at the man who made this all possible, Pope Francis. Stay Tuned.

* * *

If you found this article interesting, be sure to share and subscribe to my email newsletter.
