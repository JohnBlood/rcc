---
title: "It's Time for Francis to Resign the Papacy"
date: "2018-08-28"
categories: 
  - "current-church-crisis"
  - "michael-voris"
  - "pope-francis"
  - "video"
tags: 
  - "mccarrick"
  - "sexual-abuse"
coverImage: "pope_francis_with_cardinal_mccarrick.jpg"
---

[![](https://realromancatholic.files.wordpress.com/2018/08/pope_francis_with_cardinal_mccarrick.jpg?w=640)](https://realromancatholic.files.wordpress.com/2018/08/pope_francis_with_cardinal_mccarrick.jpg)

It's been a while since I published anything on this blog. I've been planning to return for a while, but have been very busy. There have been many things that I wanted to say about the current papacy in month gone by. Now I definitely have something to say:

**FRANCIS MUST RESIGN THE PAPACY**

There are many reasons for this, including Francis' constant attempts to dismantle Catholic doctrine and the question of [the validity of his election](https://abyssum.org/2018/07/30/an-open-letter-to-the-cardinals-of-the-holy-roman-catholic-church-and-other-catholic-christian-faithful-in-communion-with-the-apostolic-see/). However, there is now a stronger reason.

For those who have been living under a rock for the last couple of days, [Archbishop Carlo Maria Viganò](https://www.lifesitenews.com/news/former-us-nuncio-pope-francis-knew-of-mccarricks-misdeeds-repealed-sanction) (former US apostolic nuncio from 2011-2016) published an [11-page letter](https://s3.amazonaws.com/lifesite/TESTIMONYXCMVX-XENGLISH-CORRECTED-FINAL_VERSION_-_G-2.pdf) in which he accused Francis of knowing of McCarrick's predatory nature and failing to punish him. In fact, Viganò states that Pope Benedict had punished McCarrick for his crimes, but Francis lifted the restrictions. Because one must look "merciful"!

Francis has [refused to answer questions](https://onepeterfive.com/pope-refuses-to-answer-questions-about-vigano-accusations-as-another-former-vatican-diplomat-confirms-the-report/) about Viganò's accusations.

\[caption id="attachment\_1104" align="alignleft" width="640"\][![](https://realromancatholic.files.wordpress.com/2018/08/vigano.jpg?w=640)](https://realromancatholic.files.wordpress.com/2018/08/vigano.jpg) Archbishop Viganò\[/caption\]

## Who is Viganò'?

OnePeterFive reported that a [priest from Detroit named Fr. Carlos Martins](https://www.facebook.com/photo.php?fbid=1082759875233329&set=a.128785033964156&type=3&theater) did some research on Archbishop Viganò. Here is what he found:

> I just spent the last two hours on the phone with a friend in the Vatican Curia. He said that the news of Archbishop Viganò has hit the Curia like an atomic bomb. Two things are universally noted regarding Viganò: 1) He is highly respected as a professional, and 2) His Curial positions gave him clear access to the damning information he reported. In other words, he is not a hack, and he is not relying on rumor. This makes his report absolutely worthy of belief.
> 
> Viganò always had a reputation for being a combatant of internal Vatican corruption. In fact, during the Vatican leaks scandal, whistle-blowing reports that he authored were among the main documents that were leaked. This was an attempt by the persons he outed to pre-empt the report’s impact and suck the energy out of the attempt to investigate their claims. Naturally, the subsequent energy went into investigating the Vatileaks situation in general, and Viganò was exiled as Nuncio to the United States for being a trouble maker who produced “erroneous assessments” (words from a joint statement issued by Cardinal-President Emeritus Giovanni Lajolo, President Giuseppe Bertello, Secretary-General Giuseppe Sciacca and former Vice Secretary-General Giorgio Corbellini on behalf of the Governatorate of the Vatican). To put this kind of demotion in perspective, as Delegate for Pontifical Representations—a position from which he was THEN PROMOTED to Secretary General of the Governatorate— Viganò was in charge of all the Apostolic Nunciatures in the world. Thus, when garbage was reported to the Holy See on a bishop or Cardinal—like it was with McCarrick of Washington, DC—Viganò was the first to know about it, because his desk is where the information landed. For him to be demoted as the Nuncio to the USA, from having been promoted to as the Vatican’s number 3 administrator behind the pope, was severe, to say the least. In other words, Viganò is not a hack, but a highly respected individual who had been regularly promoted for doing his job well.
> 
> In the words of the Curial official I spoke with this afternoon, what Viganò has reported “makes the Borgia popes look like saints.” The feeling in the Curia right now is that the response of Viganò’s enemies will to try to discredit him personally, both because of the impeccability of Viganò’s character and the impossibility of his having interpreted the facts incorrectly. Their only hope will be to try to take energy away from the perversion and corruption that he uncovered. They will likely state that he is a bitter man who is seeking personal aggrandizement after having been exiled from Rome. When this occurs, don’t buy into it. Viganò is retired. He has nothing personally to gain from this.

Of course, the majority of professional "catholic" news sites are attacking Viganò, calling his revelations an attempted coup against Francis. They have also leveled accusations of past wrongdoing at him, as well. Archbishop Viganò has released [documents to clear his name](https://www.lifesitenews.com/news/vigano-issues-new-statement-documents-to-clear-his-name-of-false-charges).

Interestingly, [a number of high-ranking church leaders](https://onepeterfive.com/bishops-offer-support-for-vigano-six-prelates-voice-their-concern/) are clearly worried about Viganò's accusations and have given him their support. These include:

- Cardinal Raymond Burke
- Bishop Thomas Olmsted of Phoenix, Arizona
- Bishop Robert Morlino of Madison, Wisconsin
- Archbishop Allen Vigneron of Detroit, Michigan
- Bishop Joseph Strickland of Tyler, Texas
- Bishop Athanasius Schneider of Astana, Kazakhstan

## Conclusion

Essentially, Francis covered up for a sexual predator. He allowed a wolf to walk among his flock and attack the vulnerable sheep. This is unforgivable. Especially for someone who constantly talks about zero tolerance for sex abuse. In fact, Francis has said several times that those in authority who have covered for sex abusers, should be removed from authority. I bet he never thought those words would come around to bite him.

I will let Michael Voris sum up what that means.

http://www.youtube.com/watch?v=yScaroYHlVw
