---
title: "The Fat Lady Has Yet To Sing: the Vatican and the 21st Century Protestants (SSPX)"
date: "2012-05-02"
categories: 
  - "discipline"
  - "latin-mass"
  - "orthodox"
  - "pope-benedict-xvi"
  - "protestants"
  - "return-to-tradition"
  - "roman-catholic"
  - "sspx"
  - "tradition"
---

[![](images/sspxschismatic.jpg "SSPXSchismatic")](http://realromancatholic.files.wordpress.com/2012/05/sspxschismatic.jpg)One of the biggest religious news stories in the last month has been the conclusion of talks between the Vatican and the schismatic, break-off SSPX group. Here are my thoughts on the subject.

Before I do I want to make a few things perfectly clear. I love the Latin Mass. In fact, my brother and I serve it. I also believe that the English or Novus Ordo Mass is valid. I also believe that the Roman Catholic Church is the one true Church founded by Jesus Christ to save souls and Pope Benedict XVI is its head. Now that that's out of the way, let's get down to cases.

A lot of the articles I read about the conclusion of talks between the Vatican and SSPX were hopeful and spoke of an imminent reunion. There was one article however that had a more realistic take, which I agree with.

Here are a couple of the ideas from the article.

First, it's not over until the fat lady sings. This problem between SSPX and Rome has been going on for 40 years. There have been multiple times during that time period when it look as though the SSPX would come back, but each time they turned down the deal offered by the Vatican. For example, back in 1988 SSPX head Lefebvre and Cardinal Ratzinger signed an agreement only to have Lefebvre change his mind the next day because he wanted authority to ordain four bishops instead of just one. Since its been going on for this long, what makes anyone think this will change anytime soon.

Second, Catholics do not dialogue or negotiate with the Pope, they submit to him. Only those who are outside the Church, such as the Orthodox and Protestants, have to negotiate. People involved in the SSPX can call it anything they want, but they are essential trying to talk their way into the Church.

The SSPX (and people like them) are the 21st century Protestants. Just like the 16th century variety, these modern Protestants protest the legitimate authority of the Church because they do not agree with it. Instead of obeying the proper Church authorities (like true Catholics), they chose to do their own thing and even set up their own hierarchy.

In March of 2009, Pope Benedict XVI said, "Until the doctrinal questions are clarified, the Society has no canonical status in the Church, and its ministers - even though they have been freed of the ecclesiastical penalty - do not legitimately exercise any ministry in the Church." Until this changes, the SSPX is not a Roman Catholic organization and should be avoided.
