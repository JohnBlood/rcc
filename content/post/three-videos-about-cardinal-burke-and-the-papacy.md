---
title: "Three Videos About Cardinal Burke and the Papacy"
date: "2013-03-11"
categories: 
  - "cardinal-burke"
  - "roman-catholic"
  - "video"
tags: 
  - "cardinal-burke"
  - "catholic"
  - "pope"
---

Here are three videos about Cardinal Burke and his suitability for the papacy.

\[youtube=http://www.youtube.com/watch?v=F-unS1cVmAI\]

This first video was made by Michael Voris of Real Catholic TV in April of 2012. Even back then people were talking about Burke being a great choice for pope.

\[youtube=http://www.youtube.com/watch?v=xHObi2OvYuc\]

The second video was make a couple of days ago, again by Michael Voris. This video lists Cardinal Burke's accomplishments which would make him a wonderful pope.

\[youtube=http://www.youtube.com/watch?v=5pnvGWzSsNg\]

This final video is a talk that Cardinal Burke gave a year ago at a Call to Martyrdom conference in Sterling Heights, Michigan. Just imagine if we heard that speech from the pope's balcony in St. Peter's Square.

As the conclave nears, it is time to increase our prayers and storm heaven that Cardinal Burke is elected our next pope.
