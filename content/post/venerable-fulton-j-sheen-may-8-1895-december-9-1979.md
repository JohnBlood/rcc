---
title: "Venerable Fulton J. Sheen (May 8, 1895 – December 9, 1979)"
date: "2014-03-16"
categories: 
  - "archbishop-fulton-j-sheen"
  - "saints"
tags: 
  - "archbishop-sheen"
  - "saint"
---

[![ArchbishopSheen1](images/archbishopsheen1.png)](http://realromancatholic.files.wordpress.com/2014/03/archbishopsheen1.png)Venerable Fulton J. Sheen was born on May 8, 1895 in El Paso, Illinois to Newton and Delia Sheen. He was baptized Peter John Sheen, but later became known by his mother’s maiden name: Fulton. He was the oldest of four boys. After his family moved to Peoria, Sheen was an altar boy at St. Mary’s Cathedral. He graduated from high school as a valedictorian in 1913. After attending St Paul Seminary in Minnesota, Sheen was ordained in 1919. He went on to earn a doctorate in philosophy at the Catholic University in Belgium. He was the first American to win the Cardinal Mercier award for philosophy.

After he returned to America, Sheen received offers from Oxford and Columbia to teach philosophy. However, at the request of Bishop Dunne of Peoria, Sheen took on the duties of a parish priest. Nine months later, Dunne sent Sheen to teach philosophy at the Catholic University of America, which he did until 1950. In 1951, Sheen was consecrated a bishop and served as auxiliary bishop of the Archdiocese of New York for the next 15 years.

[![ArchbishopSheen2](images/archbishopsheen2.png)](http://realromancatholic.files.wordpress.com/2014/03/archbishopsheen2.png)Sheen’s communication skills would go far to help the Church. He published the first of 73 books in 1925. In 1930, he began a weekly Sunday night broadcast entitled “The Catholic Hour”. By the time it finished twenty years later, the show had an audience of four million. Sheen began a weekly television program in 1950 entitled “Life is Worth Living”. It was given the graveyard slot of Tuesday nights at 8:00pm, so it would not compete with rating giants, such as Milton Berle and Frank Sinatra. However, the show’s rating grew quickly. Berle joked, “He used old material, too.” In 1952, Sheen won an Emmy Award for his show. He said, “I feel it is time to pay tribute to my four writers: Matthew, Mark, Luke and John.” In one of his best remembered shows was in February of 1953 when he denounced Joseph Stalin. He ended the show by saying, “Stalin must one day meet his judgement.” Stalin suffered a stroke and died within a week. The show ended in 1957

Sheen became the director of the Society for the Propagation of the Faith in the US. Eight years later, he was appointed the Bishop of Rochester, New York.  From 1961 to 1968, he hosted another TV show entitled The Fulton Sheen Show. Sheen was responsible for the conversion of several notable figures. The people he converted include: agnostic Heywood Broun, politician Clare Boothe Luce, Henry Ford II, Communist Louis F. Budenz and actress Virginia Mayo.

[![ArchbishopSheen3](images/archbishopsheen3.png)](http://realromancatholic.files.wordpress.com/2014/03/archbishopsheen3.png)On the 50th anniversary of his ordination in 1969, Sheen resigned as bishop of Rochester and was appointed archbishop by Pope Paul VI. In October of 1979, Pope John Paul II visited the United States. Upon meeting Sheen at St. Patrick’s Cathedral in New York, the Pope embraced him and said, “You have written and spoken well of the Lord Jesus Christ. You are a loyal son of the Church.” Sheen died on December 9. 1979. Billy Graham said of Sheen’s death, “(His death was) a great loss for the nation and both the Catholic and Protestant churches...He broke down many walls of prejudice between Catholics and Protestants. I mourn his death and look forward to our reunion in heaven.” Pope Benedict XVI declared Archbishop Sheen Venerable in June 2012.
