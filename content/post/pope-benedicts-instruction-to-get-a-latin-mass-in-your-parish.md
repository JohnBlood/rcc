---
title: "Pope Benedict’s Instruction to Get a Latin Mass in Your Parish"
date: "2010-12-06"
categories: 
  - "bishops"
  - "motu-propio"
  - "pope-benedict-xvi"
  - "priest"
  - "roman-catholic"
  - "sommorum-pontificum"
  - "tradition"
tags: 
  - "catholic"
  - "latin"
  - "mass"
  - "popebenedict"
  - "romancatholic"
  - "tridentine"
---

Pope Benedict continually amazes me.  He has brought back the Latin Tridentine Mass and is in the process of bringing the  Anglicans into the Church.  (I will speak in depth about the later topic in the future.)

Unfortunately for some who are yearning for the Latin Mass, there are priests who refuse to have the Latin Mass in their church or only have it infrequently.  Thankfully, Pope Benedict gave us instruction on how to get the Latin Mass in our parish churches in his motu propio [Sommorum Pontificum](http://www.sanctamissa.org/en/resources/summorum-pontificum.html) issued on July 7, 2007 and became law on September 14, 2007.

[![Latin Mass](images/latinmass_thumb.png "Latin Mass")](http://realromancatholic.files.wordpress.com/2010/12/latinmass.png)First Pope Benedict starts out by stating that those who seek the Latin or Extraordinary form of the Mass are not seeking to case division (as some liberals in the Church say).

> “The Roman Missal promulgated by Paul VI is the ordinary expression of the 'Lex orandi' (Law of prayer) of the Catholic Church of the Latin rite. Nonetheless, the Roman Missal promulgated by St. Pius V and reissued by Bl. John XXIII is to be considered as an extraordinary expression of that same 'Lex orandi,' and must be given due honour for its venerable and ancient usage. These two expressions of the Church's Lex orandi will in no any way lead to a division in the Church's 'Lex credendi' (Law of belief). They are, in fact two usages of the one Roman rite.” (Article 1)

He goes on to state that priests may use one of the two forms of the Mass without needing the permission of the bishop or the Vatican.

> “For such celebrations, with either one Missal or the other, the priest has no need for permission from the Apostolic See or from his Ordinary.” (Article 2)

Next, the Pope sets up the requirements for Latin Masses in parishes.

![Pope Benedict's office](images/popebenedictoffice_thumb.png "Pope Benedict's office")

> “In parishes, where there is a stable group of faithful who adhere to the earlier liturgical tradition, the pastor should willingly accept their requests to celebrate the Mass according to the rite of the Roman Missal published in 1962…” (Article 5, § 1)

However, if the parish priest does not “willingly accept their request”, they must take their plea to a higher authority.

> “If a group of lay faithful, as mentioned in art. 5 § 1, has not obtained satisfaction to their requests from the pastor, they should inform the diocesan bishop. The bishop is strongly requested to satisfy their wishes.” (Article 7)

If the bishop is not able to help, the people have to go higher yet.

> “If he \[the bishop\] cannot arrange for such celebration to take place, the matter should be referred to the Pontifical Commission "Ecclesia Dei".” (Article 7)

Pope Benedict also included an option for the bishop to create a parish or appoint a chaplain for the sole purpose of fulfilling the needs of those who prefer the beauty of the pre-Vatican II liturgy.[![Ratzinger saying the Latin Mass](images/ratzingerlatinmass_thumb.png "Ratzinger saying the Latin Mass")](http://realromancatholic.files.wordpress.com/2010/12/ratzingerlatinmass.png)

> “The ordinary of a particular place, if he feels it appropriate, may erect a personal parish in accordance with can. 518 for celebrations following the ancient form of the Roman rite, or appoint a chaplain, while observing all the norms of law.”  (Article 10)

Not only that, but Pope Benedict also allowed the use of the older rites for the other sacraments as well.

> “For faithful and priests who request it, the pastor should also allow celebrations in this extraordinary form for special circumstances such as marriages, funerals or occasional celebrations, e.g. pilgrimages.”  (Article 5, § 3)
> 
> “The pastor, having attentively examined all aspects, may also grant permission to use the earlier ritual for the administration of the Sacraments of Baptism, Marriage, Penance, and the Anointing of the Sick, if the good of souls would seem to require it.  Ordinaries are given the right to celebrate the Sacrament of Confirmation using the earlier Roman Pontifical, if the good of souls would seem to require it.”  (Article 9, § 1-2)

You now have the information and the words of the Holy Father to back you up.  Go to your priests and ask for the Latin Tridentine Mass.  If he turns you away, follow the steps that the Pope laid out.  All the while pray and have faith.  The Latin Mass will return to the prominence it once had.
