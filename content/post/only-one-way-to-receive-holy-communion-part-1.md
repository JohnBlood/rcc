---
title: "Only One Way to Receive Holy Communion - Part 1"
date: "2011-01-09"
categories: 
  - "church-fathers"
  - "holy-communion"
  - "mass"
  - "orthodox"
  - "pope-benedict-xvi"
  - "priest"
  - "priest"
  - "roman-catholic"
tags: 
  - "catholic"
  - "communion"
  - "orthodox"
  - "pope"
---

A couple of day ago, I ran across an article on a “Catholic” website entitled “Why I’m Giving Up Communion On the Tongue” by Danielle Bean.  I had to do a double take.  The fact that someone was actually saying this left me very upset, so upset in fact that I decided to write this series of posts.

Before I start, let me say here and now that there is only one way to receive Holy Communion, on the tongue while kneeling.  This is the way it has always been, until very recently, and that is the way it should always be.[![PopeBenedictCommunion1](images/popebenedictcommunion1_thumb.png "PopeBenedictCommunion1")](http://realromancatholic.files.wordpress.com/2011/01/popebenedictcommunion1.png)

First, I’ll give the writer’s words.  Then, I’ll counter them with the words of the Church.

Bean’s article can be summed up by the following quote from her article:

> I have always received the Eucharist on the tongue…This is not something I usually get all political or righteous about. I understand that many devout people hold different opinions on this topic and that “good” Catholics are free to receive on the hand or on the tongue. For me, though, receiving on the tongue has always felt like the most appropriate way to recognize and respect Christ’s real presence in the Eucharist.
> 
> But now I’m not so sure anymore.
> 
> While I still very much prefer to receive on the tongue, I am afraid that option is becoming a less reasonable choice for me. And, ironically, it’s becoming a less respectful way to receive the Eucharist.
> 
> At the Masses I attend, more often than not, I receive Communion from an Extraordinary Minister of Holy Communion. These EMHCs, more often than not, have no idea what to do when a person presents herself to receive Communion on the tongue. I am not blaming them for this problem so much as whatever training they have received…I know I could “solve” this problem by only attending Mass in the Extraordinary Form where receiving on the tongue while kneeling is the norm. But that is just not a realistic option for me.
> 
> I could be stubborn and insist upon receiving on the tongue because _I have a right to_, even when the challenges it causes become a distraction to myself and others. But that doesn’t seem like something Christ would want me to do.

**Communion in the Hand**

The following is what the early Church Fathers thought concerning receiving Holy Communion in the Hand.

> **Pope St. Leo the Great** (440-461), already in the fifth century, is an early witness of the traditional practice. In his comments on the sixth chapter of the Gospel of John, he speaks of Communion in the mouth as the current usage: "One receives in the mouth what one believes by faith." (_Serm._91.3) Furthermore, in the ninth century the _Roman Ordo_ clearly shows that Communion on the tongue was the manner of reception. The oft-quoted reference of St. Cyril of Jerusalem is quite suspect, because what follows his famous quote is odd, superstitious, and even irreverent to Catholic thought. This has led scholars to question the authenticity of the text, that perhaps the saint's successor was really responsible for this odd statement, the Patriarch John, who succeeded St. Cyril. But this John was of suspect orthodoxy, which we know from the correspondence of St. Epiphanius, St. Jerome, and St. Augustine. So if the quote is genuine, it most likely is attributed to the Nestorian Patriarch John, which would explain the oddity of the text. The fact that St. Cyril is quoted to the exclusion of Pope St. Leo the Great, Pope St. Sixtus I, the Council of Trent, and centuries of Church tradition, is a prime example of the historical revisionism and dumbing-down of the modernists. Just a sampling of _reliable_ historical evidence is enough to demonstrate the consistent position of the Church regarding Communion in the hand:  **[![Princess Grace Kelly receives Holy Communion](images/princessgracekellycommunion_thumb.png "Princess Grace Kelly receives Holy Communion")](http://realromancatholic.files.wordpress.com/2011/01/princessgracekellycommunion.png)**
> 
> **Pope St. Sixtus I** ( 115-125): "it is prohibited for the faithful to even touch the sacred vessels, or receive in the hand";
> 
> **Origen** (185-232 A.D.): "You who are wont to assist at the divine Mysteries, know how, when you receive the body of the Lord, you take reverent care, lest any particle of it should fall to the ground and a portion of the consecrated gift _(consecrati muneris)_ escape you. You consider it a crime, and rightly so, if any particle thereof fell down through negligence." (_13th Homily on Exodus_);
> 
> **St. Basil the Great** (330-379), one of the four great Eastern Fathers, considered Communion in the hand so irregular that he did not hesitate to consider it a grave fault (_Letter_ 93);
> 
> **The Council held at Saragozza** (380), it was decided to punish with **_excommunication_** anyone who dared to continue the practice of Communion in the hand;
> 
> **The local council at Rouen**, France (650) stated, “Do not put the Eucharist in the hands of any layman or laywomen but only in their mouths”;
> 
> **The Council of Constantinople** (692) which was known as _in trullo_ (not one of the ecumenical councils held there) prohibited the faithful from giving Communion to themselves. It decreed an **_excommunication_** of one week’s duration for those who would do so in the presence of a bishop, priest or deacon;
> 
> **Council of Trent**: "To omit nothing doctrinal on so important a subject, we now come to speak of the minister of the Sacrament, a point, however, on which scarcely anyone is ignorant. The pastor then will teach, that to priests alone has been given power to consecrate and administer the Holy Eucharist. That the _unvarying practice of the Church_ has also been, that the faithful receive the Sacrament from the hand of the priest, and that the priest communicate himself, has been explained by the Council of Trent; and the same holy Council has shown that this practice is always to be scrupulously adhered to, stamped, as it is, with the authoritative impress of Apostolic tradition, and sanctioned by the illustrious example of our Lord himself, who, with His own hands, consecrated and gave to His disciples, His most sacred body. To consult as much as possible, for the dignity of this so August a Sacrament, not only is its administration confided exclusively to the priestly order; but the Church has also, by an express law, prohibited any but those who are consecrated to religion, unless in case of necessity, to touch the sacred vessels, the linen or other immediate necessaries for consecration. Priest and people may hence learn, what piety and holiness they should possess who consecrate, administer, or receive the Holy of Holies." (_Council of Trent,_ Session 13, Chapter 8 )![St Jerome](images/stjeromecommunion.png "St Jerome")

Here are quote from more recent leaders in the Church on the same subject.

> "There is an apostolic letter on the existence of a special valid permission for this \[Communion in the hand\]. But I tell you that I am not in favor of this practice, nor do I recommend it." - **His Holiness Pope John Paul II, responding to a reporter from _Stimme des glaubens_ magazine, during his visit to Fulda (Germany) in November 1980.**
> 
> Holy Communion received on the tongue "signifies the reverence of the faithful for the Eucharist ... provides that Holy Communion will be distributed with due reverence ... is more conducive to faith, reverence and humility.... It \[Communion in the hand\] carries certain dangers with it which may arise from the new manner of administering holy Communion: the danger of a loss of reverence for the August sacrament of the altar, of profanation, of adulterating the true doctrine." **\- Pope Paul VI in his instruction _Memoriale Domini_ (May 29, 1969)**
> 
> "Wherever I go in the whole world, the thing that makes me the saddest is watching people receive Communion in the hand." **\- As reported by Fr. George Rutler in his 1989 Good Friday sermon at St. Agnes Church, New York. When Mother Teresa of Calcutta was asked by Fr. Rutler, "What do you think is the worst problem in the world today?" without pausing a second she gave the above reply. She stated that to her knowledge, all of her sisters receive  Communion only on the tongue.**
> 
> "Behind Communion in the hand—I wish to repeat and make as plain as I can—is a weakening, a conscious, deliberate weakening of faith in the Real Presence.... Whatever you can do to stop Communion in the hand will be blessed by God.” **\- Fr. Hardon, S.J., November 1st, 1997 Call to Holiness Conference in Detroit, Michigan, panel discussion.**
> 
> "There can be no doubt that Communion in the hand is an expression of the trend towards desacralization in the Church in general and irreverence in approaching the Eucharist in particular.... Why—for God's sake—should Communion in the hand be introduced into our churches when it is evidently detrimental from a pastoral viewpoint, when it certainly does not increase our reverence, and when it exposes the Eucharist to the most terrible diabolical abuses? There are really no serious arguments for Communion in the hand. But there are the most gravely serious kinds of arguments against it."  **\- Dietrich von Hildebrand (called a “20th century doctor of the Church” by Pope Pius XII), in an article entitled "Communion in the Hand should be Rejected," November 8, 1973.**
> 
> [![Cardinal Ratzinger gives PopeJohn Paul Holy Communion](images/popebenedictpopejohnpaulcommunion_thumb.png "Cardinal Ratzinger gives PopeJohn Paul Holy Communion")](http://realromancatholic.files.wordpress.com/2011/01/popebenedictpopejohnpaulcommunion.png) "With Communion in the hand, a miracle would be required during each distribution of Communion to avoid some Particles from falling to the ground or remaining in the hand of the faithful.... Let us speak clearly: whoever receives Communion in the mouth not only follows exactly the tradition handed down but also the wish expressed by the last Popes and thus avoids placing himself in the occasion of committing a sin by negligently dropping a fragment of the Body of Christ.” **\- Bishop Juan Rodolfo Laise of San Luis, Argentina in his book _Communion in the Hand: Documents and History_.**

Keep your eyes open for part 2.
