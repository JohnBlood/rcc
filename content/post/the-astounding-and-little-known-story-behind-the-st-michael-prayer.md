---
title: "The Astounding and Little Known Story Behind the St. Michael Prayer."
date: "2012-06-29"
categories: 
  - "history"
  - "pope"
  - "pope-leo-xiii"
  - "religious-freedom"
  - "return-to-tradition"
  - "roman-catholic"
  - "saints"
  - "st-michael"
  - "tradition"
tags: 
  - "st-michael"
---

[![](http://realromancatholic.files.wordpress.com/2012/06/popeleoxiii.png?w=220 "PopeLeoXIII")](http://realromancatholic.files.wordpress.com/2012/06/popeleoxiii.png)Before Vatican II, the St. Michael prayer was said after every Mass. This custom was started by Pope Leo XIII. Below is the story behind the devotion and it is quite astounding to say the least.

The following is taken from _Our Glorious Popes_ published in 1955 by the Slaves of the Immaculate Heart of Mary:

> Pope Leo XIII, who died in our own century, July 20, 1903 (aged 93), had a vision during his pontificate of Lucifer and his devils. He saw their fearful triumphs in all the countries of the world in the days soon to come. He beheld their evil glee and unholy mockery as they ravished the Mystical Body of Christ, stilled heavenly espousals in the hearts of maidens, muted the voices of priests and bishops, imprisoned the Popes, and silenced the song of monks and nuns in monasteries and convents grown empty of vocations.
> 
> The vision, given the Holy Father one morning after his Mass, was so beyond bearing, so overpowering in its sheer unrelieved, inexpressible evil, that it stopped the heart of Christ’s Vicar. The Pope lost consciousness. His frail body sank to the floor. The physicians who rushed to him could not, for long moments, hear the beat of his heart or feel the throb of his pulse. When they were about to pronounce him dead, he awoke, in great labor and groaning, in overwhelming pain of spirit.
> 
> He told, as much as such things lend themselves to words, what it was that he had seen. He told that when he was filled with so much terror for the world that he thought he would die of it, there appeared to him, beside the maliciously triumphant Satan, the gloriously shining Saint Michael, the Archangel.  And when he recovered, Pope Leo XIII wrote letters of warning to the bishops all over the world. He fearlessly named the enemy behind whose deceiving mask Satan looked out upon the twentieth-century world and plotted its destruction. In the encyclical letter, _Humanum Genus,_ he instructed his bishops as to what they must teach and do, before it would be too late, in order to overcome Lucifer and his devils.
> 
> It was then that Pope Leo XIII drafted, to be added to the prayers at the end of Low Mass and said by the priests and the faithful over the whole world, the intercession to Saint Michael which is now so familiar to us all:
> 
> > _Saint Michael the Archangel, defend us in battle. Be our protection against the wickedness and snares of the devil. May God rebuke him, we humbly pray. And do thou, 0 prince of the heavenly host, by the divine power of God, cast into hell satan and all evil spirits who roam now throughout the world seeking the ruin of souls_.

Recently, several bishops in the US (Paprocki and Jenky) have returned the St. Michael its proper place at the end of Mass. If we are to win this battle for  religious freedom, we desperately need the aid of that great archangel: St. Michael. St. Michael the Archangel, Pray for Us.
