---
title: "The Heresy of Sola Scriptura is Unbiblical and Destructive"
date: "2013-10-09"
categories: 
  - "five-solas"
  - "protestant"
  - "sola-scriptura"
tags: 
  - "scripture-alone"
  - "the-canon-of-scripture"
---

The heresy of Sola Scriptura (Scripture Alone) was an invention of the 16th century, which has no actual basis in Scripture. Most Protestants point to 2 Timothy 3:16 as the Biblical basis for this heresy, but that does not work for several reasons.

> “All scripture is given by inspiration of God, and is profitable for doctrine, for reproof, for correction, for instruction in righteousness.” 2 Timothy 3:16 (KJV)

First, in this verse St. Paul does not say that Scripture alone should be followed. He says that it is “profitable”. Profitable means useful, but not sufficient.

Second, at the time that this epistle was written (between 64 to 68 AD) the Bible as we know it today was not in existence. The last book of the New Testament was written about 100 AD and the canon of Scripture was not compiled until the 4th century. The Scripture that St. Paul was referring to was the Old Testament or the Torah.

To say that “if it is not in the Bible, it is not true” is just plain foolish because the Bible is not an exhaustive record of everything that happened during Bible times. St. John says, “But there are also many other things which Jesus did which, if they were written every one, the world itself, I think, would not be able to contain the books that should be written.” (St. John 21:25, DRB) If everything that Jesus did was not written down, how can you be sure what He condemned and what He did not?

There have been people down through history who did not have Bibles. Take, for example, the early Christians. As stated before, the final book of the Bible was not finished until about 100 AD. The canon of the Scriptures was not formally declared by the Church until the 4th century. After that Bibles were scarce because they had to be laboriously copied by Catholic monks. It wasn’t until about 1440 AD, when Gutenberg invented the printing press, that Bibles were widely owned. Now, what I would like to know is: Would supporters of Sola Scriptura consider these people to be Christian? After all, they lived much closer to the time of Christ, but did not have the Scriptures. The only way they would have received the Gospel would have been through word of mouth and tradition.

Tradition is an integral part of our faith and it’s transmission to us. In 1 Corinthians 11:2, St. Paul said, “Now I praise you, brethren, that in all things you are mindful of me and keep my ordinances as I have delivered them to you.” (DRV) Later in 2 Thessalonians 2:14, he said, “Therefore, brethren, stand fast: and hold the traditions, which you have learned, whether by word or by our epistle.” In both instances, the Apostle mentions word of mouth.

All law-giving writings need to have an interpreter or the meaning will easily be misconstrued or misunderstood. Take, for example, the Constitution of the United States. The Supreme Court was created by the Constitution to do exactly that. In the case of the Bible, the Catholic Church is the interpreter of the Bible. After all Jesus did say to St. Peter, “That thou art Peter; and upon this rock I will build my church, and the gates of hell shall not prevail against it.” (St. Matthew 16:18, DRV) He did not say this about the Bible. In fact, He never mentioned written word when He talked about teaching the Gospel. St. Augustine, who lived between 354-430, said, “I would not believe the Gospel itself, if the authority of the Catholic Church did not move me to do so.” As I heard once, anyone who interprets the Bible for themselves becomes a pope unto themselves.

Sola Scriptura has been very destructive to the body of Christian believers. It has closed the minds of many people to the works of the Fathers of the Church, as well as many mystics, saints and theologians.

Many heresiarchs down through history have used the Bible to justify their heresy. Take the heretic Arius, for example. He read the Bible and was convinced that Jesus was a creature and was not equal with the God the Father. Now, you and I both know that Arius was wrong. Back then, a council was called and his heresy was condemned. That was when the world was overwhelmingly Catholic. If Arius had been born in this century, he would have successfully founded a church and gathered a following. This is what happens when people “become a pope unto themselves” by self-interpreting the Bible.

Finally, if you are going to believe in the Bible alone, which Bible are you going to pick? NIV? KJV? NKJV? NABRE? TNIV? JB? NRS? Each one has a different wording and different phrasing. Besides, there are seven books missing from all Protestant Bibles. So, which version of the Bible has God’s seal of authority and guarantee of no error?

Sola Scruptura has led to nothing, but division and disunity. Pick any verse from the Bible and you will get as many different interpretations as you have people. No wonder there are 40,000+ Protestant denominations.
