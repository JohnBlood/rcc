---
title: "Growing Number of Detroit Pastors Wear Handguns in Pulpit"
date: "2009-10-07"
categories: 
  - "roman-catholic"
tags: 
  - "detroit"
  - "guns"
  - "michigan"
  - "pastors"
  - "priests"
  - "protect"
---

    Yes, you read the headline of this post correctly.  I couldn’t believe it myself when I saw the title.  It turns out that there is an increase in crime has led some pastors to start carrying handguns.  I’m also no surprised that this article centers around Detroit, Michigan.

    Personally, I’m okay with priests carrying weapons to protect themselves, their parishioners, and their parish.  In fact, Fr. Corapi, the great American conservative and out-spoken priest, at one time carried a gun because there were several threats on this life.

    In the Bible, Our Lord said[![clint-eastwood-dirty-harry](images/clinteastwooddirtyharry_thumb1.jpg "clint-eastwood-dirty-harry")](http://realromancatholic.files.wordpress.com/2009/10/clinteastwooddirtyharry1.jpg) “all that take the sword shall perish with the sword.” (Matthew 26:47)  This refers to people who wage for a living (as St. Thomas Aquinas says in his Summa Theologica) or who wantonly kill others.  This does not refer to those who pick it up to defend themselves or others.  Please feel free to comment.

    Below you will find the original article.

The Rev. Lawrence Adams teaches his flock at the Westside Bible Church to turn the other cheek. Just in case, though, the 54-year-old retired police lieutenant also wears a handgun under his robe.

Adams is one of several Detroit clergymen who have taken to packing heat in the pulpit. They have committed their lives to a man who preached nonviolence and told followers to love their enemies. But they also say it's up to them to protect their parishioners in church.

"As a pastor, I'm referred to as a shepherd," Adams said. "Shepherds have the responsibility of watching over their flock. Do I want to hurt somebody? Absolutely not!"

Responding to a break-in at his church Sunday evening, Adams surprised a burglar carrying out a bag of loot and shot the man in the abdomen after the man swung the bag at him.

The burglar survived — for which Adams is grateful — but the reverend said he could have been hurt or killed if he had not been armed.

Detroit had the nation's highest homicide rate last year among cities of at least 500,000 residents. The city has been losing manufacturing jobs for decades, and these days about one in four working-age residents is without a job.

The northwest Detroit neighborhood surrounding Adams' church isn't one of the city's most dangerous. But there have been many recent reports of crimes in the area, including four burglaries, three auto thefts, one armed robbery and four assaults, including one with intent to murder.

"It's getting worse because of the economy," Adams said. "People are out of work and feel they have to provide for their families."

Prior to 2000, anyone who wanted to carry a concealed weapon in Michigan had to show a need to do so. Now, gun owners simply have to pass a stringent background check and complete eight hours of handgun training.

"I get people from all walks of life, including pastors," said Rick Ector, owner of Rick's Firearm Academy in Detroit. "But it's not anything specific to pastors. Detroit is not a very safe place."

Michigan allows pastors to decide if someone registered to carry a handgun can do so for protection inside churches.

The clergy in Detroit who arm themselves say they do so because of the high overall crime rate. But churchgoers elsewhere have been the target of violent attacks several times in recent years:

Last year in a New Jersey church, a man fatally shot his estranged wife and a man who intervened in the attack.

A pastor was found stabbed to death in August in an Oklahoma church.

A Maryville, Ill., preacher was gunned down during his Sunday sermon in March.

In December 2007, a gunman killed two people at a Christian youth mission center near Denver and two others at a megachurch in Colorado Springs.

Near Detroit, a man was shot to death in 2003 while worshipping in a Catholic church. And an attacker fatally shot a woman and wounded a child inside another Detroit church three years ago because of a domestic dispute.

"I don't know what kind of issues people are bringing with them. You could be running from estranged husband, boyfriend," said Bishop Charles Ellis III, pastor of the 6,500-member Greater Grace Temple in Detroit.

Ellis said he sometimes carries a gun, but never in the pulpit. His church has a "ministry of defense" for Sunday services made up of about 18 armed congregants who are off-duty law enforcement officers.

Clergy are adjusting to society, said the Rev. Kenneth J. Flowers, pastor of Greater New Mt. Moriah Baptist Church in Detroit.

"In addition to their faith, they are carrying weapons," said Flowers, who does not carry a gun. "There used to be a time when everybody respected a pastor. Even a drunk would straighten up if a preacher came by."

Many people are uncomfortable with the idea of an armed clergy, because Christ preached against violence and taught people they should love their enemies.

"But the scriptures also are clear that civil authority is part of God's plan," said Claude Wiggins, a former pastor and current assistant at the Detroit Baptist Theological Seminary.

"In our country, it says in due process that you may bear arms to protect yourself. While we should be committed to trusting God, that doesn't prevent us or command us to be totally passive," Wiggins said.

Al Meredith, pastor of the Wedgwood church in Fort Worth, said some off-duty police officers who are deacons at his church carry guns, but he's uncomfortable with the idea of an armed congregation.

"It discourages the crazies from acts of violence if they see uniforms around, but I don't want everybody bringing guns," Meredith said. "My ultimate conviction is what does the word of God say and what would Jesus do? Can you in your wildest imagination ever see Jesus packing a .38? I can't imagine Peter and Paul carrying .45s."

The Rev. William Revely, who sometimes wears his .357-caliber handgun while preaching at the Holy Hope Heritage Church in Detroit, does not worry whether it might be wrong for a man of God to carry a firearm in church.

"I've always felt that the only way to handle a bear in a bear meeting is to have something you can handle a bear with," said the 68-year-old pastor, who practices at a gun range with another pastor. "We have to be realistic. I know too many people who've been shot, carjacked."

Adams said most — if not all — of Westside's 50 members have supported his actions after encountering the burglar.

"People want to look at Christians and the church as believers in God and ask 'Why doesn't God protect you?" Adams said. "The reality is God has given man free will. We have to use our God-given talents and protect ourselves."
