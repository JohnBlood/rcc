---
title: "Ebooks coming"
date: "2015-01-15"
categories: 
  - "announcement"
tags: 
  - "ebook"
---

If you enjoyed my saint biographies, I have good news for you. I'm planning to release a collection of 20 saint biographies in ebook form. I'm going to add to the ones I have written already and write several new ones. I don't have a title. Feel free to suggest one in the comment section. I'm aiming to release it during Lent this year.

I'm also planning to published a second ebook collecting the articles I have written on SSPX. That should be published by the middle of the year.

Stay tuned for more updates, including covers.
