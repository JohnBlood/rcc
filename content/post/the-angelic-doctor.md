---
title: "The Angelic Doctor"
date: "2014-01-29"
categories: 
  - "saints"
tags: 
  - "saint"
  - "st-thomas-aquinas"
---

\[caption id="attachment\_874" align="alignright" width="330"\][![St. Thomas Aquinas before the Crucifix](images/stthomasaquinascross.png)](http://realromancatholic.files.wordpress.com/2014/01/stthomasaquinascross.png) St. Thomas Aquinas before the Crucifix\[/caption\]

Today is the feast day of St. Thomas Aquinas, also known as the Angelic Doctor. St. Thomas was a tremendous theologian whose works influence the Church to this day. In honor of this great saint, I thought I would tell a couple of my favorite stories of his life.

Many heresies have used by the devil in his effort to damage the Church. The Middle Ages where no different. A new form of the Manichaean heresy was shaking the Church. St. Thomas was engaged in fighting this heretical outbreak when he was invited to a banquet at the court of St. Louis IX. As St. Thomas sat deep in thought near the king, ladies, lords and knights talked and feasted around him. Suddenly, the saint slammed a mighty fight onto the table and shouted, "And that will settle the Manichees." Everyone was frozen to the spot in horror at the breach in etiquette. Knights were ready to eject the Italian friar. Instead, St. Louis turned to his servants and told them to bring a pen and paper so that St. Thomas would not forget his argument.

St Thomas' most well know writing is the three-volume Summa Theologica. He spent most of his life working on it and was not able to finish it before he died. At one point, St. Thomas took his manuscript into the chapel and laid it at the foot of the life-sized crucifix. Kneeling in prayer, St. Thomas told the Lord, "Lord, what I have written here is so much straw." One of the hands of the Crucified Christ came off the cross and touched St. Thomas' head. "Thomas, you have written well of me," said Christ from His Cross.

God Bless and pray to St. Thomas.
