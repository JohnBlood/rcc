---
title: "Merry Christmas and New eBook"
date: "2017-12-24"
coverImage: "nativity.jpg"
---

![nativity](images/nativity.jpg)

I would like to wish everyone a very Merry Christmas and a Happy New Year. This year I didn’t have much of an opportunity to write about the current turmoil in the Church and the World. Hopefully, the coming year will give me more time to write.

[![letters-home](images/letters-home.jpg)](https://www.books2read.com/u/4NR509)

As a Christmas present, check out my recently published short story, [Letters Home](https://www.books2read.com/u/4NR509). This short story is set in the midst of World War II and tells the story of a soldier in Europe and his mother at home in the States. It is 100% FREE. Enjoy and please rate.

Again, Merry Christmas.
