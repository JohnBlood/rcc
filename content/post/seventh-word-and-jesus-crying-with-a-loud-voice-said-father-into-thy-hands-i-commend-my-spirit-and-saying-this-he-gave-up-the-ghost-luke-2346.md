---
title: "Seventh Word - And Jesus crying with a loud voice, said: Father, into Thy Hands I commend My Spirit. And saying this, He gave up the ghost. (Luke 23:46)"
date: "2014-04-09"
categories: 
  - "last-seven-words-of-jesus"
  - "lent"
---

As He neared the end of His Passion, suffered for our sake, Jesus delivered His final words declaring that death had not power over Him. The Master and Creator of this world could never be subject to death, for He is the Master of all things. Having already declared His Mission finished, Our Lord now had to return to His Father in Heaven, where He now reigns and rules.

Lord God, help me to never fear death. Help me to live a life in accordance with Your Will, so that Heaven may be my destination. Amen.
