---
title: "The Non-Existent Catholic Voice in America"
date: "2013-06-27"
categories: 
  - "bishops"
  - "discipline"
  - "heresy"
  - "liberalism"
  - "lukewarm"
  - "roman-catholic"
  - "vatican-ii"
tags: 
  - "catholic"
  - "discipline"
  - "pope-leo-xiii"
  - "vatican-ii"
---

Monday night I attended a Fortnight For Freedom talk at a local church. It was given by a local Franciscan nun. Unfortunately, this talk was representative of current condition of the Catholic Church. Not once did the sister mention sin or its consequences. Not once did she say that abortion and contraception are sinful practices and those who take part in them should go to confession or risk their soul. Instead she gave an overview of how the world had changed, but the Church had remained a beacon of light in the world. While this was true, it is no longer.

The Church, whose purpose is to care for souls and help them get to heaven, is no longer the vocal protector of freedom and proclaimer of the Gospel. Instead it has become more concerned with social and material issues and being politically correct.

Before I go any further, a little history lesson is necessary. In October of 1884, Pope Leo XIII was given a Divine warning after offering Mass in his private chapel. As he was preparing to leave the altar, he heard two voices. One was the guttural voice of Satan and the other was the softer voice of Our Lord. Satan told Our Lord that he could destroy His Church. Our Lord told him to do so if he could. Satan replied that he needed more time and more power. When Our Lord asked Satan how much time and how much power he needed, Satan said 75 to 100 years, and a greater power over those who will give themselves over to his service. Our Lord granted him the time and the power. Pope Leo immediately went to his office and composed the prayer to St. Michael and ordered that it be said after all Low Masses.

Now, you probably have two questions. First, Why would Our Lord allow this? Like Job in the bible, this is a test for all mankind. But it is also a testament of God's power and his promise because in the end He alone will triumph. Second, you'll ask, how come nothing happened in those 75 to 100 years? After all, the Church still exists. Well, something did happen. On January 25th, 1959, Pope John XXIII announced his intention to call the Second Vatican Council. The year 1959 is 75 years after Pope Leo's vision.

The Second Vatican Council marked a turning point in the Church. It is the dividing line in the modern history of the Church. It was begun by John XXIII with good intension, but it resulted in vague documents, which in turn led to loss of faith and loss of discipline around the world. In short, it was a rupture unlike any previously seen in the Church.

I would like to state before I go any further that I am a Roman Catholic dedicated the Roman Catholic Church and to her head Pope Francis. I am not a supporter of SSPX, CMRI or any of those other renegade "independent catholic" groups. If you thought I was, you must be new to this blog. I am, however, a lover of the Roman Catholic Faith who is greatly saddened and hurt by her current, sorry state.

To continue. While John XXIII had good intentions when he launched Vatican II, good intentions don't make a whole lot of difference in the grand scheme of things. In 1972, Pope Paul VI told the world that "through some crack the smoke of satan has entered into the temple of God". If the Supreme Pontiff said that back in the 70s, just think how bad the condition of the Church is now.

The major problem that resulted from Vatican II was a wussification of the Church. Now the Church is more interested in getting along with the world instead of converting it. The focus is on "openness" to other "faith communities" (and worse, non-faith communities). Bishops want to "get along" and not rock the boat. They refuse to fight heresy and in some cases support it. This would never have happened before Vatican II.

I spoke of a loss of discipline. In particular, I am speaking of the shortening of the fast before Mass, the utter lack of confession before Mass and the end of the Friday abstinence from meat. The first and the last were both sacrifices to curb the body and strengthen the soul, but today hardly anyone abstains from meat on Fridays because they think it was abolished by Vatican II.

\[youtube=http://youtu.be/vT9oYqJV2wA\]

Sin has become such a way of life, that many in the Church fear that if they call a sin a sin they will lose popularity. They forget that life is not a popularity contest, but a journey to either heaven or hell. It has gotten so bad that bishops and cardinals are openly laughing and joking with public sinners, Catholics who openly oppose the Church's teaching and still claim to be Catholic. Princes of heaven are now trying to gain favor with the princes of earth. Before Vatican II, when bishops were not afraid to use their power, they brought kings and princes to their knees. Henry IX, the Holy Roman Emperor, was excommunicated by Pope Gregory VII. Henry journeyed to where the Pope was staying and knelt in the snow for three days as a sign of penance. Popes in the past have regularly excommunicated monarchs and absolved their citizens from obedience to those monarchs.

As I said before, many leaders in the Church are worried about the material and not the spiritual, They fight for immigration reform and environmental protection, when they should be fighting abortion, same-sex marriages and working to protect religious liberty. Many pay lip service to fighting abortion or same-sex marriages, but are not strong enough in their efforts. In fact, a pro-life activist once said that if all the bishops in the US got together, they could stop abortion in the US for good.

It is now time to throw off the chains of political correctness and the bonds of popularity. It is time to preach the plain Gospel. It is time to call sin sin, to warn people that a life of sin is rewarded with the eternal punishments of hell. We must fight evil with prayer, penance and words. Remember the Rosary is our best weapon in these trying times. Use it and the devil will flee. My God save us and save His Church.
