---
title: "A Call for a Rebirth in Catholic Literature"
date: "2011-09-18"
categories: 
  - "evangelization"
  - "roman-catholic"
  - "tradition"
  - "writers"
tags: 
  - "catholic"
  - "latin-mass"
---

> "...seeing that the chief instrument employed by our enemies is the press, which in great part receives from them its inspiration and support, it is important that Catholics should oppose the evil press by a press that is good, for the defense of truth, out of love for religion, and to uphold the rights of the Church. While the Catholic press is occupied in laying bare the perfidious designs of the sects, in helping and seconding the action of the sacred Pastors, and in defending and promoting Catholic works, it is the duty of the faithful efficaciously to support this press, both by refusing or ceasing to favor in any way the evil press; and also directly, by concurring, as far as each one can, in helping it to live and thrive..." - Pope Leo XIII's encyclical Dall'alto Dell'apostolico Seggio

Today's bookstore and library shelves are full of books, both fiction and non-fiction, which are supposedly Catholic. The majority, if not all, of these books are laced with the liberal, left-wing interpretation of Catholicism that became prevalent after Vatican II.

This means that the people who read these books will be exposed to this version of Catholicism and since they don't know any better, they will take it to be true. This is sad because there was a beauty, a majesty and a simplicity that most people who have never experienced it will miss out. Back then, a spade was called a spade and a sin a sin.

On the other hand, these books can also lead to misunderstands among the Protestant community. How many conservative Protestants have gotten an erroneous view of Catholicism because they read a book by a New Age "catholic"? If these people are exposed to the Truth, they will recognize it and be drawn to it.

Overall, today's literature is devoid of any religion, replacing it with the "science" of evolution. I learned one thing from my 4 years in a Protestant college, a little word called "worldview". We need to return to a Catholic worldview in literature. As Our Lord said,  we must be in the world, but not of the world. "And be not conformed to this world; but be reformed in the newness of your mind, that you may prove what is the good, and the acceptable, and the perfect will of God" (Romans 12:2, DRB)

As a writer, I hope to bring back what has by in large been lost. My goal is to return religion to literature, Catholic religion in particular. I've already written several mysteries involving a priest named Fr. Benedict Granger, who was a private detective before he took up the cloth. I plan write more and I plan to include references to the Latin Mass and traditional Catholic moral theology.

I'm not saying any of this to pat myself on the back, but to show how Catholics can evangelize and reawaken interest in the Traditions of the Catholic Church through words. This is a call to all Catholic writers to pitch in and take back Only with much prayer and effort will we be able to win the world back for Our Lord.
