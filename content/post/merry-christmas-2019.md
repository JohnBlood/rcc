---
title: "Merry Christmas 2019"
date: "2019-12-25"
categories: 
  - "christmas"
tags: 
  - "2019"
coverImage: "new-years-eve-3802951_640.png"
---

![](images/new-years-eve-3802951_6402024914342.png)

I would like to wish all of my readers a Merry Christmas and a Happy New Year. The last year has been full of lots of bad news for the Church. I pray that the conning year will bring true renewal for the Church and conversion of Church leaders.

I will attempt to return to this blog in the coming months to fight the errors that are now all too common in the Church. Pray that I will be open to the Holy Ghost.

Merry Christmas.
