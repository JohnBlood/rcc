---
title: "Catholic Bishops Miss the Boat"
date: "2010-08-15"
categories: 
  - "abortion"
  - "bishops"
  - "liberalism"
  - "politics"
  - "roman-catholic"
  - "sin-of-omission"
---

Last week, Americans across the country voted in the nation’s primaries.  Sadly, the bishops and priest had largely not advised those entrusted in their care how to vote as Catholics.  There is a big difference between how Catholics are required to vote and how others vote.  Catholics are require to vote pro-life 100% of the time, no matter what the issues are.

Recently, I found a voter guide for Michigan Catholics.  I will include a selection to show how Catholics are given almost no definitive guidance when it comes to voting as a Catholic should.

[![cross_and_flag](images/cross_and_flag_thumb1.jpg "cross_and_flag")](http://realromancatholic.files.wordpress.com/2010/08/cross_and_flag1.jpg)

This document produced by the Michigan Catholic Conference and is called “A Call to Conscience: Faithful Citizenship and the Common Good.”  This two page document was put in church bulletins in the week before the election.  It was signed by Bishop Allen Vigeron of Detroit, Bishop Bernard Hebda of Gaylord, Bishop Earl Boyea of Lansing, Bishop Walter Hurley of Grand Rapids, Bishop Alexander Sample of Marquette, Bishop Paul Bradley of Kalamazoo, and Bishop Joseph Cistone of Saginaw.

Here is a selection from that document:

> “Catholics are called to evaluate all matters, including politics, through the lens of faith, to participate in the public square, to engage the political process, and to allow Gospel values to transform our society into a more just and better world for all.  In other words, **Catholics are called to be “Faithful Citizens.”** \[sic\]…Practicing Faithful Citizenship flow from a **well-formed conscience**…When we act in harmony with our conscience, shunning evil for that which is good, we are bringing the Gospel values to the public square…the teachings of our Lord Jesus Christ, brought to the public square by faith-filled Catholics, help to embolden our communities and to advance the common good…Issues of religious freedom, the right to life, protection of marriage  and the family, the education of children, and how the poor, the vulnerable and immigrants are served confront elected officials…These are the issues that we,too, as Catholics…We recognize that Catholics seek guidance from their church on matter of conscience…As teachers of the faith, we do not endorse candidates or political parties.  However, as bishops of the church, we have a duty to help the Catholic faithful form their consciences based on Gospel values and the teachings of Jesus Christ…Yet not all issues carry equal moral weight…The right to life is a commandment of God, and inherent and a fundamental moral principle.”

The biggest problem that I have with document is that it is too vague.  Yes, it does say that the right to life is the most important issue.  However, it does not say that if a Catholic votes for a pro-abortion he commits a mortal sin and must confess that sin to a priest before he can approach the sacraments.  This is a truth that is never heard the pulpit.

Another big problem that I have with this document is the use of the phrase “Common Good”.  The talk of a “Common Good”, as most people use it, is uncomfortable to me.  What is good for me is not necessarily good for someone else.  The idea of “Common Good” reminds me a bit of Communism, where the leaders decide what is good for the common people.

Finally, this document stresses the use of a “well-formed conscience”.  It almost sounds as if the bishops are relying on peoples’ consciences to do their jobs for them.  You cannot have a well-formed conscience if you do not known the faith and you don’t know the faith unless it is taught to you by your leaders, such as your bishops and priests.

Once again, the bishops kind of, sort of tell their flock how to vote.  They do so ambiguously that the meaning is obscured.  This lack of clarity has led to many Catholics voting against their faith.  The blame falls directly in the laps of those bishops.  I have said this several times and it’s still true.  Until they start speaking definitively on faith and moral and fighting for the true faith, this country will not be blessed and will continue its downward spiral.
