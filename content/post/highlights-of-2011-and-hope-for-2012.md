---
title: "Highlights of 2011 and Hope for 2012"
date: "2012-01-02"
categories: 
  - "anglican"
  - "church-of-england"
  - "latin-mass"
  - "new-year"
  - "pope-benedict-xvi"
  - "roman-catholic"
  - "sommorum-pontificum"
tags: 
  - "catholic"
  - "latin-mass"
  - "latin-tridentine-mass"
  - "pope"
  - "pope-benedict-xvi"
  - "roman-missal"
---

[![](http://realromancatholic.files.wordpress.com/2012/01/2011to2012.png?w=300 "2011to2012")](http://realromancatholic.files.wordpress.com/2012/01/2011to2012.png)2011 was a turbulent year and I can't say that I'm sorry to see it go. Now we're staring at the blank slate that is 2012. Even though much of what happened in 2011 was bad (Muslim Brotherhood taking over the Middle East; US economy falls apart; Occupy Movement spreads discontent and disorder around the world), there were a few things that were positive, especially in the Church. Here are a few:

1. Pope John Paul II is beatified. A great day for the Church.
2. Catholic Church purchased Crystal Cathedral. One priest commented on this event by saying, "We'll buy those Protestant landmarks one at a time. It'll take time and money, but we'll do it."
3. New translation of the Roman Missal was released for the English speaking countries. This new translation is a closer, more exact translation from the Latin Novous Ordo and leads to a better understanding of what we celebrate.
4. Irish priests stand strong in the face of government demands that they break the seal of confession if someone child abuse.
5. Pope Benedict XVI launched the new Vatican news site from and iPad. He also send the first Papal tweet.
6. The Vatican releases _Universae Ecclesiae_, which expands on _Summorum Pontificum_ and reiterates the fact that priests around the world are free to celebrate the Latin Tridentine Mass.

On the flip side, there are also a several indicators that give me some hope for 2012. Again, here are a few.

1. The Ordinariate for Anglicans in America (Personal Ordinariate of the Chair of Saint Peter) begins January 1, 2012. The Pope has already picked a leader, Father Jeffrey N. Steenson.
2. There are many signs that the Ordinariate in the UK will grow. Hundreds are asking to join.
3. Pope Benedict plans to visit the communist country of Cuba.

During New Year's Eve vespers, Pope Benedict said, "Another year approaches its end, while we await a new one, with the trepidation, desires and expectations of always...With the spirit filled with gratitude, we prepare to cross the threshold of 2012, remembering that the Lord watches over us and takes care of us...In Him this evening we want to entrust the entire world. We put into His hands the tragedies of this world of ours and we also offer Him the hopes for a better future."

With that I would like to wish you a Happy, Holy and Healthy New Year.
