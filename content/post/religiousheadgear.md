---
title: "Religious Headgear Makes a Come Back"
date: "2012-01-06"
categories: 
  - "biretta"
  - "bishops"
  - "head-gear"
  - "mitre"
  - "priest"
  - "priest"
  - "protestant"
  - "return-to-tradition"
  - "roman-catholic"
  - "tradition"
tags: 
  - "biretta"
  - "catholic"
  - "priest"
  - "religious-head-coverings"
---

[![](http://realromancatholic.files.wordpress.com/2012/01/msgrhankiewiczchristtheking.png?w=159 "Msgr Edward Hankiewicz on the Feast of Christ the King 2011")](http://realromancatholic.files.wordpress.com/2012/01/msgrhankiewiczchristtheking.png)One of the biggest things that's missing today is religious headgear. If you look at pictures of clergy in the past, you will notice priests wearing all kinds of distinctive head coverings. These head coverings set them apart and made it obvious that they are someone special. The most recognizable piece of clerical headgear is the biretta, like the one in the picture to the left.

I had only rarely seen a biretta until Pope Benedict XVI issued _Summorum Pontificum_ and allow a freer and wider use of the Latin Mass. After that, I saw it whenever I served the Latin Mass. Several months ago, my parish received a new pastor (also left), who brought his biretta with him. He takes it out for important feasts of the Church. He calls it his liturgical party hat. A little boy called it Monsignor's helmet. They both work for me.

I hope more priests either dig out their birettas or at least rediscover them. They are an important sign of the priest's position and importance.

What's amazing is that there are many different kinds of birettas and other clerical head-gear. The color and shape can change between different orders and clerical status.

Here are a couple of site to check out to see the different forms of clerical head-gear. The first is [Domus Birettarum](http://domusbirettarum.blogspot.com/). This site focuses mainly on birettas, even a few non-Catholic ones.  The other is [The Philippi Collection](http://philippi-collection.blogspot.com). Their site description reads as follows: "The Philippi headwear collection is currently the world’s largest collection of clerical, ecclesiastical and religious head coverings and is unique in both its scope and size. Whether they are ceremonial or worn as a part of everyday life, you’ll find examples of headwear from every religious persuasion around the globe. In addition, the collection includes 116 religious objects." This collection is quite amazing. They have religious head coverings from many different churches, but most are Catholic. (Go figure.)

Check these sites out and pass them onto your pastors. Try to give them the hint to bring back religious head gear.
