---
title: "All Altar Boys Should Know How to Use a Thurible"
date: "2013-04-21"
categories: 
  - "altar-boy"
  - "humor"
  - "video"
tags: 
  - "altar-boy"
  - "humor"
---

The thurible is an important piece of liturgical equipment that all altar boys should know how to use. Unfortunately, just because they should know how to use it does not mean that most do know. Here is a video that demonstrates the best way to use a thurible.

\[youtube=http://www.youtube.com/watch?v=nKpAMvqt7Cw\]

This clip is from the TV show Human Target, about a man who goes undercover to protect people.
