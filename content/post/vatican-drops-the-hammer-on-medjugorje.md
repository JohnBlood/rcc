---
title: "Vatican Drops the Hammer on Medjugorje"
date: "2013-11-10"
categories: 
  - "medjugorje"
  - "quck-update"
---

Most people should aware of this story, but the Vatican has finally dropped the hammer on "apparitions" at Medjugorje. The Papal Nuncio to the US issued a letter at the request of the head of the Congregation fo the Doctrine of the Faith prohibiting both clergy and laity for taking part in events which accept the authenticity of Medjugorje. This is a major step that has been needed for many years. I have major problems with Medjugorje, which I will cover in a future post. For now, I'd like to say, I'm very happy the Vatican has finally made a move.

Here is the letter:

[![MedjugorjeLetter](images/medjugorjeletter.png)](http://realromancatholic.files.wordpress.com/2013/11/medjugorjeletter.png)
