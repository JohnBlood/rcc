---
title: "St. Teresa Benedicta of the Cross (Edith Stein)"
date: "2012-08-09"
categories: 
  - "roman-catholic"
  - "saints"
tags: 
  - "catholic"
  - "saint"
---

I originally wrote the following for my parish's bulletin. Since today is St. Edith Stein's feast day, I though I would share it with you.

[![](http://realromancatholic.files.wordpress.com/2012/08/st-edithstein.png?w=243 "St. Edith Stein")](http://realromancatholic.files.wordpress.com/2012/08/st-edithstein.png)Edith Stein was born on October 1891 in Breslau, Germany to Jewish parents. Her father, who ran a timber business, died when she was very young. Edith’s mother was left alone to care for the family. Despite her mother’s strong faith, Edith lost her own faith in her teens.

Naturally brilliant, Stein excelled in her studies in high school. As she entered university, her interests turned to philosophy. In 1917, Stein received a doctorate in philosophy.  While on holiday in 1921, Stein read the autobiography of St. Teresa of Avila. The story of this saint’s life touched her so much that she was baptized in the Catholic Church on January 1, 1922. She continued her work in philosophy until 1933 when anti-semitic laws passed by the Nazis forced her to give up her post.

During the same year, Stein entered the Carmelite Convent of Cologne. While in the convent, she came to believe that it was her duty to intercede in prayer for those suffering, especially the Jews, like a modern Queen Esther. As the Nazi threat grew, Stein was transferred to a convent in Echt in the Netherlands.  On June 6, 1939 she wrote, "I beg the Lord to take my life and my death … for all concerns of the sacred hearts of Jesus and Mary and the holy church, especially for the preservation of our holy order, in particular the Carmelite monasteries of Cologne and Echt, as atonement for the unbelief of the Jewish People and that the Lord will be received by His own people and His kingdom shall come in glory, for the salvation of Germany and the peace of the world, at last for my loved ones, living or dead, and for all God gave to me: that none of them shall go astray."

Even in the Netherlands she was not safe. On August 2, 1942, Stein was arrested by the Gestapo, along with her sister Rosa, who had also become Catholic. On August 7, the Stein sisters and 987 Jews were deported to Auschwitz. They were probably gassed to death two days later. She was beatified by Pope John Paul II on May 1, 1987. Here, the Pope honored her as “a daughter of Israel, who, as a Catholic during Nazi persecution, remained faithful to the crucified Lord Jesus Christ and, as a Jew, to her people in loving faithfulness.” She was canonized by Pope John Paul II on October 11,  1998.
