---
title: "Announcing Release Date of Church Triumphant Ebook"
date: "2015-03-05"
categories: 
  - "announcement"
tags: 
  - "ebook"
---

\[caption id="attachment\_995" align="alignleft" width="188"\][![Church Triumphant ebook](https://realromancatholic.files.wordpress.com/2015/03/churchtriumphantcover.jpg?w=188)](https://realromancatholic.files.wordpress.com/2015/03/churchtriumphantcover.jpg) Church Triumphant ebook\[/caption\]

I announced in January that I would release a book about the saints during Lent. I am now happy to announce that _Church Triumphant: 25 Men and Women who Gave Their Lives to Christ_ will be released on Easter Sunday. Starting next week, it will be available for preorder on Amazon, Smashwords, iTunes, and other ebook stores. Stay tuned for links when I upload it.

Also, take a moment to [sign up for my new e-newsletter](http://eepurl.com/bf8teP). Every time a new article is posted, you will get an email. Email subscribers will also get special offers on this and all upcoming ebooks.
