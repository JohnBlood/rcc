---
title: "Quick Update: Williamson has been kicked out of the SSPX"
date: "2012-10-26"
categories: 
  - "quck-update"
  - "sspx"
---

This is a quick follow up to my previous post.

\[caption id="attachment\_510" align="alignright" width="300"\][![](http://realromancatholic.files.wordpress.com/2012/10/bishopwilliamsonpolice.png?w=300 "BishopWilliamsonPolice")](http://realromancatholic.files.wordpress.com/2012/10/bishopwilliamsonpolice.png) Bishop Williamson escorted out of Heathrow Airport following his flight from Argentina in February, 2009\[/caption\]

It is now official, Bishop Richard Williamson has been booted out of the SSPX. I doubt he will be going away anytime soon. In fact, I expect him to create his own competing organization within a month or two.

Why was Williamson kicked out? According to a source I read, Bishop Fellay gave Williamson one week to close his site and email newsletter and publicly apologize for the damage he has done to the Church and the SSPX. The deadline ended yesterday with no action on Williamson's part.

Here is the official announcement from the SSPX site:

> “Bishop Richard Williamson, having distanced himself from the management and the government of the SSPX for several years, and refusing to show due respect and obedience to his lawful superiors, was declared excluded from the SSPX by decision of the superior general and its council on October 4 2012. A final deadline had been granted to him to declare his submission, after which he announced the publication of an ‘open letter’ asking the superior general to resign.
> 
> “This painful decision has become necessary by concern for the common good of the Society of Saint Pius X and its good government, according to what Archbishop Lefebvre denounced: ‘This is the destruction of authority. How authority can be exercised if it needs to ask all members to participate in the exercise of authority?’”

We must keep Bishop Williamson, the SSPX and all those who have been separated from the Church in our prayers. Hopefully, one day they will be reunited with Rome.
