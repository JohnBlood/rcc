---
title: "Altar Serving Book – Dress Code"
date: "2013-09-30"
categories: 
  - "altar-boy"
  - "roman-catholic"
  - "serving-book"
tags: 
  - "altar-boy"
  - "catholic"
  - "mass"
---

[![LatinMass](images/latinmass.png)](http://realromancatholic.files.wordpress.com/2013/09/latinmass.png)Sadly, today most churches do not have a dress code for their altar boys. While it might not seem significant, a dress code is important because it is meant to help the server understand the importance of what he is doing. Too often, I have seen servers show up for Mass wearing jeans and sneakers or sandals. This is not how an altar boy should dress. And here is why.

First of all, at Mass God the Son comes down from Heaven to be present on the altar under the appearances of bread and wine. If God is going to be in your midst, wouldn't you want to be dressed up? I certainly would.

Second, most Catholics today have lost this sense of the importance of dressing properly for Mass. By dressing up for Mass, even though your clothes are mostly covered by your cassock and surplice, you are setting an example to the congregation on how they should dress for Mass. They will see how you are dressed when you enter the sacristy and when you leave after Mass.

The typical dress for altar boys should be:

- Dark pants (preferably black)
- Dark shoes (preferably black dress shoes)
- Polo shirt or long-sleeved shirt

There should be **no**:

- Sandals
- Sneakers
- Shorts
- Jeans
