---
title: "The Anglican Church Continues Its Steady Journey Towards Oblivion"
date: "2012-11-22"
categories: 
  - "anglican"
  - "church-of-england"
  - "liberalism"
  - "modernism"
  - "protestants"
  - "women-clergy"
tags: 
  - "protestant"
  - "women-clergy"
  - "women-priest"
---

\[caption id="attachment\_536" align="alignright" width="400"\][![](images/archbishopsofyorkandcanterbury.png "The Archbishops of York and Canterbury")](http://realromancatholic.files.wordpress.com/2012/11/archbishopsofyorkandcanterbury.png) The Archbishops of York and Canterbury\[/caption\]

Today, during the course of its annual general synod, the Anglican Church voted on the subject of women bishops. Contrary to the expectations of everyone (including this writer), this legislation was not passed. Don't start rejoicing and proclaiming that there is hope for the Anglican Church just yet because the vote failed by a whole four votes in the House of the Laity. The interesting part of this story is that all of the leading Anglican clergy have been campaigning hard for women bishops. One of those leading the charge is the exiting Archbishop of Canterbury, Rowan Williams (who loses credibility every time he smiles because he looks like an aging Grover from the Sesame Street). Williams wanted to get women bishops approved before his tenure ended this year. It might have looked like he failed, but with the results that we saw today it will not be long before he gets his wish.

\[caption id="attachment\_545" align="alignright" width="288"\][![](images/anglicanwomenpriests.png "AnglicanWomenPriests")](http://realromancatholic.files.wordpress.com/2012/11/anglicanwomenpriests.png) Who thought this was a good idea?\[/caption\]

This vote show a difference of opinion between the clergy of the Anglican Church and the laity. It is just another sign of the fractures within the Church of England (C of E) and a sign of the coming civil war within the C of E (Damian Thompson said it first). Women clergy is one of the foremost issues that has caused Anglican groups to break away from the main body and often return to Rome, but it is not the only one. There is also a strong division between low church Anglicans (more mainline Protestants) and high church Anglicans (Catholic lite).

One top of that there has been a shift away from England as the center of power and authority and more towards Africa. (The second highest Anglican churchman, the Archbishop of York, was born in Uganda.) Originally, the C of E was used by the Imperial English government to strengthen and unite the empire. But as that empire disappeared and the member nations became independent, so did the branches of the C of E within those nations. (That is why the majority of Anglicanism is different in different parts of the world, both in doctrine and ceremony.) The African branches have tended to be more conservative and as such have sought to distance themselves for their progressive mother church in England. In fact, back in 2008, a number of conservative Anglicans did not attend the Lambeth Conference that takes place every ten years. (This meeting is used by Anglicans bishops around the world to set policy.) Instead, these bishops met in Jerusalem a week before Lambeth at the Global Anglican Future Conference.

\[caption id="attachment\_541" align="alignright" width="300"\][![](http://realromancatholic.files.wordpress.com/2012/11/globalanglicanfutureconference.png?w=300 "GlobalAnglicanFutureConference")](http://realromancatholic.files.wordpress.com/2012/11/globalanglicanfutureconference.png) Global Anglican Future Conference\[/caption\]

The bottom line is that the Anglican Church around the world is in crisis. It is falling apart because of the constant march of modernism and progressivism is undermining its foundation, which was already weak from its break with Rome. A former Anglican priest said that in 50 years there would be no Anglican Church, but I kind of doubt that for one reason: the British Monarchy. The Anglican Church  and the British Monarchy are very intertwined. If the Anglican Church were to go away, it would greatly weaken the monarchy. That being said, the Anglican Church as we know it today will disappear (especially the doctrine). All that will be left will be the outward appearance of a church, all fluff and bright colors, but no substance.

As they go through a very difficult period over the next few years, let's keep our Anglican brothers and sisters in our prayers. Let's pray that they will find their way home to Rome.
