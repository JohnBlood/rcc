---
title: "Archbishop Fulton Sheen Sets the Record Straight on Contraception"
date: "2012-10-19"
categories: 
  - "abortion"
  - "archbishop-fulton-j-sheen"
  - "contraception"
  - "roman-catholic"
  - "saints"
tags: 
  - "archbishop-sheen"
  - "catholic"
  - "contraception"
  - "video"
---

As we near the national election, all Catholics (and all Americans) should listen to this lecture on the evils of contraception.

\[youtube=http://www.youtube.com/watch?v=xgneJNCuRFY\]
