---
title: "Altar Serving Book - Introduction and Purpose of Altar Boys"
date: "2013-04-29"
categories: 
  - "altar-boy"
  - "latin-mass"
  - "roman-catholic"
  - "serving-book"
tags: 
  - "altar-boy"
  - "catholic"
  - "mass"
---

_This post will be the first of several posts that I will write on altar serving. I'm writing this for two reasons. First, I want to inspire those who are currently altar boys to become better. Second, I want to inspire boys who are not servers to consider taking up this great service and duty. I hope to eventually release a full ebook for free, once I think I have covered all of the topics relating to being an altar boy. I will keep you up to date in the project._

**Introduction**

To start off, I would like to get my credentials out of the way or, to put it another way, what qualifies me to talk about altar serving. Well, I've been an altar boy for over 15 years and for about 14 of those years I have served with my younger brother. We have served mostly English Novus Ordo Masses, but we've also served a few Polish Novus Ordo Masses. About three years ago, we learned how to serve the Latin Tridentine Mass. For going on two years we have been serving Mass 6 days a week for our pastor. We have been serving for a while and have the scars to prove it.

You will probably notice that during the course of this and the other serving posts, I will predominantly use the phrase "altar boys" instead of "altar servers". This is because I believe that serving is an office and a job that should be for boys only. Serving has led many boys to consider entering the priesthood. This makes sense for boys, but what about girls? First, get the people used to girls in the sanctuary wearing liturgical clothing and, then, they won't notice it when you start pushing women priests.

**[![AltarBoyPainting](images/altarboypainting.png)](http://realromancatholic.files.wordpress.com/2013/04/altarboypainting.png)Purpose of Altar Boys**

At the altar, an altar boy serves two functions that lead to one result. The first function is to help the priest. This might be obvious or common sense to many, but that does not mean everyone gets it. A well trained sever will be able anticipate what the priest will need and when. This is crucial because if the priest can be sure that, for example, the Missal is at the correct place on the altar when he needs it, then he can relax and focus on more important parts of the Mass. I have seen many times when a priest has had to prompt altar servers to bring him something. This makes for an awkward pause while the priest waits to receive what he asked for. The Mass flows much better when there are few to none of these awkward pauses. It takes time for the altar boy to get to know what the priest wants, but when he does everything flows better.

The second function of an altar boy is to help lead the people. This is more the case in the Latin Tridentine Mass, but it is also true in the English Mass. In the Latin Mass, the people often could not hear the priest, especially in the days before microphones. They could tell where the priest was in the Mass by what he did and what he occasionally said out loud. They also took their cues from the altar boys. For example, when altar boys knelt after the Gospel (or the homily, if there was one), then the people sat. The same is true for the English Mass. The altar boy act as a guide to show the people what they should be doing at certain parts of the Mass, whether it is kneeling or sitting or standing.

The result that should occur, if the two functions are performed correctly, is an increase in reverence in the parish community. While it may not be obvious to many, how altar boys act while they are around the altar can affect how the people in the pew pray and worship God. A server, who does his job well, will uplift those who watch him. They will come to have a greater appreciation for what takes place on the altar and also a greater reverence. If the congregation sees reverence around the altar, they will be inspired to reflect that reverence in their worship. Make no mistake, serving is a very important job that should be taken seriously because it can have a great affect on how people pray.

I hope you found this post interesting and enlightening. Please give me some feedback in the comments section below.
