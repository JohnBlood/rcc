---
title: "Sixth Word -  It is consummated. (John 19:30)"
date: "2014-04-09"
categories: 
  - "last-seven-words-of-jesus"
  - "lent"
---

\[caption id="attachment\_943" align="alignright" width="415"\][![It-Is-Finished](images/it-is-finished.png)](http://realromancatholic.files.wordpress.com/2014/04/it-is-finished.png) It is consummated. (John 19:30)\[/caption\]

With these three words, the Savior of this world declared that His Mission to Redeem mankind had been completed. This mission of redemption had begun in a cave in Bethlehem and now it was completed as the King of this world and the next hung on a cross to free us from our sins. God used this words before when He first created the world in the book of Genesis. Now, He recreated it by washing away the stain of man's sin with His all purifying Precious Blood. Let us now follow His example as best we can be turning away from sin. Let us have nothing more to do with sin. so that we may be worthy of eternal glory with Our Lord and His Mother in Heaven.

Lord Jesus Christ, give me the grace to declare my former, sinful ways finished. Help me to turn my back on sin and repent, so that I may never more hurt Your most Precious Heart. Strengthen my resolve to put aside sin and reach instead for Heaven, my true home, so that I may spent eternity praising Thee forever. Amen.
