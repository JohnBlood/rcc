---
title: "Chasing Liberty: a Review - Part 1"
date: "2015-01-14"
categories: 
  - "book"
  - "review"
tags: 
  - "book-review"
---

\[caption id="attachment\_987" align="alignleft" width="418"\][![Chasing Liberty by Theresa Linden](images/chasing-liberty.png)](https://realromancatholic.files.wordpress.com/2015/01/chasing-liberty.png) Chasing Liberty by Theresa Linden\[/caption\]

_Note: I received an advanced reader's copy of this book from the author for this review._

This is part one of a two-part review of [Theresa Linden](http://theresalinden.webs.com/)'s new book [_Chasing Liberty_](http://www.amazon.com/Chasing-Liberty-Theresa-Linden/dp/1629891665/ref=sr_1_1?s=books&ie=UTF8&qid=1421118050&sr=1-1&keywords=chasing+liberty). Part 1 will take a look at the religious aspects of _Chasing Liberty_. Part 2 will examine the overall writing and will be published on [my writing blog](http://writerssoapbox.wordpress.com/).

**Book Description**

Liberty 554-062466-84 of Aldonia lives in a responsible society that cares for the earth and everyone on it. They have learned to balance resource consumption with replacement initiatives, unavoidable pollution with clean-environment efforts. Science ensures that every baby born is healthy. The government ensures that every baby born is needed. All are cared for, taught, and given a specific duty to perform, their unique contribution to society. Why is Liberty so unsatisfied?

In less than two weeks, Liberty must begin her vocation. Every girl in Aldonia wishes she had Liberty's vocation. Liberty would rather flee from Aldonia and live on her own, independent of the all-controlling government, the Regimen Custodia Terra. The high electrical Boundary Fence crushes any thought of escape. The ID implant imbedded in her hand makes it impossible to hide. She has no choice but to submit. Liberty is slated to be a Breeder.

As vocation day draws near, a man with an obsession for Liberty attacks her and injects her with a drug. She’s about to lose consciousness when someone comes to her rescue, a man in a mottled cape and dark glasses. She wakes in an underground facility where people watch over Aldonia with an array of monitors and surveillance equipment. These people are full of secrets, but she discovers one thing: they rescue a man scheduled for re-education. They rescued him. They can rescue her.

**Now for the Review**

Theresa has skillfully constructed a world that is a combination of George Orwell's _1984_ and Aldous Huxley's _Brave New World_. In this future possible, the building blocks of society have been purged from common knowledge. A one world government teaches the people that such ancient ideas as "family", "God", and "free-will" are evil and lead to disaster and ruin. Theresa said that she based her novel on what she read in the news headlines and I'm afraid she's right. We live in a society that is trying to undermine the traditional family structure and trivialize faith and religion. In the world of the Regimen, science has replaced religion.

The main character of the story, Liberty 554-062466-84, has been raised in a Godless society, but still feels a strange stirring in her soul. When she's troubled or worried, she often feels the comforting presence of her "Friend". It seems that at least one or two other characters share her yearning for something more. That leads me to something I just read a couple of days ago.

In 1969, Fr. Joseph Ratzinger (the future Pope Benedict XVI) gave a series of talks in the future of Catholicism that were later compiled and published in a book entitled _Faith and the Future_. I came upon this quote after finishing _Chasing Liberty_:

> Men in a totally planned world will find themselves unspeakably lonely. If they have completely lost sight of God, they will feel the whole horror of their poverty. Then they will discover the little flock of believers as something wholly new. They will discover it as a hope that is meant for them, an answer for which they have always been searching in secret.

In the world of _Chasing Liberty_, the citizens of the Regimen fill their lives with entertainment, so they can no longer hear God calling to them. It's sad to say, but that is true of today's culture and it will only get worse. Pope Benedict's words are coming true in front of our eyes.

**Conclusions of Part 1**

Overall, _Chasing Liberty_ stands as a warning of what will surely come to pass if we do not change and pray. The family will be destroyed and children will eventually become a product of the State. Religion will be replaced with science and the worship of nature. Free-will will be replaced by State designated vocations.

I will give my final recommendation and rating in part 2.

**Stayed tuned for part 2.**
