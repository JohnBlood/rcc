---
title: "Archbishop Fulton Sheen on What's My Line"
date: "2012-07-16"
categories: 
  - "archbishop-fulton-j-sheen"
  - "humor"
  - "tv"
  - "video"
tags: 
  - "archbishop-sheen"
  - "humor"
  - "tv"
---

Continuing the video series of Archbishop Fulton Sheen, here is a fun video of when the venerable Archbishop appeared on the game show _What's My Line_. I'll get back to serious topics tomorrow, but for now enjoy.

\[youtube=http://www.youtube.com/watch?v=prgvEA2D4sw\]
