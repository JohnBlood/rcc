---
title: "A Rebuke of the Bishops and a Call to the Laity"
date: "2010-04-03"
categories: 
  - "abortion"
  - "apostles"
  - "bishops"
  - "healthcare"
  - "judas"
  - "politics"
  - "roman-catholic"
  - "saints"
---

[![judas-iscariot_wa](images/judasiscariot_wa_thumb1.jpg "judas-iscariot_wa")](http://realromancatholic.files.wordpress.com/2010/04/judasiscariot_wa1.jpg)     This Holy Week we spent some time reflecting on Judas, who handed Jesus over to His enemies.  Since he was a member of the twelve Apostles, Judas was a bishop of the early Church.  As we look upon the Church today, especially the Church in America, there is no shortage of Judases among the bishops.

Take politics for example.  The bishops around the country have failed to correct or excommunicate the many apostate “Catholic” politicians.  They have remained largely silent.  They are failing in their duties.

“That’s an uncharitable thing to say,” most will protest.  (That’s the usual response what a conservative or traditional Catholic tries to correct a liberal.  Besides I’m performing a spiritual work of mercy by instructing the ignorant.)  What about the bishops themselves?  They have failed to show charity to all the aborted babies who have died through their lack of action.  They have failed to show charity to Our Lord by failing Him.

[![Bishops](images/bishops_thumb1.png "Bishops")](http://realromancatholic.files.wordpress.com/2010/04/bishops1.png)     I can count on one hand the traditional or conservative bishops in the US.  I don’t have enough fingers or toes to count all of the liberal bishops.  The majority of bishops have been lukewarm at best in their defense of life, especially when it comes to dealing with politicians.  The road to Hell is paved with good intentions.

Let’s look at some fact of the faith that most Catholics no longer know.  There are nine ways in which a person can be an accessory to another’s sins.

1. By counsel
2. By command
3. By consent
4. By provocation
5. By praise or flattery
6. By concealment
7. By partaking
8. By silence
9. By defense of the ill done

There are four sins that call to Heaven for vengeance.

1. Willful murder
2. The sin of Sodom
3. Oppression of the poor
4. Defrauding the laborer of his wages

(You can find both of these lists in _The Young Man’s Guide_ by Fr. F. X. Lasance _circa_ 1952)

As seen by these lists, silent bishops are accessories to the murders of millions of children.

Each of these bishops were called to be “alter Christi”[![crucifixion](images/crucifixion_thumb1.jpg "crucifixion")](http://realromancatholic.files.wordpress.com/2010/04/crucifixion1.jpg), another Christ.  As my Dad always says, “Christ preached in such away that the religious leaders of His day put Him to death.”  That is the while meaning of being Catholic, especially Roman Catholic.  If you’re not willing to stand up for your faith, you might as well go and hide.

But some people worry that if the bishops speak out, they may lose their tax-exempt status.  **So what?**  Where in the Bible does it say that the Church must be tax-exempt to preach the Gospel?  This is a weak, weak excuse for the bishop to use.  There have been many non-Catholic preachers who speak out on these subjects and some have lost their tax-exempt status, but they still continue to preach the truth.  The bishops could learn a thing or two from our non-Catholic brethren.

Because the majority of the clergy has fallen down on their job to faithfully and forcefully preach the Real Roman Catholic faith, it has fallen to the laity to take up the slack.  We must band together to learn and spread the faith.

Notice the name of this blog: Real Roman Catholic.  That’s [![PopeBenedictWelcome](images/popebenedictwelcome_thumb1.jpg "PopeBenedictWelcome")](http://realromancatholic.files.wordpress.com/2010/04/popebenedictwelcome1.jpg) to emphasize that I follow and believe in the true, authentic faith that comes from Rome, not the pick-and-choose, cafeteria Catholicism prevalent in the USA.  It is time for us to take up the banner and follow the Pope before it is too late.
