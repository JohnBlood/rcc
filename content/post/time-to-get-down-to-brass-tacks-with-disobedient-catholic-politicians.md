---
title: "Time to Get Down to Brass Tacks with Disobedient Catholic Politicians"
date: "2011-07-13"
categories: 
  - "cardinal-burke"
  - "discipline"
  - "heresy"
  - "holy-communion"
  - "liberalism"
  - "politics"
  - "roman-catholic"
  - "sin-of-omission"
  - "video"
---

\[youtube=http://www.youtube.com/watch?v=QMBZDwf9dok\]

After what's been going on in New York, I would like to officially say that I'm a mad as hell. I'm mad because nothing is being done. For those of you who have been living in a hole in the ground for the last couple of months let me bring you up to date.

At the end of June 2011, the "Catholic" governor of New York signed same-sex marriage into law. This action by a "Catholic" politicians is at odds with the teachings of the Roman Catholic Church, who says and upholds that marriage is the union of one man and one woman. What is worse is the response of the bishops in New York to Governor Cuomo. This group of mislead clerics made little or no response. Only the comments of two bishops have been widely published: Archbishop Timothy Dolan of the Archdiocese of New York and Bishop Nicholas DiMarzio of the Diocese of Brooklyn.

Both of these bishops correctly pointed out that this law is an attack on marriage. Bishop DiMarzio even went so far as prohibiting Catholic institutions within his diocese from giving or receiving from honors to those who voted for this law. But that is as far as they will go. In fact, the Brooklyn Diocese stated that it has no plans to refuse to give Holy Communion to Governor Cuomo.

WHAT??? Are you kidding me?

Let's look at the facts. Governor Andrew Cuomo professes to be a Catholic, but promotes ideas that are at odds with the teachings of the Roman Catholic Church. On top of that he is currently separated from his wife and is living with a woman.

This is what the Baltimore Catechism says about Catholics who disobey the Church.

> **169b. How does a baptized person separate himself from full incorporation in the Mystical Body?**
> 
> A baptized person separates himself from full incorporation in the Mystical Body by open and deliberate heresy, apostasy or schism.
> 
> **169c. How does a baptized person separate himself from full incorporation in the Mystical Body by heresy?**
> 
> A baptized person separates himself from full incorporation in the Mystical Body by heresy when he openly rejects or doubts some doctrine proposed by the Catholic Church as a truth of divine-Catholic faith, though still professing himself a Christian.

By rejecting the Church's stance on homosexual marriage, Cuomo has manifested himself to be a heretic and thus separated from the Church.

Canon 915 of the Church's Canon Law states how those who have separated themselves from the Church should be treated when they approach the sacraments.

> Those upon whom the penalty of excommunication or interdict has been imposed or declared, and others who obstinately persist in manifest grave sin, are not to be admitted to Holy Communion.

In a letter written to the US bishops during the 2008, Cardinal Ratzinger stated:

> Regarding the grave sin of abortion or euthanasia, when a person’s formal cooperation becomes manifest (understood, in the case of a Catholic politician, as his consistently campaigning and voting for permissive abortion and euthanasia laws), his Pastor should meet with him, instructing him about the Church’s teaching, informing him that he is not to present himself for Holy Communion until he brings to an end the objective situation of sin, and warning him that he will otherwise be denied the Eucharist.
> 
> 6\. When “these precautionary measures have not had their effect or in which they were not possible,” and the person in question, with obstinate persistence, still presents himself to receive the Holy Eucharist, “the minister of Holy Communion must refuse to distribute it” (cf. Pontifical Council for Legislative Texts Declaration “Holy Communion and Divorced, Civilly Remarried Catholics” \[2002\], nos. 3-4). This decision, properly speaking, is not a sanction or a penalty. Nor is the minister of Holy Communion passing judgement on the person’s subjective guilt, but rather is reacting to the person’s public unworthiness to receive Holy Communion due to an objective situation of sin.

Why are the bishops not doing their job? Why are they afraid to offend? When this letter was originally issued only a couple of US bishops followed it. One of it's most vocal supporters was Bishop (now Cardinal) Raymond Burke of St. Louis.

I came across an articled that basically said that we should not attack Bishop Dolan because he was busy taking care of other things, after all he is head of the US Council of Catholic Bishops. My response is why didn't he make a statement on behalf of all US bishops condemning Catholic politicians who support same-sex marriage and other issues against Church teaching?

There is a little thing called the sin of omission that these bishops are committing by their silence. The Catholic encyclopedia defines sin of omission as:

> the failure to do something one can and ought to do. If this happens advertently and freely a sin is committed.

How will all this end? What will be the outcome of the lack of action of these bishops? The video below tells how it most probably will turn out. We must stand strong in our Faith. We must be ready to fight for it, even to die for it. In the meantime, the bishops need to get down to brass tack and do their jobs. We must pray that God will give them the intestinal fortitude to do what needs to be done to protect the Roman Catholic Church and civilization as we know it.

\[youtube=http://youtu.be/aLC-g9s\_jUg\]
