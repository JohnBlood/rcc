---
title: "Archbishop Fulton Sheen Speaks on the Sacrament of Marriage"
date: "2012-07-30"
categories: 
  - "archbishop-fulton-j-sheen"
  - "roman-catholic"
  - "sacraments"
  - "video"
tags: 
  - "archbishop-sheen"
  - "catholic"
  - "marriage"
  - "sacrament"
---

Venerable Fulton J Sheen is with us one again. This time he is discussing the sacrament of marriage.

\[youtube=http://www.youtube.com/watch?v=PRzGVRI6PYQ\]
