---
title: "Saint eBook Now Available for Pre-Order"
date: "2015-03-28"
categories: 
  - "announcement"
  - "saints"
tags: 
  - "catholic"
  - "ebook"
  - "saint"
---

\[caption id="attachment\_995" align="aligncenter" width="188"\][![Church Triumphant ebook](https://realromancatholic.files.wordpress.com/2015/03/churchtriumphantcover.jpg?w=188)](https://realromancatholic.files.wordpress.com/2015/03/churchtriumphantcover.jpg) Church Triumphant ebook\[/caption\]

I’m happy to announce the release of my new ebook about the saints, _Church Triumphant: 25 Men and Women who Gave Their Lives to Christ_. This new ebook is available for [preorder from Amazon](http://www.amazon.com/Church-Triumphant-Women-Their-Christ-ebook/dp/B00UZDNN0Q/), [Barnes and Noble](http://www.barnesandnoble.com/w/church-triumphant-john-paul-wohlscheid/1121502684?ean=2940046645347), [Kobo](https://store.kobobooks.com/en-US/ebook/church-triumphant-25-men-and-women-who-gave-their-lives-to-christ), and [iTunes](https://itunes.apple.com/us/book/church-triumphant-25-men-women/id978786964?mt=11). _Church Triumphant_ will be officially released on April 5th, Easter Sunday.

This book includes the biographies of the following saints: St. Gonsalo Garcia, St. Andreas Wouters, Blessed Charles I of Austria, Blessed Charles de Foucauld, St. Clement Maria Hofbauer, St. Gabriel of Our Lady of Sorrows, St. Gerard Majella, St. Gianna Molla, Blessed John Henry Newman, St. Maria Goretti, St. Maximilian Kolbe, St. Nicholas, St Peter Julian Eymard, Blessed Pier Giorgio Frassati, St. Padre Pio of Pietrelcina, Pope St. Pius X, St. Teresa Benedicta of the Cross, St. Charbel Makhluf, Venerable Bishop Frederic Baraga, Venerable Fulton J. Sheen, Blessed José Luis Sánchez del Río, St. Kateri Tekakwitha, St. Marianne Cope, Blessed Miguel Pro, Servant of God Father Patrick Peyton.

If you have friends who enjoy reading about the saint, please tell them about _Church Triumphant: 25 Men and Women who Gave Their Lives to Christ_.
