---
title: "The Roles Have Been Reversed"
date: "2012-04-02"
categories: 
  - "cuba"
  - "ireland"
  - "papal-visit"
  - "pope"
  - "pope-benedict-xvi"
  - "roman-catholic"
tags: 
  - "popebenedict"
  - "religion"
---

[![Pope Benedict XVI meets with Fidel Castro](http://realromancatholic.files.wordpress.com/2012/03/popebenedictfidelcastro.png?w=300 "Pope Benedict XVI meets with Fidel Castro")](http://realromancatholic.files.wordpress.com/2012/03/popebenedictfidelcastro.png)Well, Pope Benedict  finished his visit to Cuba last week and is now home. While he was there, the Holy Father had a meeting with Fidel Castro. He also said Mass in Havana's Revolution Square for over 300,000 people.

One thing that I can't help thinking about is the differences between Cuba (a Communist country) and Ireland (a "Catholic" country) in their responses to a Papal visit.

The Communist government of Cuba was obviously glad to have the Pope visit to show how accommodating they can be. I just wonder how long that will last after Pope Benedict has left. On the other hand, the Irish government is more hostile towards the Pope. A member of the Irish government said that they did not see a Papal visit as a possibility anytime soon. On top of this, the Vatican embassy in Ireland was closed and

This is strange considering Ireland is supposedly Catholic and has always had a close relationship with the Vatican. After all, the Irish fought for the freedom to practice the Catholic Faith for several hundred years Also, the Vatican was one of the first governments to recognize the new Irish State in the 30s.

To get an idea how ingrained Catholicism was in the early days of the Irish Republic, take a took at the opening lines of the 1937 Irish Constitution.

> In the Name of the Most Holy Trinity, from Whom is all authority and to Whom, as our final end, all actions both of men and States must be referred,
> 
> We, the people of Éire,
> 
> Humbly acknowledging all our obligations to our Divine Lord, Jesus Christ, Who sustained our fathers through centuries of trial,
> 
> Gratefully remembering their heroic and unremitting struggle to regain the rightful independence of our Nation,
> 
> And seeking to promote the common good, with due observance of Prudence, Justice and Charity, so that the dignity and freedom of the individual may be assured, true social order attained, the unity of our country restored, and concord established with other nations,
> 
> Do hereby adopt, enact, and give to ourselves this Constitution.

I wish that more countries had these kinds of words in their founding documents. But there is a difference between having these words in a constitution and living up to them.
