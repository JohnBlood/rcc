---
title: "Catholics Must Read the Bible"
date: "2010-07-07"
categories: 
  - "bible"
  - "protestants"
  - "roman-catholic"
tags: 
  - "catholic"
  - "christ"
  - "evangelize"
  - "non-catholic"
  - "saint"
---

The statement above may seem like an obvious fact, but sad to say most Catholics do not read their Bibles.  There are a few Catholics who read their Bibles, but the vast majority do not.  Most seem to think that listening to the readings at Mass on Sunday is good enough.  This is sad because it has always been the Catholic Church down through that has protected the Bible.  [![open_bible](images/open_bible_thumb.jpg "open_bible")](http://realromancatholic.files.wordpress.com/2010/07/open_bible.jpg)

Lack of knowledge of the Bible handicaps Catholics when they try to evangelize or just talk with non-Christians about matters of faith.  I spent four years at a non-denominational university and received Bachelor of Science degree in Marketing there.  In those four years, I was amazing at how well those non-Catholics knew the Bible and ashamed at how little I knew.  Their whole moral fabric and understanding of their faith was based on the Bible.  This is where the problem comes in.

Most of the time when talking about faith, the Catholic will speak from the point of tradition, primarily Catholic tradition, and the non-Catholic will quote from the Bible.  (Because of a watered-down or lack of catechesis in Catholic education, most Catholics do not know the tenants of their own faith.  More on this later.)  The Catholic could be speaking Maori (the language of New Zealand) and the non-Catholic Farsi (spoken in Iran) for all the good that it will do.

If the Catholic could quote from the Bible as well as the non-Catholic, he would have a greater chance of getting his point across.  I am not saying that we should leave tradition at home when we converse with non-Catholics, but what I am saying is that if both sides have a common source (the Bible) the discussion will go a lot better, especially for the Catholic.

# What Bible Should We Read?

You might think that you can grab any Bible off the shelf and start reading it.  Unfortunately, no.  In the middle of the 60s (around the same time as Vatican II), new Bible translations were released for the Catholic community which were changed from previous translations.  These new translations became more non-Catholic friendly and watered-down.  They removed words, such as Hell, soul, charity and host.

The Douay-Rheims version of the Bible has been the official Bible of the Catholic Church for many centuries and is the only true Catholic English translation of the Bible.  It was taken from the Latin Vulgate, which was pain-stakingly translated from the original Greek, Hebrew and Aramaic sources by St. Jerome.  He was much closer to those original sources than anyone now.[![DRB2a](images/drb2a_thumb1.jpg "DRB2a")](http://realromancatholic.files.wordpress.com/2010/07/drb2a1.jpg)

Besides, the Douay-Rheims version of the Bible has a certain poetic sound to it.  For most people, this is not important, but for a bibliophile (book nut) like me it is.  It’s like asking the difference between Ivanhoe by Sir Walter Scott and Red Storm Rising by Tom Clancy.  There is a certain beauty of verse and elegance of style in the former that is lacking in the later.

So, be sure to pick up a copy of the Douay-Rheims Bible and read so that you are ready to defend and spread your Faith.
