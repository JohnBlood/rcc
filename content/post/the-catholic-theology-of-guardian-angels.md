---
title: "The Catholic Theology of Guardian Angels"
date: "2013-10-12"
categories: 
  - "angels"
tags: 
  - "abbot-vonier"
  - "angels"
  - "guardian-angel"
---

[![GuardianAngel1](images/guardianangel1.png)](http://realromancatholic.files.wordpress.com/2013/10/guardianangel1.png)

_Since October is the month of the angels, I decided to write this article to remind people of their forgotten guardians._

Everyone is familiar with the imagery of a kindly angel watching over a child. While the most people recognize the picture of the guardian angel, few actually know what the Catholic Church says about guardian angels. My goal is to acquaint you with the theology behind guardian angels, based on the writings of St. Thomas Aquinas, Abbot Anscar Vonier, and others.

It is commonly thought that angels were created for us. This idea is correct, but it would be better to say that “man was created for the angels”. This means that the guidance and protection that the guardian angels give us during our lives has one ultimate goal: to ensure that one day we will be worthy of being their companions in Heaven.

In order to accomplish this goal each human has a guardian angel assigned to him by God. According to Tradition, guardian angels are not assigned to more than one person. Since the job of a guardian angel ends once the soul under his care has passed into eternal life, this one-angel-to-one-person ratio only applies to those of us who are on earth. According to St. Thomas, even the Anti-Christ, the Man of Sin, will have a guardian angel.

However, just because God assigns a guardian angel to us does not necessarily mean that the angel does not perform other tasks. Distance does not exist for angels because they are spirits. Our earthly bodies anchor us to this place because of their physical nature, but angels do not have this problem. When a guardian angel is acting on the human soul, he is present. When he has stopped acting, he has departed. St. Thomas Aquinas points out that the angel has complete knowledge of the soul in his charge, even when he is not present.

In order to perform their duty to the best of their ability, guardian angels are granted entire and intimate knowledge of the soul under his care. No other angel has access to this knowledge. Not even demons, who try to lead souls to Hell, have knowledge as intimate as our guardian angels.

[![GuardianAngel2](images/guardianangel2.png)](http://realromancatholic.files.wordpress.com/2013/10/guardianangel2.png)With this intimate knowledge, guardians angels have the ability to guide us. They can lead us to new trains of thought and new ideas when we are deep in thought. Angels introduce new thoughts to us so subtly, that they appear to be our own. They can help us make prudent decisions. St. Thomas remarks that if all virtue was given to a soul by God, the virtue of prudence would make external assistance necessary. Angels can also help correct our lower appetites, such as impure desires, when they go against the Divine good. The will, which should rule over these appetites, easily falls prey to them. However, angels are given the ability to satisfy and overcome these lower appetites.

While guardian angels can influence and guide us towards Heaven, the amount of help we receive depends on us. St. Thomas states that the power of a guardian angel to work on the interior life of man is dependant on how that man is spiritually disposed. An angel cannot change a person’s will or affect his intellect. These are things only God can do because He created both the will and the intellect. Guardian angels are limited in their influence by the moral state of the man in their care.

Like everything that God gives us, guardian angels are only beneficial if we would make use of them. Vonier states “Like all Divine gifts, it may be hidden forever in a napkin, and it may be made to produce a hundredfold.” A muscle that is neglected with atrophy, but exercise will strengthen it and make it strong. So too can calling upon the untapped guidance and wisdom of our guardian angels leads us to eternal life with them.
