---
title: "The Unfortunate Lack of Proper Catholic Capitalization"
date: "2012-02-06"
categories: 
  - "roman-catholic"
  - "tradition"
  - "writers"
tags: 
  - "catholic"
  - "catholic-literature"
  - "holy-sacrament"
  - "holy-sacrifice"
  - "proper-capitalization"
  - "tiny-acorn"
  - "writer"
  - "writing"
---

[![](images/teacher.png "Teacher")](http://realromancatholic.files.wordpress.com/2012/02/teacher.png)As a writer and reader of Catholic literature, there is one thing that always annoys me to no end: a lack of proper capitalization. What I mean is that modern Catholic writers fail to capitalize certain word that must should be capitalized to show proper honor and reverence towards what these words name.

Here are a couple of examples of what I mean.

First, when they refer to God (the Father, the Son or the Holy Ghost) they often use the common pronoun "he". In many ways, this is an insult to God. He's not a human being who can be referred to as "he". No, He is something more than man and must be shown that hight honor, even in written word.

Second, when they speak of the Holy Sacrifice of the Mass, most modern writers (even Catholics) fail to capitalize it. Instead of writing Mass, they write mass. What a difference a simple change can make. One form refers to a large grouping of something, the other refers to a life-giving sacrament. Again, this is an insult because a sacred action is given a common name. In many ways, I think this was started by anti-Catholics and was picked up by liberal Catholic who wished to reduce the honor and importance of that holy sacrament.

Hopefully, this short "sermon" will give inspire you to start using proper Catholic capitalization. It might seem a simple or insignificant thing to do, but even a might oak has to start as a tiny acorn. Give honor where honor is due.
