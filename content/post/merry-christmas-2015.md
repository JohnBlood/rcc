---
title: "Merry Christmas 2015"
date: "2015-12-24"
categories: 
  - "christmas"
coverImage: "merry-christmas.png"
---

[![Merry Christmas](images/merry-christmas.png)](https://realromancatholic.files.wordpress.com/2015/12/merry-christmas.png)I would like to wish all the readers of RealRomanCatholic a Merry Christmas and a Holy and Happy New Year.

As a Christmas present, I have dropped the price of [Church Triumphant](http://www.amazon.com/Church-Triumphant-Women-Their-Christ-ebook/dp/B00UZDNN0Q/ref=asap_bc?ie=UTF8) to 99cents. Start the New Year reading about the saints so that you too can become a saint.
