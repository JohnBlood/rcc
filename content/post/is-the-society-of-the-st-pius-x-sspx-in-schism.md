---
title: "Is the Society of the St. Pius X (SSPX) In Schism?"
date: "2014-01-23"
categories: 
  - "schism"
  - "sspx"
tags: 
  - "schism"
---

In December Archbishop (soon to be Cardinal) Mueller told an Italian newspaper that the leaders of the Society of St. Pius X was in schism. As you might expect, many SSPX supporters quickly jumped to the Society's defense. Since I have talked about this organization many times before, I decided I would cover this question and study it based on canon law. I am not an expert in canon law, but I will be quoting relevant canons from the Code of Canon Law of 1917. I will be using the 1917 Pio-Benedictine Code of Canon Law from Ignatius Press. (Interestingly, it is called Pio-Benedictine because the project to compile canon law was started by Pope St. Pius X and finished by Pope Benedict XV).

**Definition of Schism**

Before we examine the actions of those involved, let's define our term. The 1917 Code of Canon Law Canon defines schism in canon 1325 paragraph 2:

> If finally he refuses to be under the Supreme Pontiff of refuses communion with the members of the Church subject to him, he is a schismatic.

What is the penalty for schism? That can be answered with canon 2314, paragraph 1:

> All apostates from the Christian faith and each and every heretic or schismatic:
> 
> 1. Incur by that fact excommunication;
>     
> 2. Unless they respect warnings, they are deprived of benefice, dignity, pension, office, or other duty that they have in the Church, they are declared infamous, and if clerics, with the warning being repeated, they are deposed;
>     
> 3. If they give their names to non-Catholic sects or publicly adhere to them, they are by that fact infamous, and with due regard for the prescription of Canon 188, n 4, clerics, the previous warning having been useless, are degraded.
>     

Essentially, anyone who refuses to obey the Pope, is in schism. By extension, anyone who is in schism is excommunicated. That seems simple enough. Now let's move on to the events surrounding the 1988.

**What Happened**

After its foundation 1970, the Society of St. Pius X was constantly in conflict with Holy Mother Church. Archbishop Lefebvre, the founder of the SSPX, was the main source of ordinations for the society. In 1987, when Lefebvre was in his 80s, he announced his decision to consecrate a new bishop to take his place. However, he was prepared to do so without papal approval, which is against canon law.

Canon 332, paragraph 1 states:

> Whoever is to be promoted to the episcopate, even if he is elected, presented, or designated even by a civil Government, needs canonical provision or institution by which the Bishop is constituted in a vacant dioceses, which only the Roman Pontiff can give.

Canon 953 adds:

> The consecration of a Bishop is reserved to the Roman Pontiff so that it is not permitted to any Bishop to consecrate another as Bishop without first having gotten a pontifical mandate.

After talks, Lefebvre signed an agreement with the Vatican that would make his organization canonically recognized and allow him to consecrate one bishop. Upon returning to Switzerland, Lefebvre changed his mind and announced he would consecrate four bishops, without papal approval. Pope John Paul II even sent Lefebvre a personal letter asking him to reconsider and reminding him of the penalties he was about to incur. The consecrations took place on June 30, 1988. The Vatican replied by issuing a decree stating that those who took part in the consecrations were excommunicated. Canon 2370 covers this issue.

> A Bishop consecrating another Bishop, and the assisting Bishops or, in the place of Bishops, priests, and those who receive consecration without an apostolic mandate against the prescription of Canon 953 are by the law suspended until the Apostolic See dispenses them.

After examining the pertinent portions of canon law, I think it is safe to say that SSPX is in schism.

**Emergency?**

Most SSPX supporters try to get around the penalties of canon law by claiming a state of emergency within the Church. They claim that if Lefebvre had not consecrated the four bishops, there would have been no one to ordain new traditionalist priests. This falls flat for two reasons. First, the Vatican had given Lefebvre permission to consecrate one bishop to replace him. This would have kept the Church supplied for years. Second, there would have been "outside" bishops. For example, the Priestly Fraternity of St. Peter and the Institute of Christ the King Sovereign Priest both depend on bishops from outside the organizations for ordination. These bishops include Archbishop Sample, Cardinal Burke, Cardinal Hoyos and others.

**Thoughts of a Former Member of SSPX**

During an interview with the Wanderer newspaper, former SSPX member Fr. John Emerson FSSP said the following about SSPX and schism:

> **Q. We can get off on some of these other groups and associations later. Right now, you raised an interesting point _en_ _passant_. You spoke of the consecration of the four bishops \[by Archbishop Lefebvre\] as not being a ‘necessary disobedience.’ I wonder If you would speak to the difference between disobedience and schism.** A. Yes, well, for example, the priests that Monsignor Lefebvre ordained, including myself, I never believed, and it was entirely clear from their attitude that Rome never seriously thought, this was in any way a schismatic act. It was purely disobedient. It was a disobedience which many of us were willing to go ahead and commit because we believed it was absolutely essential for the life: of the Church, for the health of the Church, that traditional liturgy continue and not simply disappear, and it seemed at the time that was the only way it was going to continue. Now, that can be argued, but in conscience we felt fully able to go ahead and do that without sin, without any kind of real suspension. The consecration of bishops, on the other hand, is a clear act of schism, of breaking with the head of the Church, of setting up a parallel hierarchy, setting up an independent church. Of course that is exactly what schism is. It is not to be a heretic, which is to say, no, the Pope is, not head of the Church. It is to say, yes, he is, but we are simply going to ignore that and go ahead on our own. Another part of schism, interestingly, is this: Even if one continues to say, yes, we are under the Pope, we obey the Pope, but it is then to break entirely with those who also are under the Pope, and that the Society of St. Pius X also has done very clearly since the break by considering utterly traditional groups like us, like the Abbey at La Barrou in France , to be now part of the Modernist conspiracy, if you like. In truth, we are in every way traditional, but we accept the Pope and we obey him. Therefore we are outside the pale. That, too, is classically the schismatic mentality. **It’s clear the Society \[of St. Pius X\] has accepted the dangers of a real schism and is just plowing ahead.**

You can read the rest of the interview [here](http://realromancatholic.com/2013/07/14/fr-john-emerson-fssp-speaks-on-the-original-sspx-break-with-rome/).

**Current Situation**

Pope Benedict XVI lifted the excommunication for the four bishop in 2009, but that does not change things. Just because an excommunication is lifted, it does not mean that the person who was excommunicated is automatically returned to full communion with the Church. For example, when the Great Schism (or East-West Schism) took place, the Pope and the patriarch excommunicated each other. Those orders of excommunication were lifted in 1995 by Pope John Paul II and Patriarch Bartholomew I of Constantinople. Even though the excommunication is lifted, the Orthodox Church is still separated from the Roman Catholic Church.

Pope Benedict XVI made the situation clear in 2009.

> The fact that the Society of Saint Pius X does not possess a canonical status in the Church is not, in the end, based on disciplinary but on doctrinal reasons. As long as the Society does not have a canonical status in the Church, its ministers do not exercise legitimate ministries in the Church. There needs to be a distinction, then, between the disciplinary level, which deals with individuals as such, and the doctrinal level, at which ministry and institution are involved. In order to make this clear once again: until the doctrinal questions are clarified, the Society has no canonical status in the Church, and its ministers – even though they have been freed of the ecclesiastical penalty – do not legitimately exercise any ministry in the Church. - Pope Benedict XVI March 10, 2009

**Final Thoughts**

I agree with SSPX on several points. I agree that Vatican II was a major rupture with the past. I also agree that the Latin Mass is the premier form of worship and that the pre-Vatican II sacraments are more powerful. However, I do not agree with Lefebvre's actions. Nor do I agree that the English Mass invalid, it is watered down but it is still valid. There are god people in the SSPX, but they are misled by men who like leading their own church. Please keep SSPX in your prayers. Pray that they will return to return to communion with the Holy See.
