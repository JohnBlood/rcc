---
title: "Second Word - Amen I say to thee: This day thou shalt be with me in paradise. (Luke 23:43)"
date: "2014-03-21"
categories: 
  - "last-seven-words-of-jesus"
  - "lent"
---

\[caption id="attachment\_886" align="alignright" width="520"\][![This Day Thou Shalt Be With Me In Paradise](images/thisdaythoushaltbewithmeinparadise.png)](http://realromancatholic.files.wordpress.com/2014/03/thisdaythoushaltbewithmeinparadise.png) "Amen I say to thee: This day thou shalt be with me in paradise." (Luke 23:43)\[/caption\]

As Our Blessed Lord hung dying on the Sacred Cross, His ears are filled with words of abuse hurled at Him by ungrateful sinners. In the midst of this vulgarity and injury, one voice alone speaks out in His defense. Rebuking the blasphemous robber, the Good Thief (traditionally known as St. Dismas) said "'Neither dost thou fear God, seeing; thou art under the same condemnation? And we indeed justly: for we receive the due reward of our deeds. But this man hath done no evil.' And he said to Jesus: 'Lord, remember me when thou shalt come into thy kingdom.'" (Luke 23:40-2, DRB) Upon hearing this, Our Lord assured him that he would see paradise that day.

Imagine what it must have been like to witness the excruciating pain that Our Lord went though from such a close proximity. It could only lead to repentance. The Good Thief must have had a special grace to recognize Jesus' Divine innocence. We should beg God to give us the grace to avoid sin so that we may never again cause the Innocent Lamb of God so much pain. Let us not be among those who hurl abuse at Jesus, but let us be those among who strive to bring joy to His Heart by renouncing satan and all his empty promises and repenting of our sinfulness. Amen.
