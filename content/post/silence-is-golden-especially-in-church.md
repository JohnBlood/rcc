---
title: "Silence is Golden - Especially in Church"
date: "2011-05-29"
categories: 
  - "discipline"
  - "duty"
  - "liberalism"
  - "lukewarm"
  - "mass"
  - "parents"
  - "priest"
  - "sin-of-omission"
tags: 
  - "catholic"
  - "duty"
  - "parents"
  - "priest"
---

Last Sunday, as I knelt for Holy Communion, a little boy followed closely by his mother darted past me and towards the altar. This Sunday, a young mother hurried down the main and side aisles in pursuit of her baby daughter. Every Sunday, the church full of the sound of children making crying and making noise, often it's so bad the priest's sermon can't be heard.

The sad thing is that this is not a rare occurrence. It happens every Sunday. The parents never seem to be interesting in keeping their children quiet.

This upsets me because it is shows (whether the parents realize it or not):

- Lack of reverence towards Our Lord present in the Blessed Sacrament
- No respect for the prayer life of others
- Lack of discipline of both the parent and the child
- Lack of respect for the priest, who is trying to worship God

You may protest and say, "But it's so difficult to keep children quiet in church." Sadly, you are wrong. All you have to do is cover the child' mouth with your hand or take them out of church. I said take them out of church. I don't mean stand in the back, so besides screaming the children are now playing with the hymn books. Please take them out and quiet them.

As I said, part of the problem is lack of discipline. Today, parents want to be seen as the child's friend and not their parent. This is a grave error which will lead many souls to HELL. Remember the saying: "The road to Hell is paved with good intentions."

What does the Bible say about disciplining children? Keep reading.

> "He that loveth his son, frequently chastiseth him, that he may rejoice in his latter end, and not grope after the doors of his neighbors." (Ecclesiasticus 30:1)
> 
> "A horse not broken becometh stubborn, and a child left to himself will become headstrong." (Ecclesiasticus 30:8)
> 
> "Give him not liberty in his youth, and wink not at his devices. Bow down his neck while he is young, and beat his sides while he is a child, lest he grow stubborn, and regard thee not, and so be a sorrow of heart to thee." (Ecclesiasticus 30:11-12)
> 
> "He who spareth the rod hateth his son: but he that loveth him correcteth him betimes." (Proverbs 13:24)
> 
> "Withhold not correction from a child: for if thou striketh him with a rod he shall not die. Thou shalt beat him with a rod, and deliver his soul from hell." (Proverbs 23:13-14)

"But that was the Old Testament," some will reply. "We're under the New Law and that does not apply." Remember what it said on the Gospels after Our Blessed Mother and St. Joseph found Our Lord in the Temple. He returned to Bethlehem and was obedient to them.

In _Examination of Conscience for Adults: A Guide to Spiritual Progress_, Fr. Donald F. Miller lists as a mortal sin, "Have I failed to correct and punish my children for serious wrongs, or to forbid them to enter serious occasions of sin?" (p. 77, +Imprimatur: Archbishop Moses E. Kiley, May 12, 1942). As Fr. Louis Colin emphasizes, "Who loves much, chastises much." (Love One Another, p. 189)

My Dad once told a young mother after Mass that her son's crying and talking was making made it very hard to meditate and pray. She told him, in so many words, that it was none of his business. I'm sorry, but it is his business, especially if he is trying to worship his God and can't consecrate because when someone is not minding their child.

The priests seem to be be afraid of asking the parents to keep the children quiet for fear that they might be offended. I told several priests that the children crying made it impossible to hear the sermon. One laughed and others said that the noise was music to God. That me true any other time, but not in church.

Remember the old saying that long been out of fashion in this politically correct world: "Children should be seen, not heard."
