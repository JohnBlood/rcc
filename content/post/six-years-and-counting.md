---
title: "Six Years and Counting"
date: "2011-05-02"
categories: 
  - "anglican"
  - "bishops"
  - "pope-benedict-xvi"
  - "priest"
  - "roman-catholic"
  - "tradition"
tags: 
  - "catholic"
  - "church-of-england"
  - "latin"
  - "latin-mass"
  - "mass"
  - "pope"
---

\[caption id="attachment\_264" align="alignright" width="300" caption="Pope Benedict XVI waves to the crowds after his election."\][![](http://realromancatholic.files.wordpress.com/2011/05/popebenedictxvielected.png?w=300 "Pope Benedict XVI waves to the crowds after his election.")](http://realromancatholic.files.wordpress.com/2011/05/popebenedictxvielected.png)\[/caption\]

April 19, 2011 marked the sixth anniversary of Cardinal Ratzinger being elected Pope of the Roman Catholic Church by the College of Cardinals through the intercession of the Holy Ghost. It is really quite amazing what he had accomplished in those six years. Here is my list of the achievements of Pope Benedict's first six years. Let me know which achievements I may have missed.

1. Allowing any priest in the world to offer the Latin Tridentine Mass without needing to request permission from the bishop.
2. A reform of the reform. This includes a more careful translation of the English Mass.
3. Bringing traditional Anglicans into the True Church.
4. Filling the powerful positions of the Church with conservative prelates.
5. Cracking down on renegade orders.

If he has been able to accomplish this much in **6 years**, what will the next 6 bring? Keep Pope Benedict in your prayer.
