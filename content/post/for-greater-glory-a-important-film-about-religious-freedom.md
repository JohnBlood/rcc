---
title: "For Greater Glory - A Important Film About Religious Freedom"
date: "2012-06-06"
categories: 
  - "movie-review"
  - "roman-catholic"
---

[![](http://realromancatholic.files.wordpress.com/2012/06/forgreatergloryfilm.png?w=293 "For Greater Glory Film")](http://realromancatholic.files.wordpress.com/2012/06/forgreatergloryfilm.png)This is the first of many movie reviews to come. My goal is to highlight films which Catholics can watch without risking their souls. For my first review, I would like to talk about "For Greater Glory."

Year after year, Hollywood produces films that are the equivalent of the Roman circuses, they keep the people from thinking. However, every once in a while a film gets released that smacks viewers back to reality. Such a film was released this last Friday. It was called For Greater Glory. Set in the 1920s in Mexico, this film tells the mostly forgotten story of a Catholic uprising against a secular and atheist government.

The film starts with President Calles ordering the enforcement of the anti-clerical laws. (Calles was later given an award by the Mexican Scottish Freemasons for his actions against Catholics. Pope Pius XI condemned these actions in [_Iniquis Afflictisque_](http://www.papalencyclicals.net/Pius11/P11INIQU.HTM)) The first response of the Catholics to find peaceful methods, such as protests and economic boycott. After the government cracks down on Catholics further, uprisings break out across the country. Many armed groups covered the country, each following their own leader. The Catholic leaders hire retired general Enrique Gorostieta to unite the groups and create a fighting force.

The cast includes many award-winning actors, such as Academy Award nominee Andy Garcia, Golden Globe winner Eva Longoria, Oscar winner Peter O'Toole, Eduardo Verástegui, and new actor Mauricio Kuri. All of these actors give incredible performances., but Kuri, a relatively new actor, steals the show as [Blessed Jose Sanchez del Rio](http://ourfaithinaction.net/2004/jose-luis-beatified/).

This is an important film about persecution and is very timely. There are dramatic parallels between the time period the movie is set in and today. Go watch it. See what awaits us if we do not act now.

\[youtube=http://www.youtube.com/watch?v=wI8zu9lu2dk\]
