---
title: "Pictures from Pentecost 2013 High Mass"
date: "2013-06-23"
categories: 
  - "altar-boy"
  - "juventutem-michigan"
  - "latin-mass"
  - "priest"
  - "roman-catholic"
tags: 
  - "catholic"
  - "latin-mass"
  - "pentecost"
---

I've been meaning to post these pictures, but I have been very busy lately. On Pentecost Sunday, my parish church had the first Latin Mass since the new church building was constructed in the 1970s. The Mass was sponsored by Juventutem Michigan, a Catholic youth group dedicated to the Latin Mass. The Mass was offered by Monsignor Edward A. Hankiewicz. My brother Michael and I served the Mass. This was the first High Mass the we ever served, we have served Low Mass many times. A friend of ours brought two of his brothers to help. There were about 60 people in attendance, which was great for a first time. Please pray that we can get the Latin Mass on a regular basis at St. Mary's.

\[gallery ids="691,739,738,737,736,735,734,733,732,731,730,729,728,727,726,725,724,723,685,722,721,720,719,718,717,716,715,714,713,712,711,710,709,708,686,684,707,706,705,704,703,702,701,700,699,698,697,696,687,695,694,693,692,690,689,688"\]
