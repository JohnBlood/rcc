---
title: "Merry Christmas"
date: "2014-12-25"
categories: 
  - "christmas"
---

\[caption id="attachment\_980" align="aligncenter" width="660"\][![Merry Christmas](https://realromancatholic.files.wordpress.com/2014/12/gerard_van_honthorst_001.jpg?w=660)](https://realromancatholic.files.wordpress.com/2014/12/gerard_van_honthorst_001.jpg) Merry Christmas\[/caption\]

I would like to wish all of my readers a Very Merry Christmas and a Holy and Happy New Year. You are all in my prayers as we near a new year. May this be a time of peace and holiness for each of you. Be sure to check out my Christmas story [_Silent Night, Holy Night_](http://realromancatholic.com/2010/12/25/silent-night-holy-night-a-christmas-story/).

God Bless,

John Paul Wohlscheid
