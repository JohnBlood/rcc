---
title: "Thrid Word - Woman, behold thy son...Behold thy mother. (John 19:26-7)"
date: "2014-03-25"
categories: 
  - "last-seven-words-of-jesus"
  - "lent"
---

\[caption id="attachment\_889" align="alignright" width="279"\][![WomanBeholdYourSon](images/womanbeholdyourson.png)](http://realromancatholic.files.wordpress.com/2014/03/womanbeholdyourson.png) Woman, behold thy son...Behold thy mother. (John 19:26-7)\[/caption\]

Sinful man, behold the sorrowful face of Our Blessed Mother. She, who through her acceptance of God's will brought the Son of God into the world, now sees Him stretched between heaven and earth suffering unbearable torments for your sake. This Mother, who accepted God's Gift to the world with great joy, is now overcome with great sorrow to see Him who is Innocent put to death for our sakes. Weep. o sinful man, for you and your sinful habits are the cause of her sorrow.

Looking down on His Most Holy Mother, the Savior of the world gives her a parting gift: sinful mankind. With four words He gives us who have crucified Him into her care, so that she may care for us with the same kindness and dedication as she had for Him. The sorrow at losing her only Son is replaced with the sorrow of a mother who is forced to watch as her children blindly go down the path to destruction.

But Our Saviour is not finished. Turning to St. John and speaking through him to us, He reminds and warns us to honor His mother. How can we return to sin when we remember that our sin hurts Our Blessed Mother twice? First, we hurt her when our sin adds to Our Lord's suffering. Second, just like any other mother, Our Blessed Mother is saddened to the point of tears when we turn from the narrow path that leads to Salvation and instead take the wide path that leads to Eternal Damnation.

O, Most Blessed Mother, I beg that you forgive me for all that I have done to offend thee and thy Most Holy Son. I beg you further to intercede for your Son on my behalf. I deserve Eternal Punishment for my continual offenses against both thee and they Son. Take me by the hand so that I may never again offend thee and help me to grow in virtue that I may make reparation for my offences Amen.
