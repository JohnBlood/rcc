---
title: "Easter Comic You Must See"
date: "2013-04-01"
categories: 
  - "archbishop-fulton-j-sheen"
  - "comic"
tags: 
  - "archbishop-sheen"
  - "comic"
  - "happy-easter"
---

Happy Easter to everyone. I know it may be a bit late, but it still counts as Easter.

Today, when I was reading the comics (the only good part of the newspaper) I came across this comic. I hope you enjoy it as much as I did.

[![MallardFillmoreEaster2013](images/mallardfillmoreeaster2013.png)](http://realromancatholic.files.wordpress.com/2013/03/mallardfillmoreeaster2013.png)
