---
title: "Thoughts on the Passion of Christ"
date: "2009-08-15"
categories: 
  - "bible"
  - "roman-catholic"
  - "saints"
tags: 
  - "christ"
  - "jesus"
  - "love"
  - "martyr"
  - "passion"
  - "saint"
---

Today is the feast of St. Maximilian Kolbe, martyr.  St.[![St_Kolbe_Prayer_Card](images/st_kolbe_prayer_card_thumb1.jpg "St_Kolbe_Prayer_Card")](http://realromancatholic.files.wordpress.com/2009/08/st_kolbe_prayer_card1.jpg) Maximilian was a Polish Franciscan, who was imprisoned by the Nazis in the notorious Auschwitz concentration camp.  It was here that he traded places with a family man sentenced to die to discourage prisoners from escaping.  This selfless act was the main cause of St. Maximilian’s canonization in 1982.  His sacrifice is an example to all Christians of how they should be willing to lay down their lives for the good of others.

Your probably asking what this has to do with the Passion of Christ.  Well, today my brother and I were serving Mass and the priest was telling how similar St. Maximilian’s sacrifice was to Jesus’ when He died on the Cross.  This started to get me thinking and meditating on the Passion of Christ and what it means.

According to The American Heritage® Dictionary of the English Language, Fourth Edition published by Houghton Mifflin Company, passion is defined as 1) A powerful emotion, such as love, joy, hatred, or anger; 2) ardent love.

For me, this sums it all up.  Christ loved us so wholly, so unconditionally, and so entirely that He suffered a cruel, painful, and humiliating death.  He loved us with every drop of blood that left His body.  Every time the whip of His tortures fell upon His body, His love for us increased.  He did not want us to be destroyed because of our sins.  Because of all the love that Jesus showed for us through the tortures that He endured, I think that it should be called the Way of Love, not Way of the Cross.[![](images/passionofthechrist_thumb1.jpg)](http://realromancatholic.files.wordpress.com/2009/08/passionofthechrist.jpg)

Not only did Jesus love us, but His Eternal Father loved us, as well,  The often quoted passage John 3: 16 states, “For God so loved the world, as to give His only begotten Son: that whosoever believeth in Him may not perish, but may have life everlasting.”

The question is: do we love our fellow man so much and ourselves so little that we can give up something for the benefit of others?  If we were given the chance, would we go so far as to lay down our lives for other or for the Church?  We have have that opportunity sooner that we want to think about.

We need to keep in mind that every time we sin, we are causing Jesus even more pain.  Every time we sin, we are driving the nails deeper into His hands; we are driving the throne deeper into His Divine Head.  If we keep this in our minds always, hopefully we will be able to keep temptation away and sin at arms length.
