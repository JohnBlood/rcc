---
title: "First Word - Father, forgive them, for they know not what they do. (Luke 23:34)"
date: "2014-03-10"
categories: 
  - "last-seven-words-of-jesus"
---

\[caption id="attachment\_898" align="alignright" width="500"\][![Father, forgive them, for they know not what they do. (Luke 23:34)](images/fatherforgivethemfortheyknownotwhattheydo.png)](http://realromancatholic.files.wordpress.com/2014/03/fatherforgivethemfortheyknownotwhattheydo.png) Father, forgive them, for they know not what they do. (Luke 23:34)\[/caption\]

(Note: This is the first of seven meditations on the Seven Last Words of Christ. )

With this one phrase Jesus begs His Heavenly Father to have mercy on us. Why? Because of our ignorance. We are ignorant of God's plan for us. We are ignorant of how often and how deeply we hurt God the Father. He has given us such great blessing and how to we repay Him? By sinning in our ignorance. We choose to overlook and ignore the pain that our actions cause God because they give us some sort of pleasure. However, this pleasure is a passing pleasure, which will turn to pain later on.

How often have we injured others with our thoughtless words or deeds? Too often. Just so, we often hurt and offend God by not thinking about our actions and their consequences. Jesus is asking His Father to forgive us; but for forgiveness to be complete we must repent for our sins. God may forgive us repeatedly, but if we continue to sin, the measure of the punishment due to sin continues to build. We must now repent  of our ignorance. We must repent in a heartfelt manner, not just with words or thoughts, but with actions too. Lord Jesus Christ, give me the grace and the strength to overcome my ignorance and never hurt You again. Amen.
