---
title: "Hitler Attacks Pope Benedict Video"
date: "2010-03-03"
categories: 
  - "orthodox"
  - "roman-catholic"
---

In 2004, the film _Downfall_ was released chronicling the last 10 days of Adolf Hitler’s life.  One scene in particular, where Hitler launches into a furious tirade once he realizes that the war is lost, has been widely used in parody.  People have changed to subtitles so that Hitler reacts to a wide variety of subjects.  One of the latest and best concerns Pope Benedict XVI.  Watch and enjoy.

\[youtube=http://www.youtube.com/watch?v=b12aglwd8QY&feature=player\_embedded\]
