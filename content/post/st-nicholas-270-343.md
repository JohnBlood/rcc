---
title: "St. Nicholas (270 - 343)"
date: "2012-12-05"
categories: 
  - "heresy"
  - "priest"
  - "roman-catholic"
  - "saints"
---

\[caption id="attachment\_569" align="alignright" width="600"\][![St. Nicholas and Arius](images/stnicholasandarius.png)](http://realromancatholic.com/?attachment_id=569) St. Nicholas and Arius\[/caption\]

St. Nicholas was born in 270 in the Greek city of Patara. He was the only child of wealthy Christian parents. From a very young age, Nicholas was very religious. According to legend, he would observe the traditional fasts on Wednesday and Friday by only eating one meal. His parents died while he was still young and he went to live with his uncle, also named Nicholas, who was a bishop. His uncle later ordained Nicholas a priest.

Nicholas had inherited a sizable fortune from his parents and decided to devote those funds to works of charity. There are many tales of his generosity. One of these stories had to do with a poor man and his three daughters. The man could no longer afford to care for his daughters and could not find them husbands because of their poverty. The father had no choice but to give his daughters over to a life of sin. When Nicholas heard of this, he took a bag of gold and threw it through the man’s window at night. Upon finding it the next morning, the man used it as a dowry for his oldest daughter, who was soon married. Nicholas did the same two more times, for each of the remaining daughters. The third time, the poor man concealed himself to discover the identity of his benefactor. Upon seeing Nicholas toss the sack of gold in his window, the man rushed up and profusely thanked the saint.

Sometime later, Nicholas became the bishop of Myra. It happened that while the people of the city were electing a new bishop, God indicated that Nicholas was the man to be chosen. This was at time of great persecution for the Church. Because of his position as religious leader of the city, Nicholas was imprisoned and tortured by the local officials. However, when Constantine became emperor and issued the Decree of Milan in 313, Nicholas and the other Christian captives were freed. Through his constant and tireless efforts Nicholas kept his diocese free from the heresy of Arianism, which taught that Jesus Christ was not equal to God the Father in the Trinity.

In order to correct the errors and heresy of Arius, the Emperor Constantine convened the First Ecumenical Council of Nicaea in 325. During the course of this council and while the heretic Arius was speaking, Nicholas could endure his blasphemy no longer. Rising, he punched Arius, knocking him to the floor. This act so shocked everyone that Nicholas was stripped of his copy of the Gospels and his pallium, both signs of his office. On top of this, Nicholas was also thrown in jail. That night Our Lord and Our Lady appeared to him. Our Lord asked, “Why are you here?” Nicholas replied, “Because I love you, my Lord and my God.” In replied, Our Lord gave the copy of the Gospels that had been taken from him and Our Lady vested him with the pallium. When he heard the Nicholas had been restored to his ecclesial position by Heaven, Constantine ordered that Nicholas be released at once.

Nicholas also cared for the temporal welfare of his people. Once, the local governor Eustathius was bribed to condemn three innocent men to death. Before they could be executed, Nicholas appeared and stopped the execution. He berated Eustathius until he admitted that he had been bribed. He freed them and confessed his sin. St. Nicholas died in the year 343.
