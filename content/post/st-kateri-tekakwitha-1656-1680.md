---
title: "St. Kateri Tekakwitha (1656 - 1680)"
date: "2012-11-30"
categories: 
  - "roman-catholic"
  - "saints"
tags: 
  - "catholic"
  - "native-american"
  - "saint"
---

In honor of Pope Benedict’s canonization of two new saints back in October, here is a biography for St. Kateri Tekakwitha.

\[caption id="attachment\_559" align="alignright" width="213"\][![](images/stkateritekakwitha.jpg)](http://realromancatholic.com/?attachment_id=559) St. Kateri Tekakwitha\[/caption\]

St. Kateri Tekakwitha was born around 1656 in a Mohawk village near today’s Auriesville, New York. Her father was a pagan Mohawk chief and her mother was Roman Catholic Algonquin who had been captured by the Mohawks. When she was four years old, Kateri’s parents and brother died from an outbreak of smallpox. The disease also left Kateri with scars and impaired vision.

Now an orphan, Kateri was adopted by her maternal uncle, a chief of the Turtle Clan. In 1667, Jesuits missionaries visited the village of Caughnawauga where she lived. During their three day stay, Kateri took care of them and was impressed by their piety and kindness. He uncle was unhappy with her contact with the Jesuits because he did not want her to convert. However, God had other plans. In 1675, she started studying catechism under the Jesuit Father Jacques de Lamberville. The following year she was baptized on Easter Sunday 1676.

Her conversion resulted in persecution. Her uncle threatened her with death and some Mohawks accused her of sorcery and promiscuity. Finally, Fr. Lamberville suggested that she escape to the Jesuit mission of Kahnawake near Montreal, Canada. In the fall of 1677, Kateri and three other converts managed to escape and make their way to the mission. It was here, under the guidance of her mother’s best friend, that Kateri’s faith grew and deepened.

While in Caughnawauga, Kateri and other converts had practiced mortification of the flesh and devoted their bodies to God. They often did this in a group to relieve their people of their past sins. Kateri would put thorns on her sleeping mat and lie on them while praying for the conversion of her kinsmen. Her relatives had tried many times to get Kateri to marry, but she instead wanted to live a life of chastity. She said, “I have deliberated enough. For a long time my decision on what I will do has been made. I have consecrated myself entirely to Jesus, son of Mary, I have chosen Him for husband and He alone will take me for wife.” Every morning, even in the worst weather, Kateri would rise early and wait outside the chapel until the doors were unlocked at four AM. She would remain until after the last Mass finished.

Kateri’s health had never been particularly good and it worsened as time went on. Her spiritual directors feared that her bodily mortification was causing her health to deteriorate. Fr. Cholonec suggested that Kateri join in the winter hunt to restore her health. She replied. “"It is true, my Father, that my body is served most luxuriously in the forest, but the soul languishes there, and is not able to satisfy its hunger. On the contrary, in the village the body suffers; I am contented that it should be so, but the soul finds its delight in being near to Jesus Christ. Well then, I will willingly abandon this miserable body to hunger and suffering, provided that my soul may have its ordinary nourishment.” During Holy Week of 1679, Kateri’s health began to fail. She died on April 17, 1679 at 24 years of age. After her death, the smallpox scars that had marked her face in life were miraculously healed and her face became remarkably beautiful. Kateri’s gravestone reads: Kateri Tekakwitha - The fairest flower that ever bloomed among red men.” She was canonized by Pope Benedict XVI on October 21, 2012.
