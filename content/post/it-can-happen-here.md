---
title: "It Can Happen Here"
date: "2012-06-25"
categories: 
  - "history"
  - "persecution"
  - "priest"
  - "religious-freedom"
  - "socialism"
tags: 
  - "catholic"
  - "film"
  - "priest"
---

[![](http://realromancatholic.files.wordpress.com/2012/06/persecutepriest.png?w=300 "PersecutePriest")](http://realromancatholic.files.wordpress.com/2012/06/persecutepriest.png)In a recent review of the film _For Greater Glory_, a priest said that while it was a good film, there were no parallels between that time period of Mexican history (1920s) and present day US. He stated that the US Bill of Rights would never allow the US government to persecute Catholics like the Mexican government.

The priest in question has a lot of faith in a sheet of antique paper. Don't get me wrong, the Bill of Rights is a great document that guarantees our freedom. However, there have been many great documents down through history that have guaranteed freedom that have been set aside by evil, corrupt rulers. Take the Magna Carta as an example. This freedom guaranteeing document was set aside repeatedly by Queen Elizabeth and other Protestant, English monarchs who persecuted Catholics mercilessly.

A country can have hundreds of freedom protecting laws, but if the people in government (who are supposed to be enforcing those laws) find them inconvenient, they will have not  problem ignoring these laws. Remember it has happened else where before many time and it can always happen here.

Constant prayer is the only thing that will save us. Pray and pray that we may defeat the attempts by people in power to take away our religious freedoms.
