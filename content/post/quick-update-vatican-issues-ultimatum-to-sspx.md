---
title: "Quick Update: Vatican Issues Ultimatum to SSPX"
date: "2013-02-15"
categories: 
  - "quck-update"
  - "return-to-tradition"
  - "roman-catholic"
  - "sspx"
tags: 
  - "catholic"
  - "pope-benedict-xvi"
  - "quick-update"
---

\[caption id="attachment\_599" align="alignright" width="500"\][![Bridge Across the River Tiber](images/bridgeacrosstiber.png)](http://realromancatholic.files.wordpress.com/2013/02/bridgeacrosstiber.png) The Vatican offers SSPX another change to cross the Tiber and come home to Rome.\[/caption\]

Today it was discovered that the Congregation for the Doctrine of the Faith have issued an ultimatum to the Society of St. Pius X. This ultimatum gives the hierarchy of SSPX until February 22nd to restart doctrinal talks with the Vatican. If they fail to do so, the Vatican will giving individual SSPX priests the opportunity to return home to Rome.

This is a very smart move because the main impediment to the SSPX's reunion with Rome is the hierarchy. Fellay pretends to be interested in reuniting with Rome, but he spends most of his time dragging his feet. The other bishops are outright hostile to the idea. In fact, Fellay recently said that there would be no reunion under the current pope (Pope Benedict XVI). This new move gets around this roadblock and reaches out directly to the priests who love tradition, but also want to return to the Roman Catholic Church.

From the comments I have read, some people think that Pope Benedict would never take a move like this. They seem to think that this is the work of a cardinal working on their own authority. I am of the opposite opinion. Msgr. Georg Ratzinger, Pope Benedict's older brother, said one of the things that hurt his brother the most during his time as pope was the lack of cooperation from the SSPX in his many efforts to bring them back to the Roman Catholic Church. Pope Benedict was involved with relations between Rome and the SSPX from the beginning and he did a lot to try to resolve the break. He seems to be trying to make one final try before he abdicates on February 28th. Let's pray that it works and Pope Benedict's successor brings SSPX into the True Fold, with or without their stubborn hierarchy.
