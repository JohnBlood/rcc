---
title: "God, Family, Ducks"
date: "2014-01-07"
categories: 
  - "duck-dynasty"
  - "family"
  - "tv"
tags: 
  - "christian"
  - "family"
  - "phil-robertson"
  - "tv"
---

\[caption id="attachment\_861" align="alignright" width="259"\][![Phil Robertson, head of the Robertson clan and creator of the Duck Commander duck call.](images/philrobertson.png)](http://realromancatholic.files.wordpress.com/2014/01/philrobertson.png) Phil Robertson, head of the Robertson clan and creator of the Duck Commander duck call.\[/caption\]

Most people should be away for the record-breaking, extremely popular reality TV show: Duck Dynasty. It is loved by the majority of Americans and hated by a small, ignorant minority. Recently, this insignificant minority decided to flex its muscle, all because the head of the Robertson family decided to paraphrase the Bible, admittedly in a rather coarse way. A&E, the channel that broadcasts Duck Dynasty, immediately bowed to the wishes of the minority and announced that they would suspend the resulting cast member. What resulted was the biggest outcry by Americans that I have every seen. Millions protested A&E's decision. Viewership plummeted. Wal-Mart sold out of all Duck Dynasty material. Fans put pressure on Duck Dynasty sponsors to continue to support the show. Glen Beck offered the Robertson family a place on his channel if they decided to leave A&E. Within a couple of days, A&E caved and lifted the suspension.

\[caption id="attachment\_860" align="alignleft" width="250"\][![Jase Robertson, second son of the Robertsons and head of duck call manufacturing.](images/jaserobertson.png)](http://realromancatholic.files.wordpress.com/2014/01/jaserobertson.png) Jase Robertson, second son of the Robertson's and head of duck call manufacturing.\[/caption\]

Now that you are up to date, I'd like to tell you about my experience with Duck Dynasty. We don't have a TV at home, so the only time we get to see TV, outside of DVDs or videos on the web, is at Grandma's. Christmas Day we came across a Duck Dynasty marathon and proceeded to watch a couple of hours. And it was great. Each episode centered around the Robertson's and their adventures. Quite often they start with several of the main characters (usually Jase and Si Robertson and a couple of employees) sitting in the duck call assembly room talking. Quite often the conversation leads to a challenge, which kicks off the entertainment.

For example, in one episode Si says that you can eat hot donuts faster than regular donuts.

\[caption id="attachment\_862" align="alignright" width="262"\][![Si Robertson, Phil's younger brother](images/sirobertson.png)](http://realromancatholic.files.wordpress.com/2014/01/sirobertson.png) Si Robertson, Phil's younger brother\[/caption\]

He then challenged the other three in the work room to hot donut eating contest at the new donut shop down the street. When they saw a camping trailer and an advertisement for a drawing for the trailer outside of the donut shop, Jase said, "If you park a camper outside a donut shop, rednecks will come running from rocks, caves, and mountaintops to eat your donuts." Inside, after betting $20 Si proceeded to out eat the other three. Si then used the $80 in prize money to buy tickets for the camping trailer raffle. Later, as they are unloaded supplies, the camper is delivered for Si. Si then proceeded to turn the camper into his "office". After cooking a squirrel concoction, Si took a nap. Jase decided to play a trick on his uncle by hooking up the trailer and driving the sleeping Si into the woods and driving away. Just like every other episode it ended with Phil saying grace over a large family dinner.

\[caption id="attachment\_863" align="alignleft" width="372"\][![Willie Robertson, third son and CEO of Duck commander](images/willierobertson.png)](http://realromancatholic.files.wordpress.com/2014/01/willierobertson.png) Willie Robertson, third son and CEO of Duck commander\[/caption\]

In my opinion, the reason people enjoy this show is because it is very funny, but also because it is very relatable. As one article stated, the A&E top dogs intended Duck Dynasty to be the usual laugh at the dumb redneck, but instead viewer laughed with them, not at them. It is a series about a goofy uncle with crazy stories, brothers who get on each others nerves, about getting back to the simpler things. While the Christian overtones are not always obvious, they are there. As I said before, every episode ends with Phil saying grace over a large family meal. In an episode, one of Phil's grandsons stops by Phil's house to go on a fishing trip with his girlfriend. Upon hearing this Phil decided to join them as a chaperon. While going down the river, Phil tells his grandson that he should not touch his girlfriend below the neck until "he signs the dotted line".

If you get a chance, watch Duck Dynasty. Skip the rest of the shows on A&E because they are junk, just like every other channel. Keep the Robertson family in your prayers because the minority that has attacked them in the past has a long memory and may do so again.
