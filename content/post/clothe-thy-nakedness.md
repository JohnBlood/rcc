---
title: "Clothe Thy Nakedness"
date: "2010-08-27"
categories: 
  - "roman-catholic"
  - "sin-of-omission"
tags: 
  - "children"
  - "church"
  - "croatia"
  - "hedonism"
  - "hermit"
  - "music"
  - "news"
  - "parents"
  - "priest"
  - "pulpit"
  - "temptation"
  - "woman"
  - "world"
---

It happens every year.  Once the temperature gets 65 degrees, out come the shorts and flip-flops.  The problem is that every years the summer clothes keep covering less and less.  This is scandalous, especially for a guy trying to keep his thoughts pure.  Look, I’m not some old geezer sitting here and complaining just for the fun of it.  I’m a 23 year old guy trying to stay pure and holy.  It’s enough to make a guy want to become a hermit during the summer months.

It’s bad enough to go outside and see this kind of hedonism out in the world, but I should not be subjected to this kind of temptation when I go to church.  Sadly, that is not the case.  It is not uncommon for women and girls to come into church on Sunday with shorts and plunging necklines.  They look like they are going to the beach, not church.  This should not be wearing so little, this is God’s house.

The blame for this problem falls primarily upon two groups: parents and pastors.  You can say that TV, movies and music are the main reason for this promiscuity among young people, but the fact of the matter is that parents have allow their children to watch or listen to these forms of media.  In some homes, the mothers have encourage this sort of dress by wearing it too.  Ultimately, the parents are the most responsible for the souls of their children.  They must correct their children and protect them from the hedonism of the world.

Pastors also have a responsibility to guard the souls of those in their parish.  They should tell women from the pulpit to clothe themselves.  Besides being disrespectful of God’s house, this lewdness of dress is also a grave temptation for young and old men alike.  Unfortunately, many of the priests that I know of have not spoken from the pulpit about this problem.  They must.

However, there are a few who have done their duty by speaking out.  Most recently, a priest in Croatia was in the news for kicking 2,000 indecently clad pilgrims out of his church annually.  Too bad there are not more like this.

We the laypeople must get together and give our priests and parents the support they need to do their jobs.  This means verbal encouragement and prayer.
