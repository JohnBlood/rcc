---
title: "Women Shall Be Quiet in Church"
date: "2010-01-05"
categories: 
  - "apostles"
  - "orthodox"
  - "roman-catholic"
  - "saints"
tags: 
  - "communion"
  - "high-priest"
  - "mass"
  - "st-paul"
---

The other day I went to Mass and at Communion time three lay distributors went up to the altar.  All three were women!!!  That is not acceptable.[![NoCommunionInTheHand](images/nocommunioninthehand_thumb1.png "NoCommunionInTheHand")](http://realromancatholic.files.wordpress.com/2010/01/nocommunioninthehand1.png)

Most of you would not give it another thought, but to me the thought of a woman on the altar is very distasteful.  The problem is that women have no place leading worship.  Since the beginning of time, the men have offered sacrifices and performed other forms of worship.  In the Old Testament, there were High Priests, but no High Priestesses or any priestesses for that matter.  Priestesses have long been a fixture of pagan religions, but they have no place in Jewish or Christian ceremonies.

This preference for male priests started at the Last Supper.  According to _How Christ Said the First Mass_, only 12 men were present at the Last Supper and Christ consecrated them as priests.  This was a sign that all future priests would be men.

St. Paul himself stated it best.

> “Let the woman learn in silence with all subjection.  But I suffer not a woman to teach, nor to use authority over the man: but to be in silence.  For Adam was first formed; then Eve.  And Adam was not seduced; but the woman, being seduced, was in the transgression.  Yet she shall be saved through child bearing; if she continue in faith and love and sanctification with sobriety.”  (1 Timothy 2:11-15)

The push for women on the altar comes from several fronts.  First, there are those who want priests to marry.  There are also those who want homosexuals to be consecrated priests.  Both of these groups are seeking to destroy the Church, not update the Church as many of them claim.

Once again, women do not belong on the altar.  That is not because they are bad people.  The priests of the Church have always been men and must always be men.  God does not change and neither should His Church.
