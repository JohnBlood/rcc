---
title: "Upon This Rock I Will Build My Church"
date: "2011-02-28"
categories: 
  - "apostles"
  - "bible"
  - "bishops"
  - "church-fathers"
  - "orthodox"
  - "pope-benedict-xvi"
  - "roman-catholic"
  - "saints"
  - "tradition"
tags: 
  - "catholic"
  - "christ"
  - "church"
  - "high-priest"
  - "jesus"
  - "pope"
  - "saint"
  - "st-peter"
---

> [![](http://realromancatholic.files.wordpress.com/2011/02/popebenedictxvi.png?w=250 "Pope Benedict XVI")](http://realromancatholic.files.wordpress.com/2011/02/popebenedictxvi.png)"And Jesus came into the quarters of Cesarea Philippi: and He asked His disciples, saying: Whom do men say that the Son of man is? But they said: Some John the Baptist, and other some Elias, and others Jeremias, or one of the prophets. Jesus saith to them: But whom do you say that I am? Simon Peter answered and said: Thou art Christ, the Son of the living God. And Jesus answering, said to him: Blessed art thou, Simon Bar-Jona: because flesh and blood hath not revealed it to thee, but My Father who is in heaven. And I say to thee: That thou art Peter; and upon this rock I will build My church, and the gates of hell shall not prevail against it. And I will give to thee the keys of the kingdom of heaven. And whatsoever thou shalt bind upon earth, it shall be bound also in heaven: and whatsoever thou shalt loose upon earth, it shall be loosed also in heaven." - St. Matthew 16: 13-19 (DRB)

Wednesday, the Roman Catholic Church celebrated the great feast of the Chair of St. Peter.  This is truly a great feast because it commemorates the fact that the Catholic Church is the only Church that can claim an unbroken connection to the Apostles through the office of the Pope. There is no other church in the world whose leader can claim and prove descent from Peter.

Why is this important? People need an authority figure to lead them or anarchy results. From early history groups of people always has leaders to guide the way. It is part of the way God designed us. Look at the family, the basic hierarchical unit.

Let's take the case of the Jewish people. God gave the Jewish people the order of High Priests to lead them. He also sent the prophets, but the High Priests were a constant figure in the Jewish religion, until the destruction of the Temple. By this time, the Christian religion had replaced the Jewish.

Once the Christian church was established, who was seen as its leader and head? St. Peter. This is obvious once you take a look at Acts of the Apostles chapter 15. In this chapter, a council of bishops was called in Jerusalem to discuss how Gentiles should be allowed into the Church. St. Peter makes the final decision. Jesus Himself gave Peter this power in the above quote when He gave him the name Peter and made him the rock that the rest of the Church is built upon. In fact, Jesus said, "That thou art Peter; and upon this rock I will build My church, and the gates of hell shall not prevail against it." St. Matthew 16:18 (DRB) Because St. Peter was the bishop of Rome, this power was passed on to all succeeding Bishops of Rome.

All Catholics should thank God for the Papacy, in general, and Pope Benedict, in particular. He and his predecessors have led the Church for over 2,000 years and always will until Christ returns with great power and majesty. In the mean time, we must pray that he will continue to have the courage and strength to lead the Catholic Church and the world from the Chair of St. Peter.
