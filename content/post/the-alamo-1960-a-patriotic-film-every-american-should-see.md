---
title: "The Alamo (1960), A Patriotic Film Every American Should See"
date: "2013-07-09"
categories: 
  - "movie-review"
tags: 
  - "john-wayne"
  - "movie-review"
  - "patriot"
---

Like most of you, I enjoy a good movie. However, now days it is harder and harder to find a decent, especially one you can show to your entire family. Your best option is to pick a film from years gone by. Today, I would like to talk about one of my favorite films: John Wayne's The Alamo.

\[youtube=http://www.youtube.com/watch?v=VgBwE8zAJ3Y\]

John Wayne had wanted to make a movie about the Alamo for many years. In 1955, while he was at Republic Pictures, John Wayne tried to get the green-light to direct an Alamo film. He even had a script written. Unfortunately, the wanted him to star, not direct. Ultimately, Wayne left and formed his own production company. He was forced to leave the script behind at Republic. The script was later rewritten and released as _Last Command_.

Meanwhile, John Wayne was working on his version. He had a replica of the Alamo built on his property. He mortgaged his boat and other items to pay for the film. He gathered big name actors to fill out the cast, including Richard Widmark (Jim Bowie), Laurence Harvey (Col. William Travis), Frankie Avalon (Smitty), Ken Curtis (Captain Almeron Dickinson), Richard Boone (General Sam Houston) and Chill Wills (Beekeeper). John Wayne played Col. Davy Crockett.

The resulting film was a two hours long show of heroism, courage, patriotism. In one of my favorite scenes in the beginning of the film, Davy Crockett (John Wayne) tell Flaca, a young Mexican girl, the importance of standing up for what's right. This single speech encapsulates what this movie is about.

\[youtube=http://www.youtube.com/watch?v=0PtB-Y8-nsM\]

There is a Catholic tie to this film that most do not know about if they have not seen the make of documentary. Before filming started, a Franciscan friar blessed the set. During filming, no one was seriously hurt (except for a canon running over Laurence Harvey's foot). On another occasion, while Wayne was directing a scene, he heard some women talking behind him. Wayne whipped around and told them to "shut $#$@ up". The next thing he said was, "I'm sorry about that, sisters." Several nuns were visiting from a nearby convent. A sheepish Wayne gave them a tour of the set. Interestingly, Wayne's grandson, Fr. Matthew Muñoz, is a Catholic priest and Wayne himself converted in the 1970s.

[![Alamo 1960 vs 2004](images/alamo19602004.png)](http://realromancatholic.files.wordpress.com/2013/06/alamo19602004.png)

**1960 vs 2004**

Now, I would compare how this 1960 film compares with the more recent (and inferior) 2004 version. And remember there is no bias in this review whatsoever.

While the 2004 version focused more on the events that led to the siege of the Alamo, the 1960 version focused on the defenders and what they went through.

One of the big differences between the two films is how the two films treated the slaves inside the Alamo. In the 2004 version, the slaves are concerned with getting out of the Alamo alive, regardless of what happened to their masters. In the 1960 version, the sole slave is an old man owned by Jim Bowie. The night before the final battle, Jim Bowie gives his slave a piece of paper give him his freedom. The old man looked at the paper and said, "Mr. Bowie, it seems that freedom is what you people are fighting for. So, I guess I'll stay here."

Secondly, the 1960 version had more of a spiritual tone to it (which is not at all surprising.) The night before the final battle, the defenders muse about what the afterlife will be like. One says, "There is no hereafter." After which the old slave wonders how could anyone not believe in an afterlife. In the beginning of the film, when they find the Alamo, one of Crockett's men (Parson) offers a prayer of thanksgiving. One of the men remarks that Parson has a prayer for everything.

All in all, the Alamo (1960) is a great film and you should go and watch it.
