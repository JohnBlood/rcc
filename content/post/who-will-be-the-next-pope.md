---
title: "Who Will be the Next Pope?"
date: "2013-03-07"
categories: 
  - "cardinal-burke"
  - "latin-mass"
  - "roman-catholic"
tags: 
  - "cardinal-burke"
  - "catholic"
  - "latin-mass"
  - "pope"
---

Now that Pope Benedict has abdicated and the conclave has been set to start March 11th, everyone is going to be trying to guess who will be the next pope. In fact, Business Insider has an interesting list, with pros and cons for each candidate that it could only have been written by a Catholic. You can find it [here](http://www.businessinsider.com/meet-the-man-who-will-be-the-next-pope-2012-4).

Here are my top picks for the next pope. These candidates are based on orthodoxy, not popularity. There might be other candidates just as orthodox, but these are the four that I have read the most about or know the most about.

\[caption id="attachment\_603" align="alignright" width="240"\][![Cardinal Raymond Leo Burke](images/cardinalburke.png)](http://realromancatholic.files.wordpress.com/2013/03/cardinalburke.png) Cardinal Raymond Leo Burke\[/caption\]

**Cardinal Raymond Leo Burke**

Burke is the foremost defender of Catholic orthodoxy and tradition in the Church. While he was Bishop of La Crosse, Wisconsin, Archbishop of St. Louis and finally as a cardinal in the Vatican, Burke has always been a strong supporter of the Latin Mass. Even though he is the head of the Apostolic Signatura, the Church's supreme court, he still travels around the world saying Pontifical High Masses and performing ordinations for traditional priests that are united with Rome.

Burke is a strong defender of the Holy Eucharist. Whenever there was an election, he would publicly state that he would refuse to give Holy Communion to any pro-abortion Catholic politician. Recently, Ireland's politicians have decided that it was time that that Catholic country should allow abortion. Cardinal Burke told the Irish priests that they should refuse Holy Communion to the Catholic politicians who support those laws. He has also stated that bad Masses lead to a loss of Faith.

Cardinal Burke has said repeatedly that the disobedience of the clergy in terms of canon law has led to many of the problems in the Church. He has stated repeatedly that Catholics must be will to witness for the Faith, even to the point of martyrdom. He sees that that may start very soon in America. When he was asked about the renegade nuns in the US last year, Burke said that is they could not be reformed, they should be disbanded.

There also a parallel between the relationship of Pope Benedict and Cardinal Burke and that of Pope John Paul II and Cardinal Ratzinger. While Blessed John Paul II was pope, he acted as the kind fatherly figure, while Ratzinger was the hammer that went after heresy and dissent. When Ratzinger became pope, he needed someone who could be his hammer. He found Cardinal Burke in America. At the time, Burke was ostracized by other American prelates because of his conservatism. Pope Benedict took him to Rome and made him the second most powerful man in the Church's Canon Law court (second most powerful after the Holy Father, that is.). He is also very popular among the Italians. I'm sure that most of the American prelates where glad to see him go, but they did not realize that his reach was growing. Hopefully, he will shortly step into the Shoes of the Fisherman.

For more excellent reasons why Cardinal Burke should be our next pope, read Dr. Taylor Marshall's excellent post on the topic [here](http://www.taylormarshall.com/2013/03/pope-prediction-10-reasons-cardinal.html).

\[caption id="attachment\_606" align="alignright" width="200"\][![Cardinal Ouellet](images/cardinalouellet.png)](http://realromancatholic.files.wordpress.com/2013/03/cardinalouellet.png) Cardinal Marc Ouellet\[/caption\]

**Cardinal Marc Ouellet**

Cardinal Ouellet was the primate of Canada for many years before he was picked by Pope Benedict to head the Congregation for Bishops. Essentially, he helps the pope pick who will be made a bishop. He is another cardinal who has found popularity outside his own country. When he left, many where glad to see him go because of his strong opposition to abortion, no matter the conditions. One article I read said that he was made Archbishop of Quebec because the archdiocese needed to be reformed. One of thing he did while in Quebec was to reopen the minor seminary located there. He is a support of Gregorian Chant. He has also spent 11 years in Colombia, so he is popular in South America.

**Cardinal Malcolm Ranjith**

\[caption id="attachment\_607" align="alignright" width="220"\][![Cardinal Malcolm Ranjith](images/cardinalranjith.png)](http://realromancatholic.files.wordpress.com/2013/03/cardinalranjith.png) Cardinal Malcolm Ranjith\[/caption\]

Many people are hoping for a pope from Asia or Africa. If that is the case, I hope Cardinal Ranjith is the one, since he is from Colombo, Sri Lanka. Cardinal Ranjith is another strong supporter of the Latin Mass. He used to be a member of the Congregation for Divine Worship, so he knows first hand of the liturgical abuses that are plaguing the Church. In his diocese, Cardinal Ranjith has replaced the altar railing in all churches. Holy Communion is only received on the tongue and on the knees in his diocese. He also forbade the laity from preaching in church and bar the clergy from pollution the Mass by bringing in elements from other religions.

 

 

\[caption id="attachment\_608" align="alignright" width="225"\][![Cardinal George Pell](images/cardinalpell.png)](http://realromancatholic.files.wordpress.com/2013/03/cardinalpell.png) Cardinal George Pell\[/caption\]

**Cardinal George Pell**

Cardinal Pell is the primate of Australia. A friend of mine in Australia told me that his diocese was the one in Australia that offered the Latin Mass. He also worked hard to help the victims of the abuse crisis. He is trying to restore orthodoxy in Australia, but the bishops under him will not listen. He is fighting an uphill battle. Interestingly, Cardinal Pell debated the infamous atheist Richard Dawkins on TV. That takes courage and that is a virtue we need in a new pope.

**In Closing**

If it looks like I'm tilting the scales in favor of Cardinal Burke, there are two reasons for that. First, I _really, really, really_ want him to become pope. Second, I've read the most and know the most about him.

In the end, the decision of who will become the next Holy Father rests in the hands of God. Still we need to pray hard that God sends us a holy pope in the model of Pope Benedict XVI. God Bless Your Church and keep it safe.
