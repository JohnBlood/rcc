---
title: "Were the Consecrations of the SSPX Bishops Criminal?"
date: "2014-07-15"
categories: 
  - "pope-pius-xii"
  - "sspx"
---

\[caption id="attachment\_960" align="alignright" width="312"\][![Pope Pius XII](images/pope-pius-xii-holy-card.png)](https://realromancatholic.files.wordpress.com/2014/07/pope-pius-xii-holy-card.png) Pope Pius XII\[/caption\]

There seems to be a consensus among supporters of the Society of St. Pius X that the excommunication of those involved in the 1988 Écône consecrations was the act of a Modernist church against those stands for tradition. However, Pope Pius XII might have disagreed.

In June 1958, Pope Pius XII issued the encyclical _Ad Apostolorum Principis_. This encyclical addressed the persecution of the Catholic Church in China. He also mentioned the Chinese Patriotic Catholic Association, which was created by the Communists to control Catholics. One of the methods they used for control was to install their own bishops, often forcing lawfully ordained bishops to consecrate Communist-friendly men without papal approval. Many good bishops went along with them because they believed it would benefit the laity in the long run. Here is what Pope Pius XII had to say about consecrations performed without papal approval: **(Emphasis is mine.)**

> 30\. Assuming false and unjust premises, they are not afraid to take a position which would confine within a narrow scope the supreme teaching authority of the Church, claiming that there are certain questions -- such as those which concern social and economic matters -- in which Catholics may ignore the teachings and the directives of this Apostolic See.
> 
> 31\. This opinion -- it seems entirely unnecessary to demonstrate its existence -- is utterly false and full of error because, as We declared a few years ago to a special meeting of Our Venerable Brethren in the episcopacy:
> 
> 32\. "The power of the Church is in no sense limited to so-called 'strictly religious matters'; but the whole matter of the natural law, its institution, interpretation and application, in so far as the moral aspect is concerned, are within its power.
> 
> 33\. "By God's appointment the observance of the natural law concerns the way by which man must strive toward his supernatural end. The Church shows the way and is the guide and guardian of men with respect to their supernatural end."\[9\]
> 
> 34\. This truth had already been wisely explained by Our Predecessor St. Pius X in his Encyclical Letter Singulari quadam of September 24, 1912, in which he made this statement: "All actions of a Christian man so far as they are morally either good or bad -- that is, so far as they agree with or are contrary to the natural and divine law -- fall under the judgment and jurisdiction of the Church."\[10\]
> 
> 35\. Moreover, even when those who arbitrarily set and defend these narrow limits profess a desire to obey the Roman Pontiff with regard to truths to be believed, and to observe what they call ecclesiastical directives, they proceed with such boldness that they refuse to obey the precise and definite prescriptions of the Holy See. They protest that these refer to political affairs because of a hidden meaning by the author, as if these prescriptions took their origin from some secret conspiracy against their own nation.
> 
> 36\. Here We must mention a symptom of this falling away from the Church. It is a very serious matter and fills Our heart -- the heart of a Father and universal Pastor of the faithful -- with a grief that defies description. For those who profess themselves most interested in the welfare of their country have for some considerable time been striving to disseminate among the people the position, **devoid of all truth, that Catholics have the power of directly electing their bishops**. To excuse this kind of election they allege a need to look after the good souls with all possible speed and to entrust the administration of dioceses to those pastors who, because they do not oppose the communist desires and political methods, are acceptable by the civil power.
> 
> 37\. We have heard that many such elections have been held contrary to all right and law and that, in addition, certain ecclesiastics have rashly dared to receive episcopal consecration, despite the public and severe warning which this Apostolic See gave those involved.
> 
> Since, therefore, such serious offenses against the discipline and unity of the Church are being committed, We must in conscience warn all that this is completely at variance with the teachings and principles on which rests the right order of the society divinely instituted by Jesus Christ our Lord.
> 
> 38\. For it has been clearly and expressly laid down in the canons that it pertains to t**he one Apostolic See to judge whether a person is fit for the dignity and burden of the episcopacy,\[11\] and that complete freedom in the nomination of bishops is the right of the Roman Pontiff**.\[12\] But if, as happens at times, **some persons or groups are permitted to participate** in the selection of an episcopal candidate, **this is lawful only if the Apostolic See has allowed it** in express terms and in each particular case for clearly defined persons or groups, the conditions and circumstances being very plainly determined.
> 
> 39\. Granted this exception, it follows that **bishops who have been neither named nor confirmed by the Apostolic See, but who, on the contrary, have been elected and consecrated in defiance of its express orders, enjoy no powers of teaching or of jurisdiction since jurisdiction passes to bishops only through the Roman Pontiff** as We admonished in the Encyclical Letter Mystici Corporis in the following words: ". . . As far as his own diocese is concerned each (bishop) feeds the flock entrusted to him as a true shepherd and rules it in the name of Christ. Yet in exercising this office they are not altogether independent but are subordinate to the lawful authority of the Roman Pontiff, although enjoying ordinary power of jurisdiction which they receive directly from the same Supreme Pontiff."\[13\]
> 
> 40\. And when We later addressed to you the letter Ad Sinarum gentem, We again referred to this teaching in these words: "**The power of jurisdiction which is conferred directly by divine right on the Supreme Pontiff comes to bishops by that same right, but only through the successor of Peter**, to whom not only the faithful but also all bishops are bound to be constantly subject and to adhere both by the reverence of obedience and by the bond of unity."\[14\]
> 
> 41\. **Acts requiring the power of Holy Orders which are performed by ecclesiastics of this kind, though they are valid as long as the consecration conferred on them was valid, are yet gravely illicit, that is, criminal and sacrilegious.**
> 
> 42\. To such conduct the warning words of the Divine Teacher fittingly apply: "He who enters not by the door into the sheepfold, but climbs up another way, is a thief and a robber."\[15\] The sheep indeed know the true shepherd's voice. "But a stranger they will not follow, but will flee from him, because they do not know the voice of strangers."\[16\]
> 
> 43. **We are aware that those who thus belittle obedience in order to justify themselves with regard to those functions which they have unrighteously assumed, defend their position by recalling a usage which prevailed in ages past.** Yet everyone sees that all ecclesiastical discipline is overthrown if it is in any way lawful for one to restore arrangements which are no longer valid because the supreme authority of the Church long ago decreed otherwise. In no sense do they excuse their way of acting by appealing to another custom, and they indisputably prove that they follow this line deliberately in order to escape from the discipline which now prevails and which they ought to be obeying.
> 
> 44\. We mean that discipline which has been established not only for China and the regions recently enlightened by the light of the Gospel, but for the whole Church, a discipline which takes its sanction from that universal and supreme power of caring for, ruling, and governing which our Lord granted to the successors in the office of St. Peter the Apostle.
> 
> 45\. Well known are the terms of Vatican Council's solemn definition: "Relying on the open testimony of the Scriptures and abiding by the wise and clear decrees both of our predecessors, the Roman Pontiffs, and the general Councils, We renew the definition of the Ecumenical Council of Florence, by virtue of which all the faithful must believe that 'the Holy Apostolic See and the Roman Pontiff hold primacy over the whole world, and the Roman Pontiff himself is the Successor of the blessed Peter and continues to be the true Vicar of Christ and head of the whole Church, the father and teacher of all Christians, and to him is the blessed Peter our Lord Jesus Christ committed the full power of caring for, ruling and governing the Universal Church....'
> 
> 46\. "We teach, . . . We declare that the Roman Church by the Providence of God holds the primacy of ordinary power over all others, and that this power of jurisdiction of the Roman Pontiff, which is truly episcopal, is immediate. Toward it, the pastors and the faithful of whatever rite and dignity, both individually and collectively, are bound by the duty of hierarchical subordination and true obedience, not only in matters which pertain to faith and morals, but also in those which concern the discipline and government of the Church spread throughout the whole world, in such a way that once the unity of communion and the profession of the same Faith has been preserved with the Roman Pontiff, there is one flock of the Church of Christ under one supreme shepherd. This is the teaching of the Catholic truth from which no one can depart without loss of faith and salvation."\[17\]
> 
> 47\. From what We have said, it follows that **no authority whatsoever, save that which is proper to the Supreme Pastor, can render void the canonical appointment granted to any bishop; that no person or group, whether of priests or of laymen, can claim the right of nominating bishops; that no one can lawfully confer episcopal consecration unless he has received the mandate of the Apostolic See**.\[18\]
> 
> 48\. Consequently, **if consecration of this kind is being done contrary to all right and law, and by this crime the unity of the Church is being seriously attacked, an excommunication reserved specialissimo modo to the Apostolic See has been established which is automatically incurred by the consecrator and by anyone who has received consecration irresponsibly conferred**.\[19\]

You can read the whole encyclical with footnotes [here](http://www.papalencyclicals.net/Pius12/P12APOST.HTM).

It would seem clear that Pope Pius XII would not approve consecration of bishops without papal approval because it shows a lack of respect for the office of the Pope and it damages the unity of the Church.

As always, please pray for the reunion of SSPX with the Church. I firmly believe that when they return they will be a great force for good.
