---
title: "Repentance: the Forgotten Virtue"
date: "2014-04-03"
categories: 
  - "lent"
tags: 
  - "repentance"
---

[![ProdigalSon](http://realromancatholic.files.wordpress.com/2014/04/prodigalson.jpeg?w=300)](http://realromancatholic.files.wordpress.com/2014/04/prodigalson.jpeg)During the season of Lent, we are called to purge our souls of sin. The Church provides us with many prayers to help us accomplish this goal, including the Stations of the Cross. However, most of the modern versions of these prayers are devoid of any mention of repentance. This is very problematic for me because as St. John Vianney said:

> "Without it (repentance), it is impossible, absolutely impossible, to obtain forgiveness."

In the newer Stations of the Cross, there are frequent requests of the Lord to cleanse our hearts or help me to be grateful for what you did. However, there is no commitment from the person praying to do better or even sorrow for those sins which cause Jesus such great pain.

Let me give you an example of what I mean. First is a quote from the First Station of a [modern Stations of the Cross](http://www.catholic.org/prayers/station.php).

> Jesus, you stand all alone before Pilate. Nobody speaks up for you. Nobody helps defend you. You devoted your entire life to helping others, listening to the smallest ones, caring for those who were ignored by others. They don't seem to remember that as they prepare to put you to death. As a child, sometimes I feel alone. Sometimes I feel that others don't stand up for me and defend me when I am afraid. Sometimes I don't feel like I am treated fairly, especially if I am scolded or corrected. As an adult, sometimes I feel abandoned and afraid as well. Sometimes I too, feel like I am treated unfairly or blamed for things unfairly. I have a hard time when people criticize me at home or at work. Help me be grateful for what you did for me. Help me to accept criticism and unfairness as you did, and not complain. Help me pray for those who have hurt me.

Now, here is the First Station as written by [St. Francis of Assisi](http://www.ourladyswarriors.org/prayer/stations.htm) (one of my favorites).

> Jesus, most innocent, who neither did nor could commit a sin, was condemned to death, and moreover, to the most ignominious death of the cross. To remain a friend of Caesar, Pilate delivered Him into the hands of His enemies. A fearful crime -- to condemn Innocence to death, and to offend God in order not to displease men! O innocent Jesus, having sinned, I am guilty of eternal death, but Thou willingly dost accept the unjust sentence of death, that I might live. For whom, then, shall I henceforth live, if not for Thee, my Lord? Should I desire to please men, I could not be Thy servant. Let me, therefore, rather displease men and all the world, than not please Thee, O Jesus.

I hope you will agree that the second version (written by St. Francis) has a better chance of awakening feelings of remorse and repentance in the sinner's soul.

Now, some people have complained to me that the Stations of the Cross written by St. Francis are too dark. I would reply in two ways. First, St. Francis is a saint, so this kind of meditation must be beneficial. Second, in a world that is so filled with wars, murder and evil; acknowledging our sinfulness and trying to please God is dark?

The real problem is not that the Franciscan Stations are dark, it is the fact that they remind people that they are sinful human beings who deserve only death but who are saved from this death by Christ's death on the Cross. Modern man does not like to be reminded that he is sinful and must change his ways. He would rather continue on as he is and possibly repent at the end of his life. But that may be too late.  St. Augustine of Hippo said:

> "I know, and as I do every one knows, who has used a little more than ordinary consideration, that no man who has any fear of God omits to reform himself in obedience to His words, but he who thinks that he has longer time to live. This it is which kills so many, while they are saying, "Tomorrow, Tomorrow;" and suddenly the door is shut. He remains outside with the raven’s croak, because he had not the moaning of the dove. "Tomorrow, Tomorrow;" is the raven’s croak. Moan plaintively as the dove, and beat your breast; but while you are inflicting blows on your breast, be the better for the beating; lest you seem not to beat your conscience, but rather with blows to harden it, and make an evil conscience more unyielding instead of better. Moan with no fruitless moaning."

The saints knew well how beneficial repentance is. Here are some of their quotes for our mutual edification, as well as several quotes from the Bible.

> “Repentance raises the fallen, mourning knocks at the gate of Heaven, and holy humility opens it.” - St. John Climacus - "The Ladder of Divine Ascent" (Step 25) "I wealthiest am when richest in remorse." - St. Robert Southwell "And after that John was delivered up, Jesus came into Galilee, preaching the gospel of the kingdom of God, and saying: The time is accomplished, and the kingdom of God is at hand: repent, and believe the gospel." - St. Mark 1:14-15 "The Lord delayeth not His promise, as some imagine, but dealeth patiently for your sake, not willing that any should perish, but that all should return to penance." - 2 St. Peter 3:9 "Where sin was hatched, let tears now wash the nest." - St. Robert Southwell "What man of you that hath an hundred sheep: and if he shall lose one of them, doth he not leave the ninety-nine in the desert, and go after that which was lost, until he find it? And when he hath found it, lay it upon his shoulders, rejoicing: And coming home, call together his friends and neighbours, saying to them: Rejoice with me, because I have found my sheep that was lost? I say to you, that even so there shall be joy in heaven upon one sinner that doth penance, more than upon ninety-nine just who need not penance." - St. Luke 15: 4-7 “Repentance is the second grace and is begotten in the heart by faith and fear. Fear is the paternal rod which guides our way until we reach the spiritual paradise of good things. When we have attained thereto, it leaves us and turns back.” - St. Isaac the Syrian - "Ascetical Homilies” "It is true that God promises forgiveness if we repent, but what assurance have we of obtaining it tomorrow?" - St. Louis de Blois "No one is as good and merciful as the Lord. But even He does not forgive the unrepentant." - St. Mark the Ascetic

Let us take to heart these wise words of those who have gone before us in faith. Let us awaken in ourselves sufficient remorse and repentance for our sins that we may never offend God and may one day be worthy of eternal life.
