---
title: "Only One Way to Receive Holy Communion – Part 3"
date: "2011-01-24"
categories: 
  - "apostles"
  - "church-fathers"
  - "holy-communion"
  - "judas"
  - "mass"
  - "orthodox"
  - "priest"
  - "sin-of-omission"
tags: 
  - "communion"
  - "eucharist"
  - "roman-catholic"
  - "tradition"
---

In this final part of my series discussing Danielle Bean’s article “Why I’m Giving Up Communion On the Tongue”, I will critique the arguments she gives for switching to Communion in the hand and refute them.[![JesusCommunionChildren](images/jesuscommunionchildren_thumb.png "JesusCommunionChildren")](http://realromancatholic.files.wordpress.com/2011/01/jesuscommunionchildren.png)

> I have always received the Eucharist on the tongue…This is not something I usually get all political or righteous about. I understand that many devout people hold different opinions on this topic and that “good” Catholics are free to receive on the hand or on the tongue. For me, though, receiving on the tongue has always felt like the most appropriate way to recognize and respect Christ’s real presence in the Eucharist.
> 
> But now I’m not so sure anymore.

Receiving Communion on the tongue is something that anyone should get “political” and “righteous” about because you are being faithful to over 2,000 years of teaching concerning how to receive Communion.  People may have many opinions, but there is only one truth.  Follow that Truth, do not be swayed by the opinions of others.  Those opinions will lead you from the Truth.

> While I still very much prefer to receive on the tongue, I am afraid that option is becoming a less reasonable choice for me. And, ironically, it’s becoming a less respectful way to receive the Eucharist.

How is touching the Consecrated Host with unconsecrated hand being more respectful towards Our Lord in the Blessed Sacrament?  Saints and Fathers of the Church repeatedly said that to receive Communion in the hand is sinful, while they advocated receiving Communion on the tongue.

![PopeBenedictCommunionNun](images/popebenedictcommunionnun_thumb1.png "PopeBenedictCommunionNun")

Do you feel bad because you are sticking your tongue out at the priest?  One time when I went to a retreat, the priest said that receiving Communion on the tongue was the only time you could legitimately stick out your tongue at a priest.

> At the Masses I attend, more often than not, I receive Communion from an Extraordinary Minister of Holy Communion. These EMHCs, more often than not, have no idea what to do when a person presents herself to receive Communion on the tongue. I am not blaming them for this problem so much as whatever training they have received…I know I could “solve” this problem by only attending Mass in the Extraordinary Form where receiving on the tongue while kneeling is the norm. But that is just not a realistic option for me.

Okay, I have a big problem here.  Extraordinary Ministers of Holy Communion are just that extraordinary.  They are not needed.  The ordinary minister of Holy Communion is the priest.  If Bean was a real traditionalist as she pretends to be she would be receiving Holy Communion from the priest, not a layperson.  According to the revelations of demons during the exorcism in _Warnings for Beyond_, “The worst thing is when women are appointed to distribute Communion. Then there are scarcely any blessings or graces; these are not consecrated hands, these are women's hands. I want to clarify that - saying that these are women's hands would, in it self, have no special significance, but the hands are not consecrated. Christ chose men, and only men, for the Priesthood - not women.  But this is pride - pride, the original sin of the Angels”[![Consecration](images/consecration_thumb.png "Consecration")](http://realromancatholic.files.wordpress.com/2011/01/consecration.png)

Here is a parallel story from the Bible.  In the Bible, the Levite priests were allowed to carry or even touch the Ark of the Covenant: **“No one may carry the ark of God except the Levites, for the Lord chose them to carry the ark of the Lord and to minister to him forever”** (1 Chronicles 15:2; 1 Paralipomenon 15:2 in DRB). **But when a non-Levite touched the Ark of the Covenant, he was struck dead:** “And when they came to the floor of Chidon, Oza put forth his hand, to hold up the ark: for the ox being wanton had made it lean a little on one side. And the Lord was angry with Oza, and struck him, because he had touched the ark; and he died there before the Lord” (1 Chronicles 13:9-10; 1 Paralipomenon 13:9-10 in DRB).  After reading this story, I would never want to touch a Consecrated Host.

> I could be stubborn and insist upon receiving on the tongue because _I have a right to_, even when the challenges it causes become a distraction to myself and others. But that doesn’t seem like something Christ would want me to do.

Once again, this in not something optional.  The tradition of the Church and Church Fathers say that you are doing the right thing, why bow to the wishes of the modernists, the liberals, the progressives?  Your duty is to fight these moves to do away with the traditions of the Church.  You do have a right to receive Communion on the tongue, the Church says so.  Once again, noting good can come of bending to the currents of the day, when the Church has always preached the opposite.  If we give up our traditions here, where will it end?  Besides you are not distracting the people, you are setting an example for them.  At one of the churches that we attend, more and more people are receiving Communion on the tongue because they see my family doing it and follow our example.  There was a gentleman who told may dad that seeing us receive Communion on the tongue gave him the courage to do the same.  The example you give is very powerful.[![JesusFirstHolyCommunion](images/jesusfirstholycommunion_thumb.png "JesusFirstHolyCommunion")](http://realromancatholic.files.wordpress.com/2011/01/jesusfirstholycommunion.png)

In closing, here is a final quote from the demons.  They are responding to a question about whether priests should give Communion in the hand when asked.

> “In no circumstances! Absolutely not! Do you believe that the priest is the puppet of his people? He has the right of command! Broadly speaking, we have to add this: if the priests were to give Communion in the mouth, as Those up there wish (he points upward), they would probably meet with opposition at the beginning, because we (demons) put oil on the fire, but in the long run, they would have many more faithful in their churches than in the ones where Communion is given differently, where there is this lukewarmness.”
