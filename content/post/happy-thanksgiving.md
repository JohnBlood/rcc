---
title: "Happy Thanksgiving"
date: "2013-11-29"
categories: 
  - "history"
tags: 
  - "george-washington"
  - "thanksgiving"
  - "thanksgiving-day"
---

[![GeorgeWashingtonPray](images/georgewashingtonpray.jpg)](http://realromancatholic.files.wordpress.com/2013/11/georgewashingtonpray.jpg)I would like to wish all of my readers a Happy Thanksgiving. Instead of writing an article today, I will post George Washington's Thanksgiving Day proclamation which made it a national day of prayer, not a great day for shopping deals.

> "Whereas it is the duty of all nations to acknowledge the providence of Almighty God, to obey His will, to be grateful for His benefits, and humbly to implore His protection and favor; and Whereas both Houses of Congress have, by their joint committee, requested me "to recommend to the people of the United States a day of public thanksgiving and prayer, to be observed by acknowledging with grateful hearts the many and signal favors of Almighty God, especially by affording them an opportunity peaceably to establish a form of government for their safety and happiness:
> 
> "Now, therefore, I do recommend and assign Thursday, the 26th day of November next, to be devoted by the people of these States to the service of that great and glorious Being who is the beneficent author of all the good that was, that is, or that will be; that we may then all unite in rendering unto Him our sincere and humble thanks for His kind care and protection of the people of this country previous to their becoming a nation; for the signal and manifold mercies and the favorable interpositions of His providence in the course and conclusion of the late war; for the great degree of tranquility, union, and plenty which we have since enjoyed; for the peaceable and rational manner in which we have been enable to establish constitutions of government for our safety and happiness, and particularly the national one now lately instituted' for the civil and religious liberty with which we are blessed, and the means we have of acquiring and diffusing useful knowledge; and, in general, for all the great and various favors which He has been pleased to confer upon us." - George Washington, Thanksgiving Proclamation, October 3, 1789
