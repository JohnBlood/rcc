---
title: "Duty of the Priest by Michael Voris"
date: "2011-03-31"
categories: 
  - "audio"
  - "bishops"
  - "duty"
  - "holy-communion"
  - "mass"
  - "priest"
  - "priest"
  - "roman-catholic"
  - "tradition"
tags: 
  - "catholic"
  - "communion"
  - "duty"
  - "high-priest"
  - "jesus"
  - "pastors"
---

\[caption id="attachment\_251" align="alignright" width="300" caption="Michael Voris of RealCatholicTV fame"\][![](http://realromancatholic.files.wordpress.com/2011/03/michaelvoris.png?w=300 "Michael Voris")](http://realromancatholic.files.wordpress.com/2011/03/michaelvoris.png)\[/caption\]

Last Saturday, the Knights of Columbus of St. Isidore Catholic Church hosted their annual vocations dinner to raise money to help support local seminarians. The dinner was accompanied by two talks. The first was a vocation story by the local vocations director. The second talk was given by Michael Voris of RealCatholicTV fame. He spoke on the duty of priests. I was able to record his talk. You will find it below. It gives you a new appreciation for the priesthood. Makes me want to sign up for a black cassock.

[The Duty of the Priest by Michael Voris](http://www.archive.org/download/TheDutyOfThePriestByMichaelVoris/MichaelVorisDutyofPriest.mp3)
