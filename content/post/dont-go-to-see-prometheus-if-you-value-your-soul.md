---
title: "Don't Go To See Prometheus If You Value Your Soul"
date: "2012-07-02"
categories: 
  - "movie-review"
  - "occult"
  - "roman-catholic"
  - "satanism"
tags: 
  - "catholic"
  - "movie-review"
  - "occult"
---

[![](images/prometheus.png "Prometheus film movie poster")](http://realromancatholic.files.wordpress.com/2012/07/prometheus.png)A couple of weeks ago Ridley Scott release a prequel to his popular _Alien_ series. The film is entitled _Prometheus_ and is a dangerous film because it is an attack on Christianity.

As the story of the film unfolds, two archaeologists discover that an identical star map can be found in the artifacts of several ancient civilization. They realize that this map is a message from a group of aliens called the "Engineers". They convince an industrialist to finance a trip to visit the planet indicated in the star map. They make the trip in a ship called the _Prometheus_. Once they reach the plant, they encounter the "Engineers" and other aliens. Mayhem ensues.

There are two major problems with this film. The first is how it portrays Christianity. One of the main characters, Shaw, is a Christian. (You can tell that because she wears a small cross.) However, even though she believes in God, she somehow also believes that the "Engineers" are aliens who placed humans on Earth. This idea (aliens placing humans on Earth) is called panspermia. This totally disregards the Creation narrative in Genesis. It replaces a Divinely inspired story of our creation by God with a story that is science fiction at best. For someone whose faith is not on solid ground, this version of Christianity found in _Prometheus_ could be spiritually damaging.

The second problem has to do with the name of the movie and the spaceship. In Greek mythology, Prometheus was a minor god who stole the secret of fire and gave it to mortals.  If that is not bad enough, satanists believe that Prometheus is Lucifer because Lucifer means "light bearer" and he brings "light" to mankind.

Is Lucifer trying to destroy Christianity by revealing the previously hidden "truth" that humans were created by other beings, not an All-Powerful Supreme Being? (Incidentally, the movie tells use that the "Engineers" share some DNA with humans.) I don't like coincidences.

_Prometheus_ ends with the "Christian" boarding an alien spaceship and heading for the "Engineers" home world. There are rumored to be two more films in the series. Will she find a planet of gods? The following films will undoubtedly continue to try to change or modify your faith. Stay far away from this film. Watch _For Greater Glory_ instead. At least that film has more substance.
