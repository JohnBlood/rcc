---
title: "The New Lone Ranger Is a Great Example of Hollywood's Ongoing Destrution of American Culture"
date: "2013-07-21"
categories: 
  - "movie-review"
  - "old-time-radio"
  - "tv"
  - "video"
tags: 
  - "green-hornet"
  - "lone-ranger"
  - "movie-review"
---

\[caption id="attachment\_777" align="alignright" width="290"\][![It looks more like Crazy Birdman and his side kick, the Masked Man.](images/loneranger2013.png)](http://realromancatholic.files.wordpress.com/2013/07/loneranger2013.png) It looks more like Crazy Birdman and his side kick, the Masked Man.\[/caption\]

Wow! That has got to be one of the longest post titles I have ever written.

Anyway, unless you live in a cave or if you are free from the constant bombardment of video, radio and print ads, you know that Hollywood released yet another remake of a classic American story: The Lone Ranger. I have not seen this film yet and I don't plan to, but I have read enough reviews and comments to give you me opinion.

Before I begin, let me tell you the story of the Lone Ranger as it was originally intended. Some of you probably know how the story goes because you listened to the radio show or watched Clayton Moore's TV version, but many probably only know the crap they seen in theaters. The Lone Ranger was a member of the Texas Rangers named John Reid. One day, his squad, led by his brother, is chasing down the bandit leader Butch Cavendish. They are led into an ambush by a traitor and gunned down. Reid alone survives and is found by an Indian friend named Tonto, who nurses the wounded man back to health. Reid decides that he must bring Cavendish and his gang to justice, but in order to do so John Reid must die. So, they add another grave to those that Tonto had already dug to bury the other rangers. Reid fashions as mask to protect his identity. He is now the Lone Ranger. Once Cavendish and his men were captured and brought to justice, Tonto asked the Lone Ranger would remove the mask since his job was done. The Lone Ranger replied that there was still much work to do and many wrongs to right. The Lone Ranger than has adventure after adventure, first on the radio from 1933 to 1954 and then on TV from 1949 to 1957.

The Lone Ranger had several trademark that made him stand out from other westerns. He used silver bullets instead of lead as a kind of calling card and to remind himself that life, like siver, is valuable. When he was forced to shoot, he would shoot to wound, not kill. And of course, he had the William Tell Overture for his theme song.

Now, that I've taken us down memory lane for a bit, let's return to the new Lone Ranger. From what I've read, John Reid in this film is a law student returning to Colby from the East. On the train, he encounters some Protestants, who sing their hymns badly and off-key. When the minister offers him a Bible, Reid holds up a law-book and tell the preacher that it is his bible. Shortly after he arrives, Butch Cavendish breaks out of jail. John is deputized by his brother Dan and they take a posse and ride after Cavendish. They are ambushed and Cavendish cuts out Dan's heart and eats it. Tonto had been in jail, as well, and escaped, too. He come across the bodies and proceeds to bury them. He also witnesses a "spirit" horse resurrect John Reid as a "spirit walker" who cannot die. They then go off and seek revenge on Cavendish and the man behind him. There is also a brothel scene. And Tonto (Johnny Depp) has a dead crow on his head that he feeds from time to time. Also, the only prayer in the film is said by the bad guy. (What are they trying to say about Christians?)

First off, any similarities between the original Lone Ranger and this new creation are purely coincidental. The names may be similar, but the characters are not. Clayton Moor and Jay Silverheels, who played the Lone Ranger and Tonto in the 40s and 50s, knew they were role models for the youth and took that job seriously. In fact, Fran Striker, writer and co-creator of the Lone Ranger, gave the Ranger a creed which goes as follows:

> I believe...
> 
> - That to have a friend, a man must be one.
> 
> - That all men are created equal and that everyone has within himself the power to make this a better world.
> 
> - That God put the firewood there, but that every man must gather and light it himself.
> 
> - In being prepared physically, mentally, and morally to fight when necessary for what is right.
> 
> - That a man should make the most of what equipment he has.
> 
> - That 'this government of the people, by the people, and for the people' shall live always.
> 
> - That men should live by the rule of what is best for the greatest number.
> 
> - That sooner or later...somewhere...somehow...we must settle with the world and make payment for what we have taken.
> 
> - That all things change but truth, and that truth alone, lives on forever.
> 
> - In my Creator, my country, my fellow man

Striker and George W. Trendle, the other co-creator, also wrote these guidelines for the Lone Ranger.

> - The Lone Ranger is never seen without his mask or a disguise.
> 
> - With emphasis on logic, The Lone Ranger is never captured or held for any length of time by lawmen, avoiding his being unmasked.
> 
> - The Lone Ranger always uses perfect grammar and precise speech completely devoid of slang and colloquial phrases, at all times.
> 
> - When he has to use guns, The Lone Ranger never shoots to kill, but rather only to disarm his opponent as painlessly as possible.
> 
> - Logically, too, The Lone Ranger never wins against hopeless odds; _i.e._, he is never seen escaping from a barrage of bullets merely by riding into the horizon.
> 
> - Even though The Lone Ranger offers his aid to individuals or small groups, the ultimate objective of his story never fails to imply that their benefit is only a by-product of a greater achievement—the development of the west or our country. His adversaries are usually groups whose power is such that large areas are at stake.
> 
> - Adversaries are never other than American to avoid criticism from minority groups. There were exceptions to this rule. He sometimes battled foreign agents, though their nation of origin was generally not named.
> 
> - Names of unsympathetic characters are carefully chosen, never consisting of two names if it can be avoided, to avoid even further vicarious association—more often than not, a single nickname is selected.
> 
> - The Lone Ranger never drinks or smokes and saloon scenes are usually interpreted as cafes, with waiters and food instead of bartenders and liquor.
> 
> - Criminals are never shown in enviable positions of wealth or power, and they never appear as successful or glamorous.

As you can tell from my description of the new Long Ranger, he violated quite a few of these guideline, which were intended to create a good role model. And that is the biggest problem with today's films. There are no more role models. Think about it. Would you want the "heroes" of most modern American films to be role models for your children? I hope not.  You want good example for your children, so they grow up to be God-fearing. Bible believing adults. Which interpretation of the Lone Ranger would most likely give you those results? It surely would not be the 2013 version.

This is just one example of Hollywood's continued effort to destroy America's Judeo-Christian culture by promoting characters (both real and imaginary) who are very destructive to the soul. They are using these degenerates that they call actors and writers to undermine values that have been held by Americans, and Christians, for many generations. If we want to turn the tide and stop this degeneration from spreading( at the very least in our families) we must steer clear of films, TV shows and other forms of entertainment that spiritually damaging. By writing this and similar posts, I hope to prevent people from exposing themselves to such soul rot as Hollywood now produces and provide you with a safer alternative.

Now for your enjoyment, here is the original Lone Ranger film from the 1940s.

\[youtube=http://www.youtube.com/watch?v=PlOBqhlRtOI\]

For continued enjoyment you can listen to the Lone Ranger radio show [here](http://archive.org/details/The_Lone_Ranger_Page_01), [here](http://archive.org/details/The_Lone_Ranger_Page_02) and [here](http://archive.org/details/The_Lone_Ranger_Page_03).

**Green Hornet**

\[caption id="attachment\_776" align="alignright" width="300"\][![Kato, remember where we parked.](images/greenhornet2011.png)](http://realromancatholic.files.wordpress.com/2013/07/greenhornet2011.png) Kato, remember where we parked.\[/caption\]

While I'm talking about the new Lone Ranger film, I might as well talk about another film that came out rather recently that is related to the Lone Ranger character: the Green Hornet.

For those of you who do not know, the Green Hornet was in real life a newspaper owner named Britt Reid who ran a company by day and fought crime at night as the masked Green Hornet. The Green Hornet was aided in his fight for justice by his Japanese valet, Kato. Most people probably don't know this, but Britt Reid is the Lone Ranger's grandnephew. The Green Hornet has his own radio show from 1936 to 1948 and again in 1952. He also had a series of film serials in the 1940s and finally a TV show that ran from 1966 to 1967 starring Van Williams (Britt Reid/Green Hornet) and Bruce Lee (Kato). (Interestingly, Bruce Lee only agreed to take the role if he would talk normal English instead of pidgin English.)

Now the 2011 remake. Whereas, the original series portrayed the Green Hornet as a courageous man who wanted to bring gangsters and racketeers to justice, the 2011 Green Hornet (Seth Rogen) is a party boy who dons the mask after his father is killed. Rogen's version leaves most of the thinking and fighting to his companion and makes a general klutz of himself. While I like the looks of the Black Beauty (the Green Hornet's car), there is nothing else to like in it. It is as violent as most modern action films with partying klutz for a hero who fights a bed guys who is just as laughable.. What else is there to say?

Now, here, for your enjoyment, is a Green Hornet TV episode from the 1960s.

\[youtube=http://www.youtube.com/watch?v=TBgZ\_KCwEOg\]

You can find the 1939 Green Hornet film serial on YouTube [here](http://www.youtube.com/playlist?list=PLAF1848FBDFA6FBA3). Also,you can enjoy the great Green Hornet radio show [here](http://archive.org/details/TheGreenHornet).

If you found this article interesting or remember the original Lone Ranger and Green Hornet, leave a comment. If you watched either film, leave a comment with your opinion.
