---
title: "Priestly Fraternity of St. Peter Celebrates 25 Years of Fidelity to Rome"
date: "2013-11-25"
categories: 
  - "fssp"
---

[![FSSPCoatOfArms](images/fsspcoatofarms.png)](http://realromancatholic.files.wordpress.com/2013/11/fsspcoatofarms.png)This year the Priestly Fraternity of St. Peter (FSSP) celebrates their 25th anniversary. In July 18, 1988, twelve priests and a number of seminarians decided that they did not want to follow the rest of the Society of St. Pius X into schism when Archbishop Marcel Lefebvre ordained four bishops without Papal mandate. As of 2011, the FSSP 228 priests, 10 deacons, and 154 seminarians in 117 dioceses around the world.

Pope Francis sent the following message to the FSSP, through the nuncio to France, regarding their anniversary and many years of service to the Church.

> Pope Francis joins the thanksgiving of her members for the work accomplished in this quarter-century spent at the service of ecclesial communion cum Petro et sub Petro.
> 
> It was in a moment of great trial for the Church that the Priestly Fraternity of Saint Peter was created. In a great spirit of obedience and hope, her founders turned with confidence to the Successor of Peter in order to offer the faithful attached to the Missal of 1962 the possibility of living their faith in the full communion of the Church. The Holy Father encourages them to pursue their mission of reconciliation between all the faithful, whichever may be their sensibility, and this to work so that all welcome one another in the profession of the same faith and the bond of an intense fraternal charity.
> 
> By way of the celebration of the sacred Mysteries according to the extraordinary form of the Roman rite and the orientations of the Constitution on the Liturgy Sacrosanctum Concilium, as well as by passing on the apostolic faith as it is presented in the Catechism of the Catholic Church, may they contribute, in fidelity to the living Tradition of the Church, to a better comprehension and implementation of the Second Vatican Council.
> 
> The Holy Father exhorts them, according to their own charism, to take an active part in the mission of the Church in the world of today, through the testimony of a holy life, a firm faith and an inventive and generous charity.
> 
> Entrusting to the intercession of the Blessed Virgin Mary and of Saint Peter, apostle, all the pilgrims assembled in Lourdes or at the church of Saint-Sulpice in Paris to give thanks to the Lord on this occasion, the Holy Father grants them with open heart the Apostolic Benediction.
> 
> Paris, October 28, 2013 On the feast of Saints Simon and Jude, Apostles.

Please keep all the members of the Priestly Fraternity of St. Peter. Pray that they will have many more years to spend in service to the Church.
