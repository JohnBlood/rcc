---
title: "Pope Benedict and Nancy Pelosi - The Wave of a Hand Brings Joy"
date: "2009-08-02"
categories: 
  - "joke"
  - "roman-catholic"
tags: 
  - "nancy-pelosi"
  - "pope"
---

Pope Benedict and Nancy Pelosi are on the same stage in front of a  huge crowd. The Madame Speaker and The Pope, however, have seen it all before.[![Pope Benedict XVI smiling](images/pope_benedict_xvi_smiling_thumb.jpg "Pope Benedict XVI smiling")](http://realromancatholic.files.wordpress.com/2009/08/pope_benedict_xvi_smiling.jpg)

To make it a little more interesting, Madame Speaker says to the Pope, "Did You know that with just one little wave of my hand I can make every Democrat in the crowd go wild?"

He doubts it, so she shows him. Sure enough, the wave elicits rapture and cheering from every Democrat in the crowd. Gradually, the cheering subsides.

The Pope, not wanting to be outdone by such a level of arrogance, considers what he could do...

"That was impressive", the Pope says, "But did you know that with just one little wave of MY hand I can make many people in the crowd, and many around the world, go crazy with joy?  This joy will not be a momentary display like that of your subjects, but will go deep into their hearts, and they will forever speak of this day and rejoice."

The speaker seriously doubts this, and says so. "One little wave of your hand and so many people will rejoice forever? Show me."

So Pope Benedict slapped her.
