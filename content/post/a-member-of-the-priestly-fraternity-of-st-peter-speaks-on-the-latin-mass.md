---
title: "A Member of the Priestly Fraternity of St. Peter Speaks on the Latin Mass"
date: "2013-04-15"
categories: 
  - "fssp"
  - "latin-mass"
  - "orthodox"
  - "roman-catholic"
  - "tradition"
  - "video"
tags: 
  - "catholic"
  - "latin-mass"
---

I thought that I would follow-up my previous post about the problems in the SSPX by presenting you with several videos on the Latin Mass by a priest who is in communion with Rome. Fr. Calvin Goodwin is a member the Priestly Fraternity of St. Peter. The Priestly Fraternity of St. Peter was founded in July of 1988 by members of SSPX who did not want to follow the rest of the Society in to the schism the resulted from Lefebvre's ordination of four bishops.

Enjoy these two videos and please share.

\[youtube=http://www.youtube.com/watch?v=96i2d428us4\]

\[youtube=http://www.youtube.com/watch?v=mQO3YomJG7I\]
