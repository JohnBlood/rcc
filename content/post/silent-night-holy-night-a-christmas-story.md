---
title: "Silent Night, Holy Night - a Christmas Story"
date: "2010-12-25"
categories: 
  - "christmas"
  - "santa-claus"
  - "story"
tags: 
  - "catholic"
  - "santa"
---

_I wrote this story about 5 to 6 years ago and thought I’d share it with my readers.  Enjoy._ 

**By John Paul Wohlscheid**

**Prologue**

    “What good is Christmas???  Why must we have it?  It seems like an unbearable holiday.  The main reason for having Christmas seems to be so that you must spend much of your savings on presents for your friends and relatives.  The value of the presents I received never even remotely amounts to the amount I spend on presents.  What good is it?

    “Christmas is the time of year when we must invite over the relatives we dislike the most so that they can gorge themselves on the expensive meal prepared by a person’s family.  Christmas is such a waste of both time and money.”  So read the diary entry of Adrian Norton for December 23.

**Chapter 1 – The Ride**

    Adrian Norton was like any other boy his age, so full of questions.  The question that this new teenager wanted answered was “What good is Christmas?”  Everyone answered that it was the season of giving, the season of joy.  But joy for what and for whom?

    It was this question that Adrian posed to his dad at the dinner table o the eve of December 22.

    His father sat thinking for several minutes before answering.  However, before he could answer, Adrian’s little brother, Sherman piped up, “Christmas is to celebrate the birth of Jesus, Stupid.”

    “Oh, thanks a lot, Ugly,” said Adrian angrily.  He wanted to hear his father’s answer, not his know-it-all baby brother’s answer.  His little rat-fink of a brother stuck his tongue out.

    “All right, you two,” commanded their father.  Then he turned his attention back to Adrian.  “Son,” he began, “the reason we celebrate Christmas is to remember the birth of our Savior.  We celebrate the fact that He came down to earth to free us from sin.”

    “Where does Santa Claus come in,” demanded Adrian.  He was not one to get sentimental about anything.  He thought of himself as a tough guy who had a dry eye when everyone else cried during a sad movie

    “Santa Claus brings gifts to all good boys and girls to celebrate Christmas,” replied Adrian’s mother.  “You see, even Santa Claus celebrates Christmas.  He prepares all year for it.”

    Adrian gave his mother a smile that said he did not believe a word she had said.  He knew full well that his parents, not Santa Claus had given him most of his presents.

        That night as Adrian lay on the bottom bed of the bunk beds he and his brother shared, his mind turned to the dinner conversation.  To tell the truth he was not entirely sure that there wasn’t a Santa Claus.  Sure, some the presents supposedly from Santa Claus had his parents handwriting on the tag, but a few always had a strange handwriting.  His father mixed both printing and cursive when he wrote and his mother used a rather square letters, but some of the tags were written with long, bold cursive.  Was it possible that there was a Santa Claus?

    Adrian lay for several minutes trying to determine the best method to see if there was a Santa Claus.  Then, the answer came to him in a flash.  “I’ll hide in the living room tomorrow night when everyone is asleep.”  He smiled, if there was a Santa he would be the one to tell Adrian about Christmas.

     The next day passed with a whiz as Adrian went to school, did his homework, and wrapped the few presents he had dutifully bought.  The dinner conversation concerned the preparations for the Christmas Day meal.  Adrian’s parents seemed to think that their explanations to Adrian had been sufficient.  All during the rest of the evening, Adrian was in a state of anxious excitement.  His parent’s attributed it to the fact that is was Christmas Eve.

     That night Adrian lay in his bed asleep, an alarm clock under his pillow.  Promptly at 11:30, the alarm started to ring.  Immediately Adrian’s hand went under the pillow to silence it.  He knew that even if the alarm rang from now until doomsday his brother would never hear it.

    Adrian lay there quietly for several minutes.  He wanted to make sure no one else was up.  The only sound that he could hear was the soft breathing from the bunk overhead and loud snoring coming from his parents’ room.  Confident that no one else was awake; Adrian climbed out of bed and put on his clothes.  He did not think it would be proper for Santa Claus to see him in his pajamas.

    Within minutes Adrian was dressed and standing in the living room.  He inspected the tags of the presents that surrounded the bright Christmas tree that sat in the middle of the living room.  There were several presents from Santa Claus with his parents’ handwriting, but none with the strange handwriting.

    The family’s fireplace sat in the west wall of the living room, facing the decorated spruce.  Adrian took up position in a corner between his father’s La-Z-Boy and the family couch.  Here he sat waiting for Santa to appear.

    For a while it seemed as though nothing would happen.  Adrian was beginning to wonder if Santa was coming.  The silence was deafening.

    Suddenly, there was a noise.  It started out softly and far away.  Then, then it came closer and swelled in volume.  Adrian strained his ears and searched his memory to see if he could recognize the sound.  He could; it was the sound of sleigh bells.

    Minutes later the bells reached their peak volume and then stopped.  Adrian was overcome with the fear that Santa had passed his house by.  Then, there was a noise at the chimney and before Adrian could blink there stood a jolly, fat man with white hair and a red suit.  Santa Claus!

**Chapter 2 – The Adventure**

    In front of the Norton’s fireplace there stood Santa Claus with a large, brown, canvas sack at his feet, overflowing with brightly wrapped boxes.  Santa stood stretching for several minutes and then hurriedly, as if to make up for lost time, took packages out of his bag and added them to the pile already under the tree.  While he was working, Adrian had made his way, as quiet as a mouse, until he was standing next to Santa.

    When Santa Claus paused to rest and wipe his brow, Adrian tapped on his shoulder.  Santa whirled around with a start, but not as pronounced as Adrian had expected.  He realized he probably was not the first child to catch Santa in the act.

    Santa was everything Adrian had expected, and more.  Santa was dressed in a red suit trimmed with white fir.  A red cap of the same material and design was seated upon his snowy white head.  A black belt encircled his waist, black boots covered his feet, and red mittens kept his hands warm.  A bright smile was framed by his white beard.

    “Hi there,” said Santa softly.  “Is there anything I can do for you?”

    Adrian was too stunned to answer right away.  Finally, he blurted out, “I want you to tell me the meaning of Christmas.”

   Santa put his hand on his chin and thought for a moment.  Then he said, “I have an idea.  What if I took you to my North Pole workshop?  You see I need to drop off presents to quite a few more children before they wake up.”

    Now it was Adrian’s turn to think it over.  “Will I be home before by family misses me?” he asked.

    “They won’t even know you were gone,” Santa assured him with a grandfatherly smile and a twinkle in his eye.

    “Okay,” said Adrian, “let’s go.”

      The time passed quickly for Adrian as he flew through the night with Santa.  The sleigh was large, painted red, and was pulled through the night sky by a team of eight reindeer, the leader having a red nose.  Before they had taken off, Santa had wrapped Adrian in a sealskin coat to keep him from freezing.  There was a large, brown canvas sack in the back of the sleigh that yielded what seemed like an unlimited amount of toys.

    Finally after Santa had made his deliveries to different parts of the world, he turned the sleigh north.  In less time than it takes to tell, the sleigh had reached the North Pole.  Adrian was puzzled as he looked over the barren landscape.  There was absolutely no place for the sleigh and reindeer to land.  He had seen paintings of what Santa’s workshop was supposed to look like.  There was nothing on the North Pole except mountains of snow, ice and rock.  The only thing that seemed out of the ordinary was a large pole that extended from the ground.

    Adrian turned to Santa and raised his open palms as if to ask, “Where?”  Santa chuckled and pointed ahead.  Adrian took a look and noticed that the sleigh was angling towards the ice below.  He looked back to Santa with astonishment.  Santa just smiled and pointed again.  Adrian took another look and saw something he hadn’t seen the first time.  A large chunk of ice was sliding back and lights were blinking around the edges.

    Down they went, sleigh, reindeer, and all into the hole.  Adrian closed his eyes and tensed as they neared, fearing a crash.  There was a whoosh as they passed through the hole in the ice.  Adrian reopened his eyes and was amazed to be flying through a tunnel of colorful lights.  As they neared the end of the tunnel, the sleigh had slowed down quite a bit and was gliding along the ice floor.

    As they came out of the end of the tunnel, the reindeer stopped as they had many times before.  Adrian looked around and found himself in a large brightly lit room with many colorful lights.  Santa spread his arms wide and said, “Welcome to my underground workshop.”

**Chapter 3 – Explanations**

    As Santa helped Adrian out of the sleigh a large crowd of children surrounded them.  Santa introduced the children to Adrian as his “helpers”.  He also told the children that Adrian was just visiting.

    As Santa led the way towards a door framed by Christmas lights, Adrian turned to see a group of adults join the children as they unhooked the reindeer.  Santa turned and followed Adrian’s line of sight.  Then he chuckled.

    When Adrian turned back to Santa with questions in his eyes, Santa explained, “Those are all people who have come to help me in my work.”

    “I thought that you had little elves?” questioned Adrian.

    “Not really,” replied Santa Claus.  “You see, most of the people came here as children or young people.  Some were picked for this job, while others visited and had no reason to return.  They have families here and the children help whenever they can.”  Santa turned a corner and stepped into an open elevator.  As the doors closed, Santa pressed the button marked “HQ”.  Up shot the elevator.

    When the elevator opened, Adrian found himself staring with wonderment.  The room he stepped into was full of computer screens and people manning them.

    “This is the command center,” said Santa as he walked up to the computer that sat in the middle of the room.  It was staffed by a short young man with black curly hair, who turned in his swivel chair as Santa approached.

    “Hi, there, Big Red,” he greeted.  “I see that this year you were only five minutes behind last year’s record.”  Then he seemed to notice Adrian.  “Hi, kid,” he said.

    Adrian wrinkled his nose at this.  He was thirteen now, a teenager, not a kid.

    “Ben, this is Adrian Norton,” Santa introduced.

    “Norton, Adrian,” repeated Ben as he turned back to his computer screen.  His fingers flew over the keyboard as typed in Adrian’s name.  Seconds later, his entire history appeared on the screen.

    “Let’s see,” said Ben as he started to read.  “Age: 13 ½ years old, grade: 8th.  He has a history of fighting with his brother, which is not uncommon for someone his age.  Good grades…”

    “Where did you get all that information about me?” Adrian interrupted.

    “From your guardian angel,” replied Ben without looking up from the screen.  Adrian looked at Santa’s face to see if he was joking.  Santa’s face was a solemn as a judge’s.

    “It’s true,” he replied.  “Each person has a guardian angel who gives us regular updates.  For every deed points are given or taken away, depending on what sort of deed it is.  The total by Christmas determines whether that person is ‘naughty’ or ‘nice’.”

    Adrian opened his mouth to say something, but before he could one of the other computer operators cried, “I’ve got an overhead radar scan coming this way.”

    “Get the radio pole in,” Ben shouted back as he focused on a window on his screen that showed the sweeps of a ground search radar.  Several of the technicians started flipping switches immediately.  There was a loud groan and a creaking sound.

    “What’s that?” cried Adrian in alarm.

    “Oh, that’s just our radio antenna that the guardian angels from all over the world use to report in,” replied Santa.  “Isn’t that right, Ben?”  But Ben was too busy reading Adrian’s file.

    “Ah, Santa, you might want to read this,” said Ben quietly as he pointed to the screen.  Adrian leaned over and started to read as well.  It was his diary entry from the night before.  He immediately felt embarrassed.

    Santa read for several minutes, then said, “Adrian, let me show you something.”  Santa quietly led the way out of the control room and down another hallway.  Stopping in front of a door marked “Private”, Santa turned the knob and stepped in.

    The room that Adrian followed him into was Santa’s private office.  On one side of the room there was a fireplace and several chairs. On the right, there was an oaken business desk.  But, in the middle of the room there stood a Christmas tree with a large Nativity scene underneath it.  It was in front of this Christmas scene that Santa knelt and removed his cap.  Adrian quickly followed suit.

    Santa looked lovingly at the scene and said softly, “See that Child in the manger?”  Adrian nodded.  “Well, that Child is the reason for the season.  It was that Son of God, who was born; became Man; and died for our sins.  He died so that you and I maybe free.  It was quite something for God to humble Himself to become a man.  But he loved us so much, that He was willing to do anything to save us.”  They were silent for several minutes.  Then, Adrian looked at Santa’s face.  To his great surprise, he saw a tear roll down Santa’s cheek as he gazed at the Nativity scene.

    Then Santa bowed his head and rose to his feet.  He turned to Adrian and smiled through his tears.  “Would you like some cocoa?”

    Adrian sat across from Santa in front of Santa’s fireplace with a steaming cup of hot cocoa in his hand.  But he was more interested in some explanations than in the steaming liquid.  Santa knew that look he had seen it many times before.

    He smiled as he took a sip of his cocoa and set it on the small table next to him.  “Adrian, you’re probably wondering about me.  Well, I’m Santa number 1,259.  You see thousands of years ago, Saint Nicholas was the first Santa Claus.  But when God took him to Heaven, He picked another man to be Santa.  God always picked men who were generous givers and he blessed them with good, jolly temperaments.  I’ve been Santa for about fifty years,” said Santa as he looked down at his hands for several minutes.

    He continued, “Our job as Santa is to bring joy to all people, no matter what race, religion, or color.  It is the season during which we can help those less fortunate than ourselves.  The reason that your folks invite people that you don’t like is so that they can share in your joy at Our Lord’s birth.  Do you have any questions?”

    Now it was Adrian’s turn to study his hands.  He never realized before that Christmas meant so much to so many people.  He was saddened when he remembered what he had written in his diary.

    “I think I understand everything now, Santa,” he replied slowly.  A smile warmed Santa’s features.

    “I hope you’ll forgive me for what I might have thought or said.  I hope He forgives me,” Adrian said as he turned to look at the Nativity scene.

    “I’m sure He already has,” said Santa softly.  There was silence, and then the clock on the mantelpiece started to chime, softly.  Santa looked up and said, “We’d better get you home.

**Chapter 4 – The Reminder**

    The flight back to Adrian’s house was faster than the trip from it.  Santa made sure that Adrian was safely in bed before he left.  It seemed that as soon as he had closed his eyes, Adrian’s brother was jumping on him.

    “Get up, Adrian.  It’s Christmas morning.”  Adrian unwillingly sat up and put on his robe.  When he entered the living room he found it the same as he had left it.  His brother was tearing open his presents as fast as he could.  Adrian walked over to the tree wondering if everything he remembered had just been a dream.

    Adrian’s mother picked up a large package wrapped in red and white paper.  The tag said “From: Santa, To: Adrian.”  She showed it to her husband, the both shrugged their shoulders.  Neither remembered wrapping this present.

    “Here’s one for you, Adrian,” smiled his mother as she handed the package to him.  “Merry Christmas.”  Adrian managed a weak smile in return.

[![SantaCrib](images/santacrib_thumb.png "SantaCrib")](http://realromancatholic.files.wordpress.com/2010/12/santacrib.png)     He took the package and unwrapped it.  He opened the box and looked inside.  Then, tears started to form at the corners of his eyes.  His parents came over in alarm as Adrian set out the contents of the box.  It was a good sized Nativity scene with four men kneeling before the Child Jesus.  Two were shepherds, one was a prince, and the fourth was a Santa Claus with his head bowed and uncovered.

    Adrian’s parents were definitely sure that this was not from either one of them.  But where had it come from?

    To Adrian it meant that he had not dreamt his talk with Santa.

**Merry Christmas.**
