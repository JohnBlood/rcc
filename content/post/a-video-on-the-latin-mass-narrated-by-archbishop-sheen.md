---
title: "A Video on the Latin Mass Narrated by Archbishop Sheen"
date: "2012-10-23"
categories: 
  - "archbishop-fulton-j-sheen"
  - "latin-mass"
  - "mass"
  - "roman-catholic"
  - "saints"
  - "tv"
  - "video"
tags: 
  - "archbishop-sheen"
  - "latin-mass"
---

In this film for 1941, then-Msgr. Fulton Sheen narrates the celebrated of a solemn High Mass at Easter. Very beautiful.

\[youtube=http://www.youtube.com/watch?v=R6AOvStZS64\]
