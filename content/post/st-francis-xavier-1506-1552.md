---
title: "St. Francis Xavier (1506 - 1552)"
date: "2012-12-04"
categories: 
  - "missionary"
  - "priest"
  - "roman-catholic"
  - "saints"
tags: 
  - "catholic"
  - "missionary"
  - "priest"
  - "religion"
  - "saint"
---

Today is the feast of St. Francis Xavier, so

\[caption id="attachment\_564" align="alignright" width="324"\][![Saint Francis Xavier](images/saintfrancisxavier.png)](http://realromancatholic.com/2012/12/03/st-francis-xavier-1506-1552/saintfrancisxavier/) Saint Francis Xavier\[/caption\]

St. Francis Xavier was born on April 7, 1506 in the Kingdom of Navarre in Northern Spain. He was born into a wealthy aristocratic family. His father died when he was nine, following an invasion of the kingdom by Castile and Aragon. As a result of the family’s activities to regain independence, the Xavier family’s lands were confiscated, their castle’s defenses were demolished and the moat was filled in.

In 1525, Francis went to Paris to study. Here, he met St. Ignatius of Loyola and they became close friends. In 1534, Francis, Loyola and five others founded the Society of Jesus, also known as the Jesuits. They were all ordained in June of 1537. Following his ordination, Francis became interested in missionary work. When he left for the missionary field, Francis only took with him a breviary, a catechism and a book on Biblical inspiration.

At the request of King John III of Portugal, Francis’ first stop was the Portuguese portion of India in 1541. He refused to take a servant with him, saying that as long as he had the use of his hands and feet he would wait on himself. When someone remarked that it was unbecoming for someone of a noble house to care for himself, Francis replied that he could cause no scandal if he did no evil. Here, he worked to convert the natives. For a while he lived in a sea cave as he worked with the natives. He built nearly 40 churches along the eastern coast of India. Francis initially met with limited success because he focused on converting the lower classes. He also worked to reconvert Portuguese who has fallen away from the faith. Francis’ next missionary stop was Indonesia in 1545. Francis had more success here. His efforts laid the groundwork for a permanent mission. He is known as the Apostle of the Indies because of these successes.

In December of 1547, Francis met a Japanese man named Anjiro. Anjiro had heard of Francis and wanted to meet him. Francis learned the Japanese customs and culture from him. Francis had not been aware of the existence of Japan and was thrilled to hear that there were new countries to proselytize. These two and three other Jesuits returned to Japan in 1549. One of Francis’ first actions was to gain an audience with the Emperor. At first, he had quite a bit of trouble learning the language. Instead, he used paintings that he has brought with him of the Madonna and Child as visual teaching aids. He translated some important books on the Faith into Japanese. Because poverty was not as well received in Japan as in other countries, Francis dressed in fine clothes and had several of his companions act as attendants. This made it easier to preach to the Japanese nobility. After two and a half years of missionary work in Japan, Francis returned to India.

His next goal was spread the Gospel in China. He sailed for China on the Santa Cruz. However, before he could land, he died from fever and exhaustion on December 3, 1552 with a single companion by his side. He had survived three shipwrecks during his work in the missionary field and multiple attacks from hostile Muslims. He converted ten of thousands to Catholicism. He is said to have converted more people than anyone since St. Paul. Francis laid the groundwork and created the methods that Jesuit missionaries would use for years to come. He was canonized on March 12, 1622 by Pope Gregory XV.
