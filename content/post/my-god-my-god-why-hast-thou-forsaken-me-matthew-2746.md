---
title: "Fourth Word - My God, My God, why hast thou forsaken me? (Matthew 27:46)"
date: "2014-03-28"
categories: 
  - "last-seven-words-of-jesus"
  - "lent"
---

\[caption id="attachment\_901" align="alignright" width="500"\][!["My God, My God, why hast thou forsaken me?" (Matthew 27:46)](images/mygodmygodwhyhastthouforsakenme.png)](http://realromancatholic.files.wordpress.com/2014/03/mygodmygodwhyhastthouforsakenme.png) "My God, My God, why hast thou forsaken me?" (Matthew 27:46)\[/caption\]

To ensure that He suffered every torment that normal man is prone to, Christ allowed Himself to experience despair. Up to this point, Jesus had suffered mainly physically. These torments had left His body racked with pain and agony. But now it was time for the ultimate pain, the pain a soul feels when it is separated from God.

The soul is spiritual being in the image of God. The human soul is like a plant is nourished by the bright sunlight of God. The human soul needs this light to grow and flourish. However, unlike a plant, the human soul does not die when it is separated from God because it cannot die. Instead the soul endures great and debilitating agony. It was this kind of agony that Our Lord willingly accepted on the Cross.

O sinful man, how can you claim that Our Lord does not understand the pain you are going through? He has suffered every imaginable punishment. He has felt the rejection of His own people. He has endured the dreadful physical pains of a brutal scourging and ignominious death on a Cross. He had endure the despair of a soul separated from God. He understands pain, agony, loss and despair. And He wishes to console you. He stands with arms out stretched on the Cross, looking to comfort you in all your distress.

Lord Jesus Christ, You know better than anyone what suffering I am enduring. I beg you to give me the grace and strength to endure these hardships, that I may offer them as penance for my sins. Help me to never refuse my cross, so that by taking it up daily I may be worthy of You one day. Amen.
