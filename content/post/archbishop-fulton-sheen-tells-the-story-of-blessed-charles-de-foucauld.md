---
title: "Archbishop Fulton Sheen Tells the Story of Blessed Charles de Foucauld"
date: "2012-10-18"
categories: 
  - "archbishop-fulton-j-sheen"
  - "roman-catholic"
  - "saints"
  - "video"
tags: 
  - "archbishop-sheen"
  - "catholic"
  - "saint"
---

In this video, Archbishop Fulton Sheen tells the story of Blessed Charles de Foucauld, a modern martyr.

\[youtube=http://www.youtube.com/watch?v=Qtx9MFo7J5E\]
