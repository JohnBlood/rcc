---
title: "Only One Way to Receive Holy Communion - Part 2"
date: "2011-01-23"
categories: 
  - "bishops"
  - "holy-communion"
  - "liberalism"
  - "mass"
  - "pope-benedict-xvi"
  - "priest"
  - "priest"
  - "roman-catholic"
  - "satanism"
tags: 
  - "bishop"
  - "catholic"
  - "christ"
  - "communion"
  - "critique"
  - "pope"
---

**History**

Most people are under the illusion that Communion in the Hand has always been a part of way Catholics received the sacraments.  If you have read the quotes from Church fathers and Church documents in the previous part of this series, you will know this is not true.

Even in the face of all the documentation to the contrary, there are many who say that Communion in the hand was practiced by the Early Church.  They go on to say, “If they did it, why can’t we?”  There was a book published in the 1980s called _Warnings from Beyond_.  This book contained revelations made by demons during a series of exorcisms from 1975 to 1978.  When asked about Communion in the hand, they revealed the following: [![LastSupperCommunion](images/lastsuppercommunion_thumb.png "LastSupperCommunion")](http://realromancatholic.files.wordpress.com/2011/01/lastsuppercommunion.png)

> It is we who schemed and plotted for that. We said to ourselves: If we can manage to introduce Communion in the hand among the early Christians, then later on they will be able to say: “There was Communion in the hand previously at the time of the first Christians.” And so it was that this Council, these men of today, would be able to say: “The first Christians communicated in the hand, therefore there is nothing wrong in it. They were the first Christians, It was the time of the life of Christ, they were near Christ. Therefore, it absolutely cannot be sinful.”
> 
> True, they did not know that this was not wanted by God. At that very moment, even then, we said to ourselves that if we could make that happen, the result would be a certain lukewarmness. However, Communion in the mouth was brought back. Certain saintly souls and very great Doctors of the Church saw clearly where this was leading, and that it would be better and that there would be much more respect if He up there (he points upward) were received in the mouth... if they could not simply take Him into their hands, into their filthy hands ... with nails too long or varnished, or uncared-for hands. We cannot even say it all. There are often people who have not washed their hands all day, when they go somewhere... I do not want to say that! ... It is a frightful irreverence.

If Communion in the hand is so bad and contrary to the counsel of many saints and the laws of the Church, how did it become so prevalent?  Through disobedience to the Pope.  His Excellency Juan Rodolfo Laise, in his book **_Communion in the Hand: Documents and History_**, stated the following: “It is true that the practice spread but this was due only to the fact that the Episcopal Conferences allowed its introduction without the demanded conditions being in existence and without taking into consideration the exhortation of Paul VI.”

Hoping to stop this disobedience form spreading, Pope Paul VI issued four restrictions in _Memoriale Domini_ before Communion in the hand could be allowed:

> (a) the indult could only be requested **if** Communion in the hand was an already established custom (i.e., disobedient abuse) in the country, and
> 
> (b) **if** “by a secret vote and with a two-thirds majority” the episcopal conference petitions Rome,
> 
> (c) **then** Rome would grant the necessary permission,
> 
> (d) once the permission was granted, **several conditions** had to exist simultaneously (among these conditions, no loss of sacred particles and no loss of faith in the Real Presence), or Communion in the hand was not permitted, even with the indult. These conditions are outlined in “En réponse à la demande,” which is attached to the _Memoriale Domini_ instruction.

In 1977, the US National Council of Catholic Bishops met to vote to petition Rome for a dispensation.

Fr. John Hardon, S.J. declared on November 1, 1997 in Detroit, Michigan:

> **“To get enough votes to give Communion on the hand, bishops who were retired, bishops who were dying, were solicited to vote to make sure that the vote would be an affirmative in favor of Communion in the hand. Whatever you can do to stop Communion in the hand will be blessed by God.”**

For more detailed information on the history of Communion in the hand, go [here](http://www.tldm.org/news5/cinh2.htm).

**Particles**

One of the major problems with Communion in the Hand is the problem of the particles.  Just as a piece of bread has many crumbs, so do that consecrated hosts have many crumbs or particles.  Each one of those particles is the sacred Body and Blood of Jesus.  The Council of Florence said, “Christ is present in every Particle of the consecrated Host and of the consecrated wine, when separated from the rest.”  The Council of Trent said, “If anyone denies that the Venerable Sacrament of the Eucharist that Christ is present under every part of each Species when separated, let him be anathema.”[![PopeBenedictCommunionNun](images/popebenedictcommunionnun_thumb.png "PopeBenedictCommunionNun")](http://realromancatholic.files.wordpress.com/2011/01/popebenedictcommunionnun.png)

When a person received Communion in the hand, their hand is covered with hundreds of tiny particles.  It is almost impossible to consume all of them.  When a person received Holy Communion on the tongue, there is not problem about particles because the particles fall on the tongue and are consumed.

Why is this so important?  Because is it a sign of irreverence, however unintended, towards Our Lord’s Real Presence in the Most Blessed Sacrament.  Here is how several saint reacted when they found particles of consecrated hosts.  "When St. Teresa Margaret found a Fragment of a Host on the floor near the altar, she broke into tears because she thought about the irreverence that might be shown to Jesus, she knelt in adoration before the Particle until a priest came to take It and put It in the tabernacle."  "Once when St. Charles Borromeo was distributing Holy Communion, he inadvertently dropped a Sacred Particle from his hand. The saint considered himself guilty of grave irreverence to Jesus, and was so afflicted that for four days he had not the courage to celebrate Holy Mass, and as a penance he imposed an eight-day fast on himself!"  " (Both quotes  are taken from, _Jesus Our Eucharistic Love_ by Father Stefano Manelli, O.F.M. Conv., S.T.D.)

**Satanists**

Most people do not know this, but there two groups of people who believe in the Real Presence of Our Lord in the Eucharist: Catholics and satanists.  If you put a hundred unconsecrated hosts and **one** consecrate host in front of a satanist and he will be able to tell you which one is consecrated.

Satanists are very happy about Communion in the hand.  It makes it much easier for them to get a hold of consecrated hosts to desecrate in their black masses.  Before Communion in the hand, satanists had to pay fallen or unscrupulous Catholics to pretend to take Communion in the mouth and smuggle it out.  Now, all a satanist has to do it go to a Mass and receive Communion in the hand.

Finally, remember and take to heart the following words of two modern saints (one beatified, the other declared a servant of God).

As reported by Fr. George Rutler in his Good Friday sermon at St. Agnes Church, New York in 1989, when **Mother Teresa of Calcutta** was asked by Fr. Rutler, "What do you think is the worst problem in the world today?" She more than anyone could name any number of candidates: famine, plague, disease, the breakdown of the family, rebellion against God, the corruption of the media, world debt, nuclear threat and so on. "Without pausing a second she said, **'Wherever I go in the whole world, the thing that makes me the saddest is watching people receive Communion in the hand.'"** **(note: Fr. Emerson of the Fraternity of St. Peter was also a witness to this statement by Mother Teresa).[![MotherTeresaCommunion](images/motherteresacommunion_thumb.png "MotherTeresaCommunion")](http://realromancatholic.files.wordpress.com/2011/01/motherteresacommunion.png)**

The late, great **Fr. John Hardon** spoken out against Communion in the hand. On November 1st, 1997 at the Call to Holiness Conference in Detroit, Michigan, there was a panel discussion in which Fr. John Hardon was one of the speakers who fielded various questions from the audience. One of the questions was about Communion in the hand. After explaining how the practice was illegally introduced into the United States, he concluded by saying, **“Whatever you can do to stop Communion in the hand will be blessed by God.”**

The third and final part will be completed shortly.  Watch for it.
