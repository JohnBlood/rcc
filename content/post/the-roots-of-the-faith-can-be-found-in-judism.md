---
title: "The Roots of the Faith Can Be Found in Judaism"
date: "2013-07-26"
categories: 
  - "book"
  - "latin-mass"
tags: 
  - "catholic"
  - "jewish"
  - "latin-mass"
  - "mass"
---

[![HowChristSaidTheFirstMass](images/howchristsaidthefirstmass.png)](http://realromancatholic.files.wordpress.com/2013/07/howchristsaidthefirstmass.png)The Catholic Faith is based on the strong foundation of the Jewish religion. You probably have heard this many times, but don't much beyond it. Well, there is a book that explain in-depth the connection between Judaism and Catholicism. The name of that book is _How Christ Said the First Mass_. The tremendous book was written by Rev. James L. Meagher. It starts off by detailing the ceremonies and rituals of Judaism. The section is quite extensive and covers the creation of the Ark of the Covenant to the Temple rituals. The last section is a complete description of how the Last Supper took place. It is well work a read for anyone who wishes to learn more about their Catholic faith. Tan books used to carry it, but it is now out of print. However, you can still get a digital copy of it. You can with go [here to Archive.org](http://archive.org/details/howchristsaidfir00meag) or here is [a copy hosted by me](http://db.tt/E2gcqQuL).

I greatly enjoyed this book and I hope you do to.

Please comment below if you have read or heard of this book before.
