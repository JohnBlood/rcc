---
title: "Dumb Catholics Will Throw the Election to Pro-Abortion Hillary"
date: "2016-10-28"
categories: 
  - "election"
tags: 
  - "donald-trump"
  - "election-2016"
  - "hilary-clinton"
coverImage: "trump-catholics.png"
---

[![trump-catholics](images/trump-catholics.png)](https://realromancatholic.files.wordpress.com/2016/10/trump-catholics.png)

Catholics are one of the largest voting blocks in the US. A large portion of that block has been responsible for a string of anti-life candidates being elected to the Presidency of the US. It looks like the dummies will do it again this year.

Here are some replies to common complaints against Trump.

Note: This post will cover both conservative and liberal Catholics. Yes, I know many will say that liberals can't be Catholics, but they like to pretend.

## Dumb Conservative Catholics

**He used to be pro-abort**

I'm sure that quite a few conservatives will point out that in the past Trump has always supported liberal ideas, like abortion. However, during the course of this election, he has become very conservative. They will say that this disqualifies him because changed his position to get elected.

A person has the right to change his mind and in Trump's case, he has changed for the better. He isn't the first person to change his politics. Ronald Reagan also changed his political leanings.

Trump has made it clear repeatedly that he will support limits on abortion. He has also repeatedly called for the defunding of Planned Parenthood. He recently pledged that [he would remain pro-life](https://www.lifesitenews.com/news/trump-to-catholic-leaders-i-am-and-will-remain-pro-life) and "protect the rights of Catholics to live their faith". He is also the [first presidential candidate to sign an anti-porn pledge](https://www.lifesitenews.com/news/donald-trump-signs-first-ever-presidential-anti-porn-pledge). To prove his commitment to his pro-life agenda, Trump picked the most pro-life VP candidate in recent history. Not only that, but he has appointed a [number of pro-life people to his campaign](https://www.lifesitenews.com/news/donald-trump-names-32-pro-life-leaders-to-his-pro-life-coalition).

Someone asked Cardinal Burke for advice on the coming election. [He said](https://www.lifesitenews.com/news/cardinal-burke-to-u.s.-voters-consider-candidate-who-defend-life-family-fre):

> “I think that what we have to do in this time is to look at both candidates to see if one of them will not, at least in some way, advance the common good, both with respect to the good of human life, the good of the family, the freedom of conscience, the care of the poor, and to look at that very carefully.”

On the other side of the coin, you have Clinton and Kaine who are both very committed to supporting abortion and expanding it. For Catholics, the choice is clear: a vote for Trump-Pence is a vote for life.

**We're Not Voting For a Pope**

One of the most common complaints I've heard from conservatives is that Trump is vulgar and unpolished. They point to comments he made about women years ago. They also point to the new claims by women that Trump assaulted them.

Hey, folks. We're not electing a pope. We're not election a saint. We are voting for the President of the United States. We are looking for a leader. Trump is a businessman who has built multiple properties in multiple countries. I don't think any of our current politicians could say the same. In fact, while Trump got his money by working for it, many politicians get kickbacks and take advantage of insider knowledge to make their fortune.

The majority of these supposed events with Trump took place decades ago. The liberal media has had to dig dead into Trump's past to find and dirty laundry. (Let's not forget that there are quite [a few indications that some of these allegations are made up or fabricated](http://pamelageller.com/2016/10/wikileaks-democrats-fake-trump-grope-ad.html/).) On the other hand, Clinton has so many skeletons in her closet that the door won't even shut. Consider the deaths in Benghazi, the Arab Spring that she created, the death of Gaddafi which resulted in much bloodshed, the millions of children who died because of her support for abortion, and the list goes on. Trump has apologized for the mistakes he did make, Clinton has not.

For those of you still say you don't like Trump, I'd like to point you to a Protestant pastor named [Bishop Aubrey Shines](http://www.breitbart.com/big-government/2016/10/15/exclusive-bishop-aubrey-shines-immigration-jobs-school-choice-donald-trump-candidate-black-voters-waiting/).

> "When I look at the Republicans who’ve abandoned Trump, I think to myself, ‘who’s your second choice?’ And if they really adhere to the Christian values of forgiveness — and Trump has apologized and has said ‘I was wrong‘ — then why not forgive, instead of showing so much disdain for him?

Remember what Our Lord said, "Let he who is without sin cast the first stone."

**Not Voting Is Not an Option**

I've heard many people say that since they don't like either one of the candidates, they won't vote. They seem to think that be doing so, they will be sending a message. Really? Who's going to know you withheld your vote?

Besides, if you don't vote, you have no right to complain about the results of the election. More importantly, not voting will ensure that pro-abort Hillary will get elected. The blood of the children who could have been saved from abortion will be on your hands.

**Third Party**

There are those who plan to vote in the election, but like the above group don't like either candidate. There are a number of "catholic" bloggers calling on people to vote for a third-party or write-in candidates. Third party candidates have never been successful. Voting third-party is the same thing as not voting.

Here are some [facts on third-party voting](http://listverse.com/2012/10/31/10-successful-3rd-party-election-candidates/). The third-party candidate to receive the most votes in the US was Teddy Roosevelt in 1912. In that election, Teddy won 27% of the votes through his Bull-Moose party. The most recent third-party presidential candidate to receive a large number of votes was Ross Perot in 1992. In that year, Perot only received 18.9% of the vote.

In short, the US has a two-party system and third parties have never had a real impact on politics.

## Dumb Liberal Catholics

I have heard one major complaint from liberals in the Church every election cycle: "There's too much focus on abortion. The economy and taking care of the poor are important too." My reply: "Ahh, yes, the seamless garment crap."

The Seamless Garment was idea introduced by the late Cardinal Bernardin of Chicago. Bernardin's idea was that "abortion, capital punishment, militarism, euthanasia, social injustice and economic injustice" were all of equal importance.

This is contrary to the teachings of St. John Paul II, who said that life issues were of paramount importance. Recently, Cardinal Gomez of LA reiterated that [abortion and euthenasia](https://www.lifesitenews.com/news/la-archbishop-rips-the-seamless-garment-abortion-and-euthanasia-stand-alone) stand out as the most important issues. [Pope Benedict](http://thewandererpress.com/catholic/news/our-catholic-faith/cardinal-ratzingers-2004-letter-forbids-catholics-to-vote-for-hillary-clinton-under-pain-of-mortal-sin/) also noted that abortion and euthanasia are not negotiable.

When asked about his "obsession with abortion", [St. John Paul II said](https://www.lifesitenews.com/blogs/heres-how-pope-john-paul-ii-handled-the-charge-of-obsession-with-abortion):

> "I must repeat that I categorically reject every accusation or suspicion concerning the Pope's alleged "obsession" with this issue. We are dealing with a problem of tremendous importance, in which all of us must show the utmost responsibility and vigilance. We cannot afford forms of permissiveness that would lead directly to the trampling of human rights, and also to the complete destruction of values which are fundamental not only for the lives of individuals and families but for society itself."

## Final Thoughts

This election goes beyond what we think of Trump. We are fighting for the future of the US. We are fighting for the future of the Church.

I would like to close with a final quote from St. John Paul II from the [1976 Eucharistic Congress](https://www.lifesitenews.com/pulse/two-timely-quotes-from-st.-john-paul-ii-on-his-feast-day) when he was a cardinal.

> "We are now standing in the face of the greatest historical confrontation humanity has ever experienced. I do not think that the wide circle of the American Society, or the whole wide circle of the Christian Community realize this fully. We are now facing the final confrontation between the Church and the anti-church, between the gospel and the anti-gospel, between Christ and the antichrist. The confrontation lies within the plans of Divine Providence. It is, therefore, in God’s Plan, and it must be a trial which the Church must take up, and face courageously."

Truer words were never said.
