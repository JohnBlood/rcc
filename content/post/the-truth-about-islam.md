---
title: "The Truth about Islam"
date: "2013-09-23"
categories: 
  - "islam"
  - "video"
tags: 
  - "belloc"
  - "great-heresies"
---

There are a lot of different explanations of the origins and beliefs of Islam. Most of what is available is incorrect because misinformation. Catholics need to beware of the history and beliefs of their past and future enemy.

Hillary Belloc called Islam one of the great heresies in his book by the same name. Here is part of what he said.

> "Mohammedanism was a heresy: that is the essential point to grasp before going any further. It began as a heresy, not as a new religion. It was not a pagan contrast with the Church; it was not an alien enemy.  It was a perversion of Christian doctrine. Its vitality and endurance soon gave it the appearance of a new religion, but those who were contemporary with its rise saw it for what it was-not a denial, but an adaptation and a misuse, of the Christian thing. It differed from most (not from all) heresies in this, that it did not arise within the bounds of the Christian Church. The chief heresiarch, Mohammed himself, was not, like most heresiarchs, a man of Catholic birth and doctrine to begin with. He sprang from pagans. But that which he taught was in the main Catholic doctrine, oversimplified. It was the great Catholic world-on the frontiers of which he lived, whose influence was all around him and whose territories he had known by travel-which inspired his convictions. He came of, and mixed with, the degraded idolaters of the Arabian wilderness, the conquest of which had never seemed worth the Romans' while.
> 
> He took over very few of those old pagan ideas which might have been native to him from his descent. On the contrary, he preached and insisted upon a whole group of ideas which were peculiar to the Catholic Church and distinguished it from the paganism which it had conquered in the Greek and Roman civilization. Thus the very foundation of his teaching was that prime Catholic doctrine, the unity and omnipotence of God. The attributes of God he also took over in the main from Catholic doctrine: the personal nature, the all-goodness, the timelessness, the providence of God, His creative power as the origin of all things, and His sustenance of all things by His power alone. The world of good spirits and angels and of evil spirits in rebellion against God was a part of the teaching, with a chief evil spirit, such as Christendom had recognized. Mohammed preached with insistence that prime Catholic doctrine, on the human side-the immortality of the soul and its responsibility for actions in this life, coupled with the consequent doctrine of punishment and reward after death."

The following videos are part 1 and 2 of a very interesting lecture given by Dr. Bill Warner on Islam. Dr. Warner's information is based on his reading of original sources, including the Koran.

\[youtube=http://www.youtube.com/watch?v=C9sYgqRtZGg\]

\[youtube=http://www.youtube.com/watch?v=t\_Qpy0mXg8Y\]
