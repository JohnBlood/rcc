---
title: "The Socialism That You Asked For"
date: "2012-11-15"
categories: 
  - "roman-catholic"
  - "socialism"
  - "video"
tags: 
  - "election"
---

My response to the election can be summed up in this video, which I have posted before.

\[youtube=http://www.youtube.com/watch?v=xG0x3NsCw3Y\]

Also, this newer video.

\[youtube=http://www.youtube.com/watch?v=js9JCXkQQaM\]

Prayer is the only thing that can protected us in the days ahead. Pray for your friends and family, but also for the clergy and those who will soon become our persecutors. God help us.
