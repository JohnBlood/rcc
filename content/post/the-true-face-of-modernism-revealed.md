---
title: "The True Face of Modernism Revealed"
date: "2011-01-25"
categories: 
  - "anglican"
  - "church-of-england"
  - "liberalism"
  - "modernism"
  - "protestants"
  - "video"
---

The following is an amusing clip from a British TV show "Yes, Prime Minister".  In it there is an explanation of modernism that is spot on.  Enjoy.

\[youtube=http://www.youtube.com/watch?v=u\_Ox24WIBEQ\]
