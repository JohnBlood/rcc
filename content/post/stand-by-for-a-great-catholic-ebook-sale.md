---
title: "Stand By for a Great Catholic Ebook Sale"
date: "2015-06-21"
categories: 
  - "announcement"
tags: 
  - "ebook"
---

[![IndieCatholicWritersSale](images/indiecatholicwriterssale1.jpg)](https://realromancatholic.files.wordpress.com/2015/06/indiecatholicwriterssale1.jpg)Hi, everyone.

This coming week the members of the Indie Catholic Authors Google+ group are having a sale. From June 24th to 26th, 16 great Catholic books (including [_Church Triumphant_](https://www.smashwords.com/books/view/529160)) will be available for 99 cents or less. Visit the Indie Catholic Authors blog post for more information: [https://indiecatholicauthors.wordpress.com/2015/06/15/major-catholic-ebook-sale-next-week/](https://indiecatholicauthors.wordpress.com/2015/06/15/major-catholic-ebook-sale-next-week/).

P.S. We desperately need your help. Please sign up for our [Thunderclap campaign](https://www.thunderclap.it/projects/27781-beyond-the-bestsellers) so we can reach more people. If you sign up, Thunderclap will post a one-time message on you Facebook, Twitter, or Tumblr account on June 24th about the sale. If we don't get the required 100 supporters by the 24th, no messages will be sent out. Please hurry, we only have 3 days left. Thanks in advance
