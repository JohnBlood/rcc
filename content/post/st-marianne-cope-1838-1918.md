---
title: "St. Marianne Cope (1838 - 1918)"
date: "2012-11-29"
categories: 
  - "roman-catholic"
  - "saints"
  - "tradition"
tags: 
  - "catholic"
  - "pope-benedict"
  - "saint"
---

In honor of Pope Benedict's canonization of two new saints back in October, here is a biography for St. Marianne Cope.

\[caption id="attachment\_551" align="alignright" width="276"\][![](images/stmariannecope.png)](http://realromancatholic.com/2012/11/28/st-marianne-cope-1838-1918/stmariannecope/) St. Marianne Cope\[/caption\]

St. Marianne Cope was born on January 23, 1838 to Peter Koob and Barbara Witzenbacher in the Grand Duchy of Hesse (modern-day Germany). She had ten siblings. The year after she was born her family immigrated to Utica, New York. From an early age, Marianne experienced a calling to religious life. However, her father became ill while she was in the eighth grade. Because she was the oldest, she took a job as a factory worker to support the family.

After her father’s death in 1862, Marianne’s siblings were old enough to support themselves. Now free from family responsibilities, she entered the novitiate of the Sisters of the Third Order Regular of Saint Francis in Syracuse, New York. After completing a year of formation, Marianne received the habit of a Franciscan sister. She started work as a teacher and later became a principal in a school for German-speaking immigrants in New York.

In the 1860s, Mother Marianne became a member of the governing board of her community. As part of this board, she was involved with establishing two of the first hospitals in the central New York area. Over the next two decades, Marianne worked as a nurse-administrator in several hospitals. Here, she became known for accepting outcast patients, such as alcoholics, and for her kindness, wisdom and practicality. She also required hand washing before treating patients before it became a standard practice. Many said there was no challenge too great for her.

In 1883, Mother Marianne received a request from King Kalakaua of Hawaii requesting help to care for those afflicted with leprosy. Fifty congregations had already refused his request, but Mother Marianne could not turn down such a request. She said, “I am hungry for the work and I wish with all my heart to be one of the chosen Ones, whose privilege it will be, to sacrifice themselves for the salvation of the souls of the poor Islanders... I am not afraid of any disease, hence it would be my greatest delight even to minister to the abandoned ‘lepers.’ Mother Marianne and six of her Sisters set out for Hawaii and arrived in November of the same year. Mother Marianne opened several hospitals for lepers. After seeing the abuse that leprosy patients were suffering at the hands of the government administrator, Mother Marianne demanded to be given full-charge of the hospital or the sisters would return to America. She was given control of the hospital. In two years, she accomplished so much that the King bestowed the Cross of a Companion of the Royal Order of Kapiolani on Mother Marianne for her work to relieve his suffering people

A new government enforced the exile of all lepers to the island of Molokai in 1888. Mother Marianne was asked if she would continue to care for the lepers on Molokai. This would mean that she would never be able to return to New York or see her family again. She replied, “We will cheerfully accept the work.” Once on the island, Mother Marianne took care of St Damien, known around the world as the Apostle of the Lepers, as he lay dying from the same disease his parishioners suffered from. After he had contracted the disease, St. Damien had been shunned by both civil and Church leaders. Mother Marianne, alone, cared for him. After, St. Damien died on April 15, 1889, Mother Marianne took charge of his ministry on the island. In the long run she succeeded in bringing many of St. Damien’s plans to fruition. Throughout the stress and hardships of caring for the lepers of Molokai, Mother Marianne continued to be an example of optimism and trust in God, inspiring many of those around her. Mother Marianne died on August 9, 1918 of natural causes. The fact that she never contracted leprosy was seen as a miracle by many. She was canonized by Pope Benedict XVI on October 21, 2012.
