---
title: "A Taste of Things to Come"
date: "2009-03-18"
categories: 
  - "introduction"
  - "protestants"
  - "roman-catholic"
---

This is the first of hopefully many posts to come.  If you go to my profile you will see that it says three things, “Born Roman Catholic.  Raised Roman Catholic.  Will die Roman Catholic.”  That will gives you some idea how important my faith is to me.

For me the Roman Catholic Church is the one, true, holy, and Apostolic church founded by Jesus Christ.  I know that will turn some people off, but let me assure my Protestant friends that I consider you an important part of the Body of Christ.  Back over 30 years ago, a teaching nun told my Dad that Protestants had the spirit of the Law (the Bible), while the Catholics had the letter of the Law (all the writing of the Father’s of the Church and the Traditions).  I have spend the last four years of my life attending a formerly Baptist, but now non-denomination university, and I can understand that what that Sister said was true.  (I will write more about this later.)

The main purpose of this blog will be to give a conservative viewpoint in an increasingly liberal world.  In particular, this blog will set it’s sights on the problems that liberalism has caused within the Roman Catholic Church.

I’d better end here and leave the good stuff for later.
