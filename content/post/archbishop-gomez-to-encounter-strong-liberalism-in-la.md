---
title: "Archbishop Gomez to Encounter Strong Liberalism in LA"
date: "2010-04-08"
categories: 
  - "bishops"
  - "liberalism"
  - "liturgical-dance"
  - "orthodox"
  - "roman-catholic"
tags: 
  - "archbishop-gomez"
  - "catholic"
  - "mass"
---

Once he heard that Archbishop Gomez was going to take over for Cardinal Mahony in the ultra-liberal LA archdiocese, Hitler decided to post a new rant on Youtube.

\[youtube=http://www.youtube.com/watch?v=fCJPmvfU4Ow\]

Seriously however, Gomez will be facing strong liberal feeling.  LA has been the most liberal diocese in the country for many years under Mahony and others.  Watch the following for an example of the foolishness that went on as recently as this March.

\[youtube=http://www.youtube.com/watch?v=nZ5it20gKqw\]

It's really sad all this has gone on.  Instead of being a solemn remembrance of Christ's sacrifice, the Mass has become like many of the Protestant services I saw while I went to a Baptist college.  Liturgical dance has no place in Church.  I'll say more on that later.  For now it is enough to say that Gomez has a lot of work to do.

For those who don't know much about Archbishop Gomez, he is not perfect, but he is light-years ahead of Mahony.  The LA Times wrote:

\[caption id="attachment\_53" align="alignright" width="256" caption="Archbishop Gomez of LA Archdiocese"\][![Archbishop Gomez of LA Archdiocese](images/archbishopgomez1.jpg "Archbishop Gomez")](http://realromancatholic.files.wordpress.com/2010/04/archbishopgomez1.jpg)\[/caption\]

> And like many of those Latinos, he is at once a conservative and a progressive: unyielding in his opposition to abortion and gay marriage, passionate in his advocacy for immigrants and the poor, confounding to those who try to wedge him into the traditional right-left political paradigm.
> 
> During his six-year tenure atop the San Antonio archdiocese, Gomez emerged as a leading advocate for doctrinal conformity, determined to stave off what he saw as creeping secularism in the church.
> 
> He denounced one Catholic university when it invited then-Sen. Hillary Clinton to campus, because she favored abortion rights, and another when it invited a Benedictine nun, because she had advocated the ordination of women. Under his reign, a local Catholic high school ended its relationship with an organization that raised money to fight breast cancer, because the same organization gave grants to Planned Parenthood. After a 17-year-old lay advisory commission created by his predecessor suggested that gay marriage might be a human rights issue under one reading of the church's teachings, Gomez disbanded the commission.
> 
> ...
> 
> Yet in Denver, where Gomez served as a bishop, he was the driving force behind the creation of Centro San Juan Diego, both a formation center for lay leaders and a social services center for immigrants. Roughly 30,000 adults visited the center last year to learn English and computer skills and obtain free legal advice to gain citizenship and fight deportation.
> 
> Gomez has marched for immigrants' rights and worked to bridge the complex cultural gap between long-established Mexican American communities and newly arrived immigrant communities from elsewhere in Latin America.

On top of that he is a member of Opus Dei and was consecrated bishop by Archbishop Charles J. Chaput of Denver.

I think that Pope Benedict made a good choice when he picked Gomez.  We need to pray for Archbishop Gomez as he marches towards his inevitable battle with liberalism.  Let's pray that he is victorious and helps California become more conservative.
