---
title: "Planned Parenthood and Resurrection Men"
date: "2015-08-11"
categories: 
  - "abortion"
tags: 
  - "planned-parenthood"
coverImage: "resurrection-men.png"
---

\[caption id="attachment\_1035" align="alignright" width="500"\][![resurrection-men](images/resurrection-men.png)](https://realromancatholic.files.wordpress.com/2015/08/resurrection-men.png) Resurrection men going about their grisly business.\[/caption\]

Unless you are a fan of Victorian history or literature, chances are that you have never heard of the term resurrection men. Resurrection men (also known as body snatchers, but I prefer the term resurrection men) were men paid by doctors to dig up the bodies of the dead. These corpses were then used for dissection so that the doctors could learn how the body works. This job was highly illegal and had to be done under the cover of darkness.

When I first heard of the five videos (so far) exposing Planned Parenthood's sale of baby body parts, I thought of the resurrection men. In many ways, the two are very similar. Both disturb the rest of the dead for use in medical experiments. Both are also highly illegal. Both are for profit. However, back in the day there was a lot more outrage over body snatching than there is over Planned Parenthood's sale of baby body parts. In fact, there were several riots when people learned about loved ones being dug up for use in experiments.

Those who support Planned Parenthood are making arguments that could have been used to defend the use of resurrection men hundreds of years ago. These heartless people claim that PP is not profiting from the sale of these body parts, that they are going used to further medical research. What the bodies are used for is beside the point. PP is profiting from the murder of those who cannot defend themselves. These people claim that PP is only charging a transportation fee. Based on the prices that PP is charging in the videos, they must be making deliveries in a fleet of Hummers.

There was a pair of resurrection men who operated in a similar fashion to Planned Parenthood. The infamous Burke and Hare operated in the 1820s and decided that the best way to get fresh corpses for their clients (and more money) was to commit murder. The two men murdered sixteen people before they were caught. Hare was able to testify against his partner in exchange for immunity and Burke was hanged. Like I said, the public was more outraged about this behavior in the past.

As a country, we have to awake from our slumber and realize the evil that is going on in our world. This Planned Parenthood scandal did not come into being overnight. It had been happening for a long time and no one did anything about it. That time is past. We must storm heaven with prayers and our politicians with calls, texts and emails demanding that this madness stop. If we do not stop soon, I have no doubt that the US will feel the wrath of an angry God.

\===

If you liked this post, don't forget to share using one of the buttons below. If you would like to get future updates via email, as well as special offers on ebooks, sign up for my free email newsletter [here](http://eepurl.com/bf8teP).
