---
title: "The Most Beautiful Song That Elvis Ever Sang"
date: "2011-03-11"
categories: 
  - "elvis"
  - "our-lady"
  - "rosary"
  - "saints"
  - "song"
tags: 
  - "video"
---

Elvis sang many beautiful songs, especially Gospel. In fact, Elvis got his start singing Gospel. Now listen to the most beautiful song about Our Lady ever sung by a non-Catholic.

\[youtube=http://www.youtube.com/watch?v=9DvPSl0EuA4\]
