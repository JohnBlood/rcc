---
title: "Early Church Documents Lay the Foundation of the Catholic Church"
date: "2010-11-09"
categories: 
  - "bible"
  - "church-fathers"
  - "roman-catholic"
  - "tradition"
tags: 
  - "catholic"
  - "faith"
  - "holiness"
  - "prayer"
---

In recent years, there seems to be a move to discard old documents of the Church in favor of modern documents.  Some people in this movement appear to believe that older works are worthless because the authors are not up-to-date with the latest advances in history, science or what have you.

Age means nothing in term of the authority of a document.  If age rendered a document worthless, we should abandon the Bible itself.  We should also throw out the Constitution, after all what did those Founder Fathers know all those years ago.

The truth of the matter is that these early documents of the Church are the pillars that support Faith that we now practice.  The firm foundation, the bedrock that these pillars rest on is the Holy Bible.  Built on top of this foundation, there are over 2000 years worth of theological works of the Saints and Church Fathers, the encyclicals and letters of the Popes.  No other church in the world can say, “We have 2000 years of documents to provide help and counsel in any situation, trouble or crisis.”  For example, the non-Catholics have the Bible as their foundation, but their theologians only started writing 500 years ago.

We must constantly constantly consult this treasury of knowledge and insight to better understand our one, true Faith.  Only by constant study and prayer can we grow in our Faith and holiness.  We are blessed with an enormous treasure, let us make good use of it.
