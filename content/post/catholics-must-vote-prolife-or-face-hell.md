---
title: "Catholics Must Vote Prolife or Face Hell"
date: "2012-10-30"
categories: 
  - "abortion"
  - "election"
  - "hell"
  - "politics"
  - "roman-catholic"
tags: 
  - "catholic"
  - "election"
---

[![](images/firesofhell.png "FiresofHell")](http://realromancatholic.files.wordpress.com/2012/10/firesofhell.png)No matter what anyone says, Catholics must vote for pro-life, anti-abortion, anti-gay marriage, pro-traditional family, and pro-Bible politicians or endanger their immortal soul. This is a topic that every priest and bishop in the United States should have been preaching about for the last year or so. However, the responsibility has fallen to me and other laymen. Any Catholic who dares to vote for an anti-life, anti-family and anti-God candidate must go to Confession and beg God for forgiveness or they will incur God's wrath on themselves and our country.

There are certain unspeakable acts which Catholics can never support in any way, these acts are intrinsically evil by their nature. They seek to disrupt the natural order that God intended, they destroy life and call down God's vengeance. These vile acts are murder of the unborn and the destruction of traditional marriage. The problem is that the support of these two acts has become part of the platform of the Democratic Party. Anyone who supports the Democratic Party in any way, by extension, also supports the murder of the unborn and the destruction of traditional marriage.

Many die-hard Democrat supports will say, "But what about immigration or taxes? Those are important too and I will vote for a candidate based on what he will give me." If you think that way, I guess you had better enjoy this life while you can because this attitude will not help you when you face God the Father on judgment day. When God asks you why you did not help save the children He created or the Sacrament that He created for all time, your material gains in this life will do you no good.

Here is a good Protestant saying what many Catholics clerics are afraid to said for fear it will ruin their all-important popularity or their just as important tax-exempt status.

\[youtube=http://www.youtube.com/watch?v=rrcmTjijRS8\]

That is the problem with the world that we find ourselves in. We been taught from birth to seek material comfort and enjoyment at the expense of our eternal souls. We must break this trend before it is too late.

[![](images/scales.png "Scales")](http://realromancatholic.files.wordpress.com/2012/10/scales.png)"Aren't you overreacting?" many of you will ask. "Evil has always been with us and we are still here. Abortion has been around for years, but we are still here." Fools, how long do you think God will turn the other cheek. Since 1973, over 50 million children have died from abortion in the US alone. For every child that does in the world, a weight is added to the scales of God's justice. On the other side of the scales is the good works done around the world. That scales is already tipping heavily from the weight of those murdered children. Once it reaches the breaking point (which is very soon), God's wrathful and vengeful justice will be unleashed.

When you go to vote next week, don't think about what you can get from the politicians. Think about how you would like to spend eternity: in eternal bliss because you upheld God's laws or in eternal torment because you rejected God's laws in favors of passing temporal comfort. Pray that others will do the same. Email and give this article to your silent pastors, liberal relatives and friends. May God have mercy on us if we fail.
