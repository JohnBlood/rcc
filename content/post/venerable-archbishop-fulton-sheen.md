---
title: "Venerable Archbishop Fulton Sheen Talks about Angels"
date: "2012-10-28"
categories: 
  - "angels"
  - "archbishop-fulton-j-sheen"
  - "roman-catholic"
  - "tv"
  - "video"
tags: 
  - "angel"
  - "archbishop-sheen"
---

\[youtube=http://www.youtube.com/watch?v=bQSs8QwZFvU\]
