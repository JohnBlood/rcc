---
title: "Archbishop Fulton J Sheen Preaches on Confession"
date: "2012-07-24"
categories: 
  - "archbishop-fulton-j-sheen"
  - "roman-catholic"
  - "sacraments"
  - "video"
tags: 
  - "archbishop-sheen"
  - "catholic"
  - "confession"
  - "sacrament"
---

Once again, here is Archbishop Fulton Sheen. This time he is giving a talk about the great sacrament of Confession. Enjoy.

\[youtube=http://www.youtube.com/watch?v=jgFndZIH568\]
