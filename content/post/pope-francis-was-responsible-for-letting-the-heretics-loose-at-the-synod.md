---
title: "Pope Francis Was Responsible for Letting the Heretics Loose at the Synod"
date: "2015-12-04"
categories: 
  - "2015-synod-on-the-family"
  - "pope-francis"
---

\[caption id="attachment\_1076" align="alignright" width="500"\][![If the poor slobs only knew.](images/francis-snicker.png)](https://realromancatholic.files.wordpress.com/2015/12/francis-snicker.png) If the poor slobs only knew.\[/caption\]

_This is **part three** of a series of three articles this week where I comment on the 2015 Synod on the Family in Rome. Hold onto your seats._

Before I start, I would like to apologize for the fact that this final post on the 2015 Synod on the Family has taken over a month to publish. Every time I went to write this article, something came up. But I'm here now to finish what I started. Now that is out-of-the-way, we can begin

* * *

I know there will be quite a few cries of "papal infallibility" from the audience because of the title of this post. As a Catholic, I am very aware of the doctrine of papal infallibility. However, papal infallibility **only** applies when the pope speaks from the Chair of Peter on a topic of Faith or Morals. The last time this happened was when Pope Pius XII declared the dogma of the Immaculate Conception in **1854**. It has not happened since.

Everything that took place during the 2015 Synod on the Family (as well as the 2014 synod) can be laid at the feet of Pope Francis. As pope, Francis is the leader of all synods that are called during his papacy. He is the president of the synod. He invites the participants. He determines the agenda. At the end of the synod, he ratifies the final documents. The synod is his creation.

Francis could have created the illusion that this year's synod was going to be more conservative (or at least more moderate) after last years pro-homosexual union love fest. However, he appointed the same liberals to lead this year's synod.

Now some will argue that Pope Francis was not aware of the liberal and sometimes heretical leanings of those he invited to the synod. I doubt that very much. Before he was elected pope, Francis was a cardinal for 14 years. You can't be in the hierarchy of the largest church in the world for that long without getting to know the other players, at least by reputation. Besides he saw what they came up with last year.

An example of the heterodox clerics that Francis included in the synod is Cardinal Deneels of Belgium, who I mentioned in a previous post. Deneels is the retired head of the Catholic Church in Belgium. During his time as primate, Deneels pushed for the government legalization of abortion and same-sex marriage. Deneels also covered up for a fellow bishop who was a pedophile. Deneels went to the bishop's victim (also the bishop's nephew) and told him to keep quiet because the bishop was going to retire soon.

According to Deneels' autobiography, he and a group of liberal cardinals pressured Pope Benedict to retire. During the resulting conclave, the same group of cardinals campaigned and canvassed votes (which is against Canon Law) to get Francis elected. Again this is what Deneels said in his own book. These are the people who Francis invited to the Synod. It seems like more than a coincidence.

In the end, all we can do is pray that God will give us the strength and wisdom to overcome the advances of the Devil as he tries to destroy his ultimate enemy: the Catholic Church.

* * *

If you found this article interesting, be sure to share and subscribe to my email newsletter.
