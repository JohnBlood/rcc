---
title: "Happy 84th Birthday, Pope Benedict XVI"
date: "2011-04-17"
categories: 
  - "birthday"
  - "pope-benedict-xvi"
tags: 
  - "birthday"
  - "pope"
---

\[caption id="attachment\_258" align="alignright" width="262" caption="Pope Benedict XVI"\]![Pope Benedict XVI](http://realromancatholic.files.wordpress.com/2011/04/popebenedictxviwave6.png?w=262 "Pope Benedict XVI")\[/caption\]

Pope Benedict XVI celebrates his 84th birthday today. Not surprisingly, the Pope spent his birthday working. He gave a speech to welcome the new Spanish ambassador, appointed an Italian bishop and met with several members of the hierarchy. There was not rest for the leader of the world's Catholics even on his birthday or a Saturday. Pope Benedict XVI's birthday falls on the eve of Holy Week, one of the busiest weeks in the year for him. The Holy Father received congratulations from leaders and laity all over the world. Continue to keep Pope Benedict in your prayers. May he be successful in the work he does and may he rule over the Roman Catholic Church for many years to come. God Bless, Pope Benedict XVI.
