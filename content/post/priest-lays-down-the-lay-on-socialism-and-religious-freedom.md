---
title: "Priest Lays Down the Law on Socialism and Religious Freedom"
date: "2012-06-18"
categories: 
  - "politics"
  - "priest"
  - "priest"
  - "religious-freedom"
  - "socialism"
  - "video"
tags: 
  - "catholic"
  - "priest"
---

I found this video the other day. All I can say is "Finally, here is a priest (Fr. Andrew Kemberling) who is not afraid to speak out against socialism." Watch and forward to your friends and relatives. This should be a rallying cry for all Christians in the US.

\[youtube=http://www.youtube.com/watch?v=xG0x3NsCw3Y\]

From the video's description: Fr. Andrew Kemberling was invited to lead the opening prayer at the 2012 Colorado Republican State Assembly and Convention in the Magness Arena at the University of Denver. The moral challenges facing our country are not caused by political affiliation, but rather by attacks on religious freedom. He invites all people of conscience to uphold religious freedom.
