---
title: "Our Duty As Catholic Lay People"
date: "2010-08-15"
categories: 
  - "bishops"
  - "orthodox"
  - "roman-catholic"
tags: 
  - "catholic"
  - "duty"
  - "pastors"
  - "priests"
  - "protect"
---

After reading my latest post, some of you might think I should rename this blog "_Bashing Bishops_".  The fact of the matter is that there are good bishops; they are not 100% bad, just 99.5% bad.  During the last national election, there were a number of bishops that stood up an said "No" to pro-abortion politicians receiving the sacraments.  Bishops Bruskewitz and Burke were two of the most well-known.  There have been others, but they are too few and far in between.  (Several of these prelates have been given jobs in the Vatican.  More on this later.)

[![](http://realromancatholic.files.wordpress.com/2010/08/monstrance31.jpg?w=281 "Monstrance")](http://realromancatholic.files.wordpress.com/2010/08/monstrance31.jpg)While, most of what I have said has been pointed at the bishops; we, the laity, have our own job to perform.  We must get down on our knees and pray before the Blessed Sacrament that our bishops will be filled with the zeal and courage necessary to do their job.  Our most powerful weapon is the Rosary, as shown by St. Pio and many other saints.  We must storm Heaven with our prayers so that our religious leaders will be enlightened by the Holy Ghost.  We cannot stop there.  We must perform sacrifices and penance for those leaders who have offended God, as well as those who have the courage to stand up for what's right.  The world glorifies evil in all forms, we must fight the spiritual battle, so that our spiritual leaders will have the strength and determination to persevere until the final victory.

# Bruskewitz
