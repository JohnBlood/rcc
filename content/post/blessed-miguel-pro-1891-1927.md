---
title: "Blessed Miguel Pro (1891-1927)"
date: "2012-11-24"
categories: 
  - "priest"
  - "roman-catholic"
  - "saints"
tags: 
  - "saint"
---

Yesterday was the feast of Blessed Miguel Pro. I did not have a chance to put this up yesterday, but here is a biography that I wrote on Blessed Pro for my parish's bulletin. Enjoy and Viva Cristo Rey.

[![](images/blessedmiguelpro.png "BlessedMiguelPro")](http://realromancatholic.files.wordpress.com/2012/11/blessedmiguelpro.png)Blessed Miguel Pro was born on January 13, 1891 in the town of Guadalupe in the Mexican state of Zacatecas. His father was a mining engineer and his mother was a dedicated homemaker. Miguel was the third of eleven children. From a very young age he was known for his fondness for humor and practical jokes.

When he was very young, Miguel was given some bad fruit by an unsuspecting Aztec woman who idolized him. He was struck was a serious illness that affected his brain. The doctors told his parents that he would either die or become mentally handicapped. Miguel lived for a year, unable to speak or recognize his parents. Finally, when Miguel was near death, his father took him in his arms and knelt before an image of Our Lady of Guadalupe. Looking at the image, Senor Pro prayed, “Madre mia, give me back my son.” Shortly thereafter, Miguel came out of his death trance. Several days later, he was restored to health, both mentally and physically.

Starting in 1910, two of Miguel’s sisters entered the convent. This inspired Miguel to think about his own vocation. On August 10, 1911, Miguel entered the Jesuit seminary in El Lana. He continued his studies until 1914, when anti-Catholic persecution by the government forced him and many others to flee the country. He traveled to California, Spain, Nicaragua and Belgium, all the while continuing his studies. He was ordained on August 31, 1925. Meanwhile back in Mexico, the government had taken away the clergy’s civil rights and they were prohibited from wearing clerical garb in public. Many other laws were passed with the goal of suppressing the Catholic Church.

In the summer of 1925, Miguel returned to Mexico. Because of the continuing and increasing persecution of Catholics, he was forced to practice his priesthood in secret and wear disguises to move about. One time, he had his picture taken in front of a police station, while in disguise, as a policeman passed. On another occasion, he was being pursued by the secret police. Coming alongside a young lady, he said, “Help me. I am a priest.” The couple pretended they were lovers until the police gave up the search. Miguel wore a wedding ring for just such an emergency. After surviving many close calls in the service of God’s people, Miguel was finally arrested in November of 1927.

Hoping to make an example of the young priest, President Calles ordered that Miguel be executed without a trial on November 23, 1927. Calles also had photographers on hand to document the execution. After blessing the soldiers in the firing squad, Miguel knelt to say his final prayers. With a Crucifix in one hand and a Rosary in the other, Miguel faced the firing squad with his arms in the form of a cross. His last words were “Viva Cristo Rey!” - “Long Live Christ the King!”. Ignoring the laws against public demonstrations, 40,000 people lined his funeral procession, which passed under President Calles windows. Another 20,000 waited at the cemetery. The pictures of the execution in the newspapers, instead of destroying the will of the Catholic Cristeros fighting for their faith, actually increased it. Miguel was beatified by Pope John Paul II on September 15, 1988.
