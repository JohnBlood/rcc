---
title: "Archbishop Sample and the Latin Mass"
date: "2014-03-24"
categories: 
  - "archbishop-sample"
  - "latin-mass"
tags: 
  - "latin-mass"
  - "pope-benedict-xvi"
  - "summorum-pontificum"
---

One of the youngest supporters of the Latin Mass in the church hierarchy is Archbishop Alexander Sample of Portland, Oregon. Back when he was bishop of Marquette, Michigan, Archbishop Sample went out of his way to learn how to say the Latin Mass after Pope Benedict XVI issued the motu poprio Summorum Pontificum. Now that he's been move to one of the most liberal archdiocese in the country, he hasn't stop. As recently as March 1, the archbishop offered a High Mass. Here is the homily from that Mass. It's inspiring to hear these words from a 53-year-old archbishop.

\[youtube=http://www.youtube.com/watch?v=3K-x6odm8Sc\]
