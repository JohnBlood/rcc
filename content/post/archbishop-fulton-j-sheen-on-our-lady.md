---
title: "Archbishop Fulton J Sheen on Our Lady"
date: "2012-07-11"
categories: 
  - "archbishop-fulton-j-sheen"
  - "our-lady"
  - "pope-benedict-xvi"
  - "return-to-tradition"
  - "roman-catholic"
  - "saints"
tags: 
  - "archbishop-sheen"
  - "catholic"
  - "video"
---

In the last couple of weeks, Pope Benedict XVI declared Archbishop Fulton J Sheen venerable. In honor of this influential Catholic evangelist, I would like to present this beautiful and moving talk given by the Archbishop on Our Lady.

\[youtube=http://www.youtube.com/watch?v=kgQ\_YQCZRFM\]
