---
title: "Catholics Must Vote Like Catholics"
date: "2010-11-01"
categories: 
  - "abortion"
  - "bishops"
  - "cardinal-burke"
  - "liberalism"
  - "politics"
  - "priest"
  - "priest"
  - "roman-catholic"
  - "sin-of-omission"
tags: 
  - "catholic"
  - "pope"
---

This coming Tuesday, voters all around the country will be making their way to the polls.  The problem is that most Catholics will go to the polls without having been correctly instructed how to vote by their priests and bishops.  Some may disagree with this statement, but the overwhelming number of Catholics who voted for pro-death candidates in last election will bear me out.

When he was Cardinal Ratzinger, the current Pope said that pro-abortion politicians should be refused Holy Communion.  Only a very few, brave bishops and priests did this.  One of them was Archbishop Raymond Burke (soon to be Cardinal Burke) from St. Louis.  In 2008, Pope Benedict XVI called Burke to Rome.  It was originally thought by some that Burke had been called to Rome to take him out of the spotlight of American politics.  Things have happened to show that the opposite is true.  Cardinal-elect Burke was named head of the Supreme Tribunal on the Apostolic Signatura (basically the highest court in the Roman Catholic Church).  This makes him the second highest American in the Roman Catholic Church.

Recently, Cardinal-elect Burke gave an interview to Catholic Action for Faith and Family where he emphatically stated that Catholics cannot, under any circumstance vote for a pro-abortion candidate.  He said:

> “You can never vote for someone who favors absolutely what’s called the right to choice of a woman to destroy human life in her womb or the right to a procured abortion…You can never justify voting for a candidate who not only does not want to limit abortion but believes that it should be available to everyone.”

\[youtube=http://www.youtube.com/watch?v=OK514UNkvfU\]

I have said it before, Catholics must vote like Catholics or God will punish this country.  Seriously, how can God continue to bless a country that kills millions of His children every year.  This calls out to God for His just wrath.

God bless you and may you vote as God, not man, wants you to.
