---
title: "I Wish the Catholic Media Would Tell the Truth"
date: "2015-10-21"
categories: 
  - "2015-synod-on-the-family"
tags: 
  - "fr-dwight-longnecker"
  - "part-one"
---

\[caption id="attachment\_1064" align="alignright" width="320"\][![The Cave from Plato's Republic](images/platos-cave.png)](https://realromancatholic.files.wordpress.com/2015/10/platos-cave.png) Only the shadows are real. Ignore the people behind the wall\[/caption\]

_This is **part one** of a series of three articles this week where I comment on the 2015 Synod on the Family in Rome. Hold onto your seats._

When I look at how the mainstream Catholic news services report on this year's Synod on the Family, I am immediately frustrated. Most major Catholic news sites (ETWN, National Catholic Register, Catholic Herald, Patheos, and others) put on a cheery face and ignore all the errors and craziness coming out of the Synod. You have archbishops proposing giving Holy Communion to people who openly live in sin or people who are non-Catholic, but are married to a Catholic spouse. You have cardinals who want to "widen" the definition marriage to allow all kinds of perverse relations to be considered the norm. This is what these "professional" Catholic newsmen (and women) should report on, but they don't.

The problem goes further. If a Catholic has the audacity to report something that goes against the mainstream Catholic media, like the stories I mentioned above, the "professional" Catholics attack them. For example, one of my main sources for Catholic news is a site named [PewSitter](http://pewsitter.com/page_1.html), basically the Drudge Report of Catholic news. PewSitter does not host articles but points to important articles on other sites. PewSitter has been attacked by "professional" Catholic commentators such as Mark Shea and Fr. Dwight Longnecker.

I used to respect Fr. Longnecker because he appeared to be Traditional in his views, but he recently attacked PewSitter by saying it must be a liberal joke site because no real Catholic could believe the headlines. In fact, my previous post "[Catholics are Destroying Christianity](http://realromancatholic.com/2015/09/19/catholics-are-destroying-christianity/)" was written in response to an article he wrote.  (Interestingly, last year Fr. Longnecker wrote an article defending the Muslim religion by incorrectly stating that the Koran does not justify beheading. You can read more [here](http://www.jihadwatch.org/2014/09/catholic-priest-insists-quran-doesnt-justify-beheadings).) Father Longnecker, St. Gregory the Great said, “It is better that scandals arise than the truth be suppressed.” The Church has always sought to tell and preach the truth, why are you afraid to do so?

Why do I call these people "professional" Catholics? Well, to be honest (which as Catholics, we are supposed to be) I picked it up from Michael Voris. The term "professional" Catholic refers to people who get paid to promote the Catholic establishment's slogan "Everything is okay. Everything is alright." These people receive large salaries to hide and obfuscate the truth, so you will not become alarmed by the destruction of the Church and civilization around you. One the other hand, independent Catholic, like myself, receive little to no financial support. In the years that I have run this blog, my donations totaled a whole 99 cents. For me and others this is not a money-making proposition, it is our feeble attempt to help wake up enough Catholics to get the Catholic Church back on the path to saving souls.

By now, you may be wondering "What does that picture at the top the article have to do with any of this?" Simple. It is a reference to the cave allegory in Plato's Republic. For those of you who never heard of it or barely passed Philosophy in college (just like me), let me explain. In a cave, there are several people chained so they can only face the wall of the cave. Behind them, there is a light source and between them and the light source is a short wall. Behind the short wall, people pass by carrying different items. The people chained only see the blurry shadows of the items on the wall and think that is reality. When one of the prisoners gets loose, he goes outside and sees the true reality of the world. He returns to the cave and tries to convince the remaining prisoners that what they are seeing is not reality. They refuse to believe him, content with only the shadows of reality.

In this instance, the prisoners staring at the cave wall are the majority of Catholics. The people casting the shadows on the wall are the "professional" Catholics, who deal in fluff and half-truths. The prisoner who escaped to the outside and returned to tell his fellows the truth stands for all Catholics who cannot stand by meekly and see the Church they love ruined by clerics who are heretics.

We must pray for our fellow Catholics and spread the truth of what is happening around us before it is too late.

In my next article, I will ask the question "What the hell are the bishops thinking?"

* * *

If you found this article interesting, be sure to share and subscribe to my email newsletter.
