---
title: "Pope St. Pius X (1835 - 1914)"
date: "2013-08-22"
categories: 
  - "pope-pius-x"
  - "roman-catholic"
  - "saints"
tags: 
  - "catholic"
  - "saint"
---

[![PopePiusXHalo](images/popepiusxhalo.png)](http://realromancatholic.files.wordpress.com/2013/08/popepiusxhalo.png)Pope St. Pius X was born Giuseppe Melchiorre Sarto on June 2, 1835. His father was a postman in the town of Riese, Italy.  He was the second of ten children. Even though his family was poor, education was very important to his parents and Sarto walked 4 miles to school. This love of education would be evident during his life, as he always made sure that the young could get a religious education.

After studying Latin with his parish priest, Giuseppe entered the seminary. He was ordained a priest in 1858. After working in a parish and teaching in a seminary, Sarto was made Bishop of Treviso in 1879. As a priest and bishop, he worked to make religious education available to youths who would not have had the opportunity otherwise. On June 12, 1893, Pope Leo XIII made him a cardinal and three days later he was named the Patriarch of Venice.

On July 10, 1903, Pope Leo XIII died and at the end of the month the conclave met to elect a new pope. Many doubted that Sarto had a chance to be elected because of his conservatism. Before the conclave, someone told him that he would not be elected Pope. Sarto replied, "Thanks be to God, as I bought a round-trip ticket.” On the first ballot, Sarto only received five votes, but as time went on the number increased. When he saw the vote going in his favor, Sarto pleaded with his brother cardinals, “For the love of God, forget about me. I do not have the qualities to be pope. When it became clear that he had won, he said “I accept as one accepts a cross.” He was crowned Pope Pius X on August 9, 1903.

[![PopePiusXBanners](images/popepiusxbanners.png)](http://realromancatholic.files.wordpress.com/2013/08/popepiusxbanners.png)Upon becoming pope, Pius X took the motto “Instaurare omnia in Christo” - “To restore all things to Christ”. These words would define his papacy. Being a humble man, he greatly reduced the Papal ceremonies. He abolished the custom of applauding the Pope in St. Peter’s, saying that it was not “fair to applaud the servant in the home of the Master.” He promoted daily Communion for Catholics as a way to draw closer to God. He reduced the required age for First Holy Communion from 12 to 7 years old. Also, he promoted the frequent use of the Sacrament of Penance, so that Communion could be received worthily. He worked to reform seminaries and Church music. Pius X set in motion the creation of the first universal Code of Canon Law. He is remembered to this day for his strong anti-modernism efforts. He went so far as to call liberal Catholics “wolves in sheep’s clothing”. He required all clergy to take an Oath Against Modernism.

Several miracles were performed through Pius X’s intercession during his lifetime. During a papal audience, he was holding a paralyzed child in his arms, when the child wriggled free and ran around the room.  The coming of World War I in 1914 greatly devastated the Holy Father. When he was asked to bless some troops before they left for the front, he replied that he would “only bless peace”. He died on August 20, 1914, after reigning for 11 years. He was canonized by Pope Pius XII on May 29, 1954. He is honored on August 21st.
