---
title: "St. John Bosco Is Taken on a Tour of Hell"
date: "2013-02-09"
categories: 
  - "saints"
  - "sin"
tags: 
  - "catholic"
  - "don-bosco"
  - "saint"
  - "sin"
---

[![St.DonBosco](images/st-donbosco.png)](http://realromancatholic.files.wordpress.com/2013/02/st-donbosco.png)I don't usually post articles from other blogs on this site, but after reading this particular one I had to. You can find the original post [here](http://americaneedsfatima.blogspot.com/2013/01/saint-reveals-what-hell-is-like.html). This is a story that should be read to all young people to persuade them avoid sin.

We are all familiar with Charles Dickens’ character, Ebenezer Scrooge, a selfish, greedy, merciless man saved from eternal fire by three spirits who led him on a journey to the past, the present and the future, and, ultimately, Hell.

Dickens wrote his famed story in 1838. Interestingly enough, exactly thirty years after the novelist penned _A Christmas Carol_ in England, in faraway Piedmont, Italy, a spirit indeed tapped a man’s shoulder and bid him follow.

The man was St. John Bosco, the endearing friend of the youth, the antithesis of Scrooge.

Don Bosco, as he was known, dedicated his priestly life to the welfare of wayward boys. A visionary and mystic, he was given a series of “dreams,” rather mystical visions, for the spiritual welfare of the boys of his “Oratory.”

One such vision was that of Hell. Stark, terrifying, it ultimately represented to Don Bosco’s boys the same outreach of Mercy that the Scrooge of fiction received–with the difference that what Don Bosco penned was no fiction, but Scriptural, dogmatic truth enhanced by experience.

After each “dream” Don Bosco gathered his boys and told them what he had been shown. He also left us a lengthy, detailed account. We publish a summary of his vision of Hell.

**The Snares, the Demon, and the Weapons**

After several nights awoken by an angelic visitor, Don Bosco was exhausted.

“Lest I should fall asleep and start dreaming, I set my pillow upright…and practically sat up, but soon, exhausted, I fell asleep. Immediately the same person of the night before appeared at my beside.”

“Get up and follow me!” he said.

Reluctantly, the saint followed and found himself in a desolate, desert-like place. As guide and guided trudged through the dismal valley, a delightful, green, flowery road opened before them. Don Bosco eagerly took to the path, but as he walked, realized it gently sloped downward.

He then saw that his boys and others were passing him left and right. Suddenly, one of them fell violently backwards, legs in the air, and as if pulled by an invisible snag, disappeared over a distant cliff. Several of the boys met with the same fate.

On closer inspection, Don Bosco realized that there were lassos on the ground made of such filmy fiber they were hardly detectable. They have spread cords for a net; by the wayside they have laid snares for me – Psalms 139:6.

“Do you know what this is?” the priest asked his guide.

“A mere nothing,” he answered. “Just plain human respect.”

At the guide’s bidding, Don Bosco picked up a snare, and began to pull.

“…I immediately felt resistance. I pulled harder only to feel …that I was being pulled down myself…and soon was at the mouth of a frightful cave…I kept tugging, and after a long while a huge, hideous monster emerged, clutching a rope to which all…the snares were tied.”

Letting go, Don Bosco turned to the guide who said,

“Now you know who he is.”

“I surely do, the devil himself!”

Now Don Bosco began to inspect the snares. Each bore an inscription: Pride, Disobedience, Envy, Impurity, Theft, Gluttony, Sloth, Anger and so on. He realized that the sins that trapped most boys were those of impurity, disobedience, and pride, though others caught them as well. Those of “human respect” pulled them down swiftly.

As he looked even closer, he spotted knives among the snares placed there by a helping hand. These had inscriptions on them as well: meditation, attentive spiritual reading. There were also swords that read: devotion to the Blessed Sacrament, frequent Holy Communion, and devotion to the Blessed Virgin Mary, St. Joseph, St. Louis Gonzaga, and other saints. There were also hammers symbolizing Confession. The boys that made use of these weapons were able to cut themselves free.

**The Place of No Return**

As guide and priest continued, the road became ever steeper and increasingly devoid of vegetation and flowers. At a certain point it was so vertical Don Bosco could hardly stay upright.

“…at the bottom of this precipice, at the entrance of a dark valley, an enormous building loomed into sight, its towering portal tightly locked facing our road. When I finally arrived at the bottom, I felt smothered by a suffocating heat, while a greasy-greenish smoke, and flashes of scarlet flames rose behind those enormous walls that loomed higher than mountains.”

As Don Bosco looked up, he read a sign over the massive gates: The Place of No Return–and he knew they were at the gates of Hell.

Suddenly, the guide pointed to the distance, and Don Bosco saw a boy racing down the path at an uncontrollable speed. As he approached, the horrified priest recognized one of his boys. The boy’s hair stood on end, his eyes bulged, and his arms flayed like those of one drowning.

“Let’s help him! Let’s stop him,” I shouted.”

“Leave him alone,” the guide replied.

“Why?!”

“Do you think you can restrain one who is fleeing from God’s just wrath?”

As the boy crashed into the portal, it sprang open with a roar, and instantly a thousand inner portals opened with a deafening clamor as if struck by a body propelled by an irresistible gale.

Other boys now came hurtling down the path, screaming in terror, arms outstretched. Some came down alone, others arm in arm, one boy being pushed by another. Each had his particular sin written on his forehead. Don Bosco recognized them as they crashed into the portal to be sucked into the endless corridor amid a long-drawn, fading, infernal echo. He called to them in anguish, but they did not hear him. As the gates stood momentarily open, Don Bosco caught a glimpse of something like furnace jaws spouting fiery balls.

“Bad friends, bad books and bad habits,” spoke the guide, “are mainly responsible for so many eternally lost.”

“If so many of our boys end up this way, we are working in vain. How can we prevent such tragedies?”, asked Don Bosco.

“This is their present state,” answered the guide, “and that is where they would go were they to die now.”

**Into the Gates**

Another group of boys came hurtling down, and the portals momentarily opened.

“Let’s go in,” the guide said, as Don Bosco pulled back in horror.

“Come. You’ll learn much.”

“We entered that narrow, horrible corridor and whizzed through it with lightning speed. Threatening inscriptions shone eerily over all the inner gateways. The last one opened into a vast, grim courtyard with a large, forbidding entrance at the far end.”

“From here on,” said the guide, “no one may have a helpful companion, a comforting friend, a loving heart, a compassionate glance, or a benevolent word; all that is gone forever. Do you just want to see or would you rather experience these things yourself?”

“I only want to see!” answered Don Bosco readily.

Stepping through the forbidding gate, the guide took Don Bosco down a corridor to an observation platform behind a great glass wall. Gripped by an indescribable terror, Don Bosco beheld an immense cave sunk into the bowels of the mountains.

“The cave was ablaze, but not with an earthly fire with leaping tongues of flames. The entire cave, walls, ceiling, floor, iron, stones, wood, and coal…glowed white at temperatures of thousands of degrees…”

As he watched, with shrilling screams a few boys were plunged into the white heat as into a cauldron of liquid bronze. Instantly they too became incandescent and perfectly motionless.

**A Terrible Choice**

More frightened than ever Don Bosco asked,

“When these boys come dashing into this cave, don’t they know where they are going?”

“They surely do,” explained the guide, “They have been warned a thousand times, but they still choose to rush into the fire because they do not detest sin and are loath to forsake it. Furthermore, they despise and reject God’s incessant, merciful invitations to do penance. “

The guide then bid the priest look closer and he saw those poor wretches savagely striking at each other like mad dogs. Others clawed their own faces and hands, tearing their own flesh and spitefully throwing it about. Just then the entire ceiling of the cave became transparent as crystal and revealed a patch of heaven and their radiant companions safe for all eternity.

The poor wretches fumed with envy and burned with rage because they had once ridiculed the good. _The wicked shall see and shall be angry. He shall gnash his teeth and pine away_–Psalms 111:10

“Pressing my ear to the crystal window, I heard screams and sobs, blasphemies and imprecations against the saints.”

The guide then led Don Bosco into a lower cavern above which was written, Their worm shall not die and their fire shall not be quenched – Isaiah 66:24.

In this lower cave Don Bosco again beheld boys from the Oratory.

“…I drew closer…and noticed that they were covered with worms and vermin which gnawed at their vitals, hearts, eyes, hands, legs, and entire bodies so ferociously as to defy description. Helpless and motionless, they were prey to every kind of torment…”

Don Bosco again tried to talk to them but no one even looked at him or spoke to him. The guide then explained that the damned are totally deprived of freedom. Each must endure his punishment with no possible reprieve.

As he watched these wretched boys, again Don Bosco turned to his guide.

“How can these boys be damned? Last night they were still alive at the Oratory!”

“The boys you see here,” retorted the guide, “are all dead to God’s grace. Were they to die now or persist in their evil ways, they would indeed be damned.”

Don Bosco was also shown the atrocious remorse of those who had been pupils in his schools. What a torment to remember the innumerable favors, blessings, warnings and graces they had received at the Oratory, especially graces from the Blessed Virgin Mary. What torture to think that they could have been saved so easily if they only had kept their good resolutions. Indeed, Hell is paved with good intentions!

**Sexual Sins**

Lastly, Don Bosco was shown the damage that the sin of impurity causes, which is the sin that abuses the sacred gift of our sexuality which God meant to be legitimately used to unite a man and a woman and to procreate children.

Our Lord teaches that such sins are already sinful in accepted thoughts, in deliberate looks and, of course, in actions that are the result of impure thoughts and looks.

Don Bosco saw an entrance above which was written, The Sixth Commandment. The guide exclaimed,

“Transgressions of this commandment caused the eternal ruin of many boys.”

“Didn’t they go to Confession?” asked Don Bosco.

“They did, but they either omitted or insufficiently confessed the sins against the beautiful virtue of purity. Other boys may have fallen into that sin but once in their childhood, and, through shame, never confessed it or did so insufficiently. Others were not truly sorry or sincere in their resolve to avoid it in the future. There were some who, rather than examine their conscience, spent their time trying to figure out how best to deceive their confessor. Anyone dying in this frame of mind chooses to be among the damned, and so he is doomed for all eternity. Only those who die truly repentant shall be eternally happy.

Now, do you want to see why our merciful God brought you here? “

And the guide showed Don Bosco a group of boys whom he knew well who were in Hell because of this sin. Among them were some whose conduct seemed good. Don Bosco begged to be allowed to jot down their names so as to warn them. But the guide said it was not necessary.

“Always preach against immodesty. Bear in mind that even if you did admonish them individually, they would promise, but not always in earnest. For a firm resolution to avoid the sin of impurity one needs God’s grace, which will not be denied to your boys if they pray. God’s power is specially manifested through mercy and forgiveness. On your part, pray and offer sacrifices. As for the boys, let them listen to your admonitions and consult their consciences. They will know what to do.”

And the guide continued,

“Keep telling them that by obeying God, the church, their parents, and their superiors, even in little things, they will be saved. Warn them against idleness. Tell them to keep busy at all times, because the devil will not then have a chance to tempt them.”

**On the Way Out**

Now it was finally time to leave that place of dread. Don Bosco could hardly stand up, so the guide held him up gently and in no time at all they had retraced their steps through the terrible corridor. But as soon as they had stepped across the last portal, the guide said,

“Now that you have seen what others suffer, you too must experience a touch of hell.”

“No, no!” Don Bosco cried in terror.

“Look at this wall,” said the guide. “There are a thousand walls between this and the real fire of hell.”

When he said this, Don Bosco instinctively pulled back, but seizing his hand the guide touched it to the last wall of Hell.

“The sensation was so utterly excruciating that I leaped back with a scream and found myself sitting up in bed. My hand was stinging and I kept rubbing it to ease the pain. Next morning I noticed that it was swollen. Later the skin of my palm peeled off.”

**Conclusion**

As free human beings, we have the capacity to choose; otherwise, we would be mere robots. Free Will and Choice generate Responsibility, which, in turn places us necessarily at a stark crossroads between God, Who is Supreme Good, and the antithesis of God, which is total evil. If we choose God, we choose all that He is: Good, Beauty, Love, Bliss. If we choose against God, we have all that He is not: evil, hideousness, hate, misery. _He has set water and fire before you: stretch forth your hand to which you will_. Eccl.15:17
