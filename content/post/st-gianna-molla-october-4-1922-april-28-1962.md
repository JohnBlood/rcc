---
title: "St. Gianna Molla (October 4, 1922 – April 28, 1962)"
date: "2013-04-18"
categories: 
  - "roman-catholic"
  - "saints"
tags: 
  - "saint"
---

\[caption id="attachment\_648" align="alignright" width="250"\][![St. Gianna Molla](images/stgiannamolla.png "St. Gianna Molla")](http://realromancatholic.files.wordpress.com/2013/04/stgiannamolla.png) St. Gianna Molla\[/caption\]

Gianna decided that medicine was the career for her to best help others. To accomplish this, she entered medical school in November of 1942 in Milan.  She graduated in 1949 and opened an office near her hometown. Her brother was a missionary priest to Brazil. Gianna wanted to join him to offer her services to poor women, but her poor health made that impossible. On December 8, 1954, Gianna attended the first Mass of a young priest named Fr. Garavaglia from the town of Mesero. Another native of Mesero named Pietro Mollo was present. Pietro was an engineer and the manager of a local match factory. Over the next several months, Gianna and Pietro started to date. In early 1955, Pietro asked her to married him. They were married on April 11, 1955 in the Basilica of San Martino in Magenta.

Gianna was happy in married life. She and Pietro moved to a villa in Ponte Nuovo. The local church, Our Lady of Good Counsel, was located close by and Gianna would attend Mass daily. Between 1956 and 1959, she gave birth to three children, Pierluigi, Maria Zita and Laura. However, in 1961, she suffered from two miscarriages. After she became pregnant again, doctors found a tumor on her uterus. The doctors told Gianni she only had three treatment options: remove her uterus, remove the tumor and the unborn child or remove only the tumor. The first option meant that Gianni would not be able to have any more children. The second option would mean the loss of her unborn child. The third option was the riskiest. As a doctor herself Gianni was aware of the risk, but chose the third option because it would ensure the birth of her daughter. She told the doctors, “If you must choose between me and the baby, no hesitation; choose – and I demand it – the baby. Save her!”

The surgery was performed successfully and Gianna returned home. However, the next seven months of her pregnancy were full of complications. Nonetheless, Gianna bore these trials patiently because she knew that her unborn daughter had a right to be born. Finally, Gianna returned to the hospital in April 20, 1962 (Good Friday) to give birth to her fourth child. After natural methods failed, Gianna Emanuela was delivered via Cesarean Section. Shortly after the birth, Gianna suffered from abdominal pains and high fever. She lingered for seven days. She wished to receive Holy Communion one last time before she died, but was unable to because of intense vomiting. Gianna Mollo went to her eternal reward on April 28, 1962 at the age of 39. She was canonized on May 26, 2004 by Blessed Pope John Paul II. Her feast is celebrated on April 28.
