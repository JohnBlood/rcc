---
title: "Fifth Word - I thirst (John 19:27)"
date: "2014-04-10"
categories: 
  - "last-seven-words-of-jesus"
  - "lent"
---

\[caption id="attachment\_933" align="alignright" width="632"\][![I-Thirst](images/i-thirst.png)](http://realromancatholic.files.wordpress.com/2014/04/i-thirst.png) I thirst (John 19:27)\[/caption\]

During Our Lord's Passion, He was twice offered a drink. This first was a mixture of wine and myrrh. This Our Lord refused because it was commonly given to condemned criminals to deaden pain. His Passion and Death would have been rendered worthless if He had allowed anything to mitigate the pain He was about to suffer. The second drink He was offered was sour wine or vinegar. This He drank. In doing so, He drank deeply of the cup which He had begged His Father to remove from Him in the Garden. He drank the last dregs of the cup of our punishment.

Lord God, Your Only Begotten Son drank deeply of the cup of iniquity for my sake. If I were to try to drink the same draft by myself, I would not be able to survive. It is only with Thy help that I can hope to drink of my own bitter draught and survive. Help me to turn away from the sweetness of the world and accept the bitter drink that is punishment for my sins. I beg You to send me the grace and strength required to accept this bitter cup. Let not my will be done, but Thine. Amen.
