---
title: "Venerable Archbishop Fulton Sheen Speaks on Temptation"
date: "2012-10-26"
categories: 
  - "archbishop-fulton-j-sheen"
  - "roman-catholic"
  - "saints"
  - "tv"
  - "video"
tags: 
  - "archbishop-sheen"
  - "temptation"
---

\[youtube=http://www.youtube.com/watch?v=x7OdSU9zmkc\]
