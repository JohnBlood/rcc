---
title: "SSPX Splinters Further (Like all Protestant Organizations)"
date: "2012-10-20"
categories: 
  - "pope-pius-x"
  - "protestants"
  - "sspx"
---

The Society of St. Pius X (SSPX) has once again show its true colors through the actions of its members. In the last year or so, three breakaway movements have appeared within the ranks of SSPX to counter Fellay's efforts to reunite with Rome. These people by their very actions deny that they have any connection to the Catholic Church. In order to call yourself Catholic, you have to believe all the tenants of the faith, especially the primacy of Peter. By further turning their back on Rome, they are acting more Protestant-like and even less Catholic.

\[caption id="attachment\_503" align="alignright" width="300"\][![](http://realromancatholic.files.wordpress.com/2012/10/bishopwilliamsonfist.png?w=300 "Bishop Williamson")](http://realromancatholic.files.wordpress.com/2012/10/bishopwilliamsonfist.png) How would you like a punch up the konk.\[/caption\]

**Bishop Williamson**

One of the most prominent splits has not happened yet, to the best of my knowledge, but when it does it will be big. Bishop Richard William is one of the most widely known of the SSPX bishops, even more well-known than Fellay, the superior general of SSPX. The reason that Williamson is so well-known is because of his denial of the Holocaust during World War II, which has landed him in hot water with the German government.

Williamson leads an element within SSPX which is very anti-Rome. Earlier this year it came out that Williamson and his cronies were working with lawyers to get ownership of SSPX chapels and property. According to the same source, Williamson's group believes that if there is a split, they can gain control of the England, Asia, and Mexico districts.

Thankfully, Williamson's subversive activities have not gone on unnoticed. Several months ago, Williamson was banned for the organizations General Chapter because of his continual disobedience. Recently, he stirred up more controversy by confirming over 100 traditional catholics in South America without approval from his superior. The last I heard is that Williamson will be kicked out of the SSPX for his disobedience and other activities. Undoubtedly, a lot of lay people and religious will go with him.

**SSPX-SO**

Towards the end of August, five priests got together in the US to found the Society of St. Pius X of Strict Observance. The five priests elected Fr. Joseph Pfeiffer as their superior. From what I understand Fr. Pfeiffer was a high-ranking member of SSPX until he was kicked out by Fellay. The founders of the SSPX-SO expect to be joined by 25 former SSPX priests, as well as religious and laity. They have a HQ and a bank account and they are ready to go.

Before I go farther, I want to comment on the name choice for this group. It is very evocative of the name choices of many Protestant churches. During the 500 year history of Protestantism, there have been many groups that tacked the word "reformed" on the front of their name to show that they were similar to the original group, but better. That is what this feels like. Like another Catholic blogger said, it will not belong before there is the Society of St. Pius X of Stricter Observance, the Society of St. Pius X of Strictest Observance, the Reformed Society of St. Pius X and the Orthodox Society of St. Pius X.

**A Whole District Rebels against Fellay**

Lastly, I heard from a friend that an entire district of SSPX in South America voted to remove Bishop Fellay because of his efforts to reunite with Rome.

As King Leonidas said, "This is madness." It is amazing how much hatred so-called "catholics" have for Rome, the seat of the Catholic Church, and for Pope Benedict. The only thing that I can compare it to is the hatred shown towards Rome and the Pope after a man named Luther nailed his 95 Theses to the Cathedral of Wettenberg
