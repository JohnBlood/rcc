---
title: "Nancy Pelosi Continues to Pretend She is \"Catholic\""
date: "2010-03-19"
categories: 
  - "healthcare"
  - "orthodox"
  - "politics"
  - "roman-catholic"
tags: 
  - "catholic"
  - "irreligious"
  - "nancy-pelosi"
  - "st-joseph"
---

Nancy Pelosi pretended that she was Catholic by linking today's feast of St. Joseph to the passage of the health care bill.  She went on to say that she has the signature of many "Catholic" religious who want her to pass what she called "this life-affirming bill".

It is becoming more and more apparent that when these people (politician) use the term "Catholic", they do not mean that they believe the teachings of the Roman Catholic Church.  Instead, they use the term "Catholic" to get votes, no more and no less.  There are few, if any people, anymore in public life who say that they are "Catholic" and are truly follower of Jesus Christ.  Most twist the Church's teachings for their own personal gain.  God help them when they come before the Great Judge in Heaven.  We must continue to pray that these people realize the error of their ways before it is too late.

\[youtube=http://www.youtube.com/watch?v=RMWetHDwSuw&feature=player\_embedded\]
