---
title: "What is Laetare Sunday?"
date: "2015-03-15"
categories: 
  - "lent"
---

Today is Laetare Sunday, also known as the fourth Sunday of Lent. Laetare Sunday is one of the most solemn days of the year. It takes it’s name from the first word of the entrance antiphon or introit: “Laetare Jerusalem: et conventum facite omnes qui diligitis eam: gaudete cum laetitia”, “Rejoice, O Jerusalem: and come together all you that love her: rejoice with joy”. On this one day, the Church interrupts the solemn and penitent air of the previous days and weeks of Lent with words of joy and rejoicing. Why? It seems so out of place during a time when we have been constantly reminded since Ash Wednesday to drive sin out of our lives with prayer and fasting. To suddenly be called to rejoice seems to be out of character for the season.

\[caption id="attachment\_999" align="alignleft" width="225"\][![](images/File:Weltliche_Schatzkammer_Wien_(129).JPG)](https://realromancatholic.files.wordpress.com/2015/03/weltliche_schatzkammer_wien_129.jpg) Golden Rose by Giuseppe and Pietro Paolo Spagna. Rome, around 1818/19. Kept today in the Imperial Treasury in Hofburg Imperial Palace in Vienna.\[/caption\]

However, this one day of rejoicing in the middle of Lent is a sign that the Church deeply understands the needs of her children. She knows the we need all the encouragement we can get. So in the midst of our penance and mortification, the Church gives us a preview of the coming joys of Easter to give us the courage to finish the race, to fight the good fight.

On Laetare Sunday, the priest wears rose colored vestments, instead of the Lenten purple, as a sign of rejoicing. This tradition originated from an ancient papal ceremony called the Blessing of the Golden Rose. The practise is thought to have originated in the Middle Ages. The pope would often bless a golden rose either before or during the Laetare Sunday Mass. Afterwards, the newly blessed rose would be sent to a Catholic monarch, church, or city as a sign of honor. Pope Innocent III said the following: "As Laetare Sunday, the day set apart for the function, represents love after hate, joy after sorrow, and fullness after hunger, so does the rose designate by its colour, odour and taste, love, joy and satiety respectively."

Now, go forth and rejoice for the glories of Easter are close at hand.
