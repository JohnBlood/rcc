---
title: "Cardinal Burke Does it Again"
date: "2013-09-28"
categories: 
  - "cardinal-burke"
  - "michael-voris"
  - "video"
tags: 
  - "cardinal-burke"
---

Recently, Cardinal Raymond Burke gave a very good interview to the Wanderer magazine. I suggest that you read it. You can find it [here](http://www.thewandererpress.com/ee/wandererpress/index.php?pSetup=wandererpress&curDate=20130905). Michael Voris did two very good Vortex episodes on the interview. Enjoy.

\[youtube=http://www.youtube.com/watch?v=88XTSPWqeeI\]

\[youtube=http://www.youtube.com/watch?v=S1Kvw2RBQCY\]
