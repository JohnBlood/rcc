---
title: "Answers for Traditionally Minded Catholics"
date: "2013-07-31"
categories: 
  - "catholic"
  - "church-fathers"
  - "history"
  - "return-to-tradition"
  - "video"
---

It is the duty of all confirmed Catholics to continue to learn about their Faith. However, many of the sources available on the web are either based on the water down theology that appeared after Vatican II or is written by groups who appear Catholic but are not associated with Rome. Thankfully, there is a new website that fills this void.

[Faithful Answers](http://www.faithfulanswers.com/) was recently created to give Catholics answer to questions on Faith based on the ancient traditions of the Church. This site is still very new and does not have a large selection of articles currently. However, they have started a project which will be a great help to many people.

What is most interesting about this project for me is the founder. Here is his bio from the site:

> Chad Arneson is a former Protestant evangelist and pastor. He served as founding senior pastor of West Bay Assemblies of God in Grand Cayman, Cayman Islands, BWI. He also traveled extensively preaching throughout the United States and various countries as an evangelist. His preaching and teaching were further marked by a very real anti-Catholic sentiment. The grace of his conversion to the Catholic Church resulted in a radical change of direction. As president of Faithful Answers he is doing what he calls his “happy penance” of sharing the life-giving Truth of the One, Holy, Catholic, and Apostolic Church.

The current contributors include two priests: Fr. Chad Ripperger and Fr. Michael Rodríguez. Fr. Ripperger is a member of the Priestly Fraternity of St. Peter and Fr. Rodriguez is a strong supporter of the Latin Mass and also got in trouble with his bishop for promoting the Church's stance on homosexuality. Please keep these priests and the other contributors in your prayers as they seek to make the One, True Faith available to everyone.

In closing, here are a couple of videos posted on Faithful Answers. Enjoy.

\[youtube=http://www.youtube.com/watch?v=qySx8tSs8BQ\]

\[youtube=http://www.youtube.com/watch?v=nCGjlrtY0sg\]
