---
title: "My Thoughts on Pope Benedict's Abdication"
date: "2013-02-14"
categories: 
  - "pope-benedict-xvi"
  - "roman-catholic"
tags: 
  - "catholic"
  - "holy-pope"
  - "pope"
  - "pope-benedict-xvi"
---

[![Pope Benedict XVI Wave](images/popebenedictxviwave.png)](http://realromancatholic.files.wordpress.com/2013/02/popebenedictxviwave.png)This Monday we head the news that Pope Benedict XVI is abdicating the Chair of St. Peter. I head this sad news shortly after I woke up at 6 am. My first thought was that it was a horrible and tasteless hoax. But as the day went on, it became clear that it was true. This news has filled me with sadness and dread. I am sad that we are losing a great and holy pope. I am filled with dread because in the last two conclaves, at least, there have been efforts by certain evil elements to get their man elected. We dodged the bullet before, but how long will we be so lucky? We must pray that the cardinals will be led by the Holy Ghost and by the concern of the welfare of the Church and not political gain.

Many people are asking why Pope Benedict would abdicate when the lost pope to do so lived over 600 years ago. I have become convinced that he did so, because the Holy Father sees a battle coming on the horizon. It will be a battle for the very soul of the Church. It will be a battle that will shake the Church to its very core. Because he can sense that this battle is near and he has realized that he does not have the physical capabilities to be the Church's champion, that is why he is abdicating the Throne of Peter.

You might think that I am being an alarmist, but I'm afraid I'm not. All over the world, the Church is taking a beating. Church attendance is down. The sacraments are being abused and neglected. Catholics who are not worthy of the name are supporting policies and ways of life that are in direct contradiction to the will of God. There are many bishops who do not instruct their flock or even correct them. They only seek popularity. Pope Benedict has fought this battle for the life of the Church as long as he can, but can do so no longer. He is seeking to pass the torch to a younger, stronger man, who can fight the battle that is coming without a doubt. God help us when it comes.

Another blogger pointed out some interesting facts about the timing of this announcement. First, the announcement was made on the feast of Our Lady of Lourdes. Second, the conclave will take place in the middle of Lent. This will encourage the cardinals to finish voting before they have to return home for Holy Week. It will also be in the middle of the Church's annual time for prayer and penance.

The whole idea of the pope resigning is part of canon law, but it only takes up one simple line: "If it happens that the Roman Pontiff resigns his office, it is required for validity that the resignation is made freely and properly manifested but not that it is accepted by anyone." Because of this, no one is really sure right now what to do. How will Pope Benedict be addressed in March? What authority will he have? What kind of ceremony should mark the end of his reign? What is known is that he will live in a monastery within the Vatican that is currently undergoing repairs.

In case anyone is wondering, I am referring to this as an abdication not a resignation because is what it is. Pope Benedict is not a CEO, he is an absolute monarch. The correct terminology to describe the retirement of a monarch is abdicate.

In closing, when Pope Benedict XVI bids us farewell, I will undoubted cry bitter tears of sorrow. From now until the conclave finishes, I will pray constantly that the Cardinals will vote with the eyes fixed on the things of heaven and not on the things of earth. I will also pray from Pope Benedict's continued health and well being. (He will always be Pope Benedict XVI to me.) God Bless Pope Benedict XVI and God Bless his successor. May He guide, protect and strengthen us for the battle that lies ahead.
